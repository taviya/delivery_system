@extends('user.layouts.app')
@section('title', 'Contact Us')

@section('content')

    <div class="container">
        <nav aria-label="breadcrumb" class="breadcrumb-nav">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Shop Inquiry</li>
            </ol>
        </nav>
    </div>

    <div class="page-header">
        <div class="container">
            <h1>Shop Inquiry</h1>
        </div>
    </div>

    <div class="container">
        <form id="shop-inqiry-form" method="POST" action="{{ route('shop-inquiry') }}" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6">@csrf
                    <div class="form-group required-field">
                        <label for="shop_name">Shop Name</label>
                        <input type="text" class="form-control" id="shop_name" name="shop_name"
                               value="{{ old('shop_name') }}">
                        @error('shop_name')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required-field">
                        <label for="shop_email">Shop Email</label>
                        <input type="email" class="form-control" id="shop_email" name="shop_email"
                               value="{{ old('shop_email') }}">
                        @error('shop_email')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required-field">
                        <label for="shop_phone">Phone Number</label>
                        <input type="tel" class="form-control" id="shop_phone" name="shop_phone"
                               value="{{ old('shop_phone') }}">
                        @error('shop_phone')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required-field">
                        <label for="shop_phone">Turnover</label>
                        <select name="turnover" class="form-control">
                            <option value="">Select One Value Only</option>
                            @if(!empty(Config::get('constants.shop_turnover')))
                                @foreach (Config::get('constants.shop_turnover') as $key => $value)
                                    <option
                                        value="{{ $value }}">{{ $value }}
                                        Lacs
                                    </option>
                                @endforeach
                            @endif
                        </select>
                        @error('turnover')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required-field">
                        <label for="shop_address">Shop Address</label>
                        <textarea cols="15" rows="1" id="shop_address" class="form-control h-100" name="address"
                        >{{ old('shop_address') }}</textarea>
                        @error('shop_address')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required-field">
                        <label for="shop_message">What’s on your mind?</label>
                        <textarea cols="15" rows="1" id="shop_message" class="form-control h-100" name="shop_message"
                        >{{ old('shop_message') }}</textarea>
                        @error('shop_message')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group required-field">
                        <label>Shop image</label>
                        <div class="col-sm-10">
                            <input type="file" name="shop_image" id="fileUploader" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <img style="height: 100px; width: 100px; display: none;" id="display_image" class="img-thumbnail" src="">
                </div>

                <div class="col-md-12">
                    <div class="form-footer">
                        <input type="submit" class="btn btn-primary btnSubmit" value="Submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@push('scripts')

    <script>
        $('#shop-inqiry-form').validate({
            rules: {
                shop_name: {
                    required: true,
                },
                shop_email: {
                    required: true,
                    email: true,
                },
                shop_phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    digits: true,
                },
                shop_message: {
                    required: true,
                },
                address: {
                    required: true,
                },
                turnover: {
                    required: true,
                },
                shop_image: {
                    required: true,
                    extension: "jpeg|png|jpg"
                }
            },
            messages: {
                shop_name: {
                    required: 'Please enter name',
                },
                shop_email: {
                    required: 'Please enter email',
                },
                shop_phone: {
                    required: 'Please Enter Your Mobile No',
                    minlength: 'Please Enter 10 Digit Mobile No',
                },
                shop_message: {
                    required: 'Please enter message',
                },
                address: {
                    required: 'Please enter shop address',
                },
                turnover: {
                    required: 'Please select turnover',
                },
                shop_image: {
                    required: 'Please select shop image',
                }
            },
            submitHandler: function (form) {
                $(".btnSubmit").val("Please Wait...").attr('disabled', 'disabled');
                return true;
            }
        });

        function readIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#fileUploader").change(function () {
            $("#display_image").css("display", "block");
            readIMG(this);
        });
    </script>

@endpush
