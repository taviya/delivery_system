@extends('user.layouts.app')
@section('title', 'Products shorts')
@section('content')
<style type="text/css">
    h1{
      font-family: Satisfy;
      font-size:50px;
      text-align:center;
      color:black;
      padding:1%;
  }
  #gallery{
      -webkit-column-count:5;
      -moz-column-count:5;
      column-count:4;

      -webkit-column-gap:20px;
      -moz-column-gap:20px;
      column-gap:20px;
  }
  @media (max-width:1200px){
      #gallery{
          -webkit-column-count:3;
          -moz-column-count:3;
          column-count:3;

          -webkit-column-gap:20px;
          -moz-column-gap:20px;
          column-gap:20px;
      }
  }
  @media (max-width:800px){
      #gallery{
          -webkit-column-count:2;
          -moz-column-count:2;
          column-count:2;

          -webkit-column-gap:20px;
          -moz-column-gap:20px;
          column-gap:20px;
      }
  }
  @media (max-width:600px){
      #gallery{
          -webkit-column-count:1;
          -moz-column-count:1;
          column-count:1;
      }  
  }
  #gallery img,#gallery video {
      width:100%;
      height:auto;
      margin: 4% auto;
      /*box-shadow:-3px 5px 15px #000;*/
      cursor: pointer;
      -webkit-transition: all 0.2s;
      transition: all 0.2s;
  }
  .modal-img,.model-vid{
      width:100%;
      height:auto;
  }
  .modal-body{
      padding:0px;
  }
</style>
<link href="https://vjs.zencdn.net/7.15.4/video-js.css" rel="stylesheet" />
<main class="main">
    <div class="page-header">
        <div class="container">
            <center><h2>Products Shorts</h2></center>
        </div>
    </div>
    <div class="container">
        <div id="gallery" class="container-fluid">  
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/sari.mp4') }}" type="video/mp4">
                </source>
            </video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/redsari.mp4') }}" type="video/mp4">
                </source>
            </video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/sample1.mp4') }}" type="video/mp4">
                </source>
            </video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/sarionly.mp4') }}" type="video/mp4">
                </source>
            </video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/sample2.mp4') }}" type="video/mp4">
                </source>
            </video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/redsari.mp4') }}" type="video/mp4" />
            </video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/sari.mp4') }}" type="video/mp4">
                </source>
            </video>
            <video class="vid" autoplay loop muted>
  <source src="{{ asset('assets/user_assets/shorts/bluesari.mp4') }}" type="video/mp4">
  </source>
</video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/bluesari.mp4') }}" type="video/mp4">
                </source>
            </video>
            <video class="vid" autoplay loop muted>
                <source src="{{ asset('assets/user_assets/shorts/bluesari.mp4') }}" type="video/mp4">
                </source>
            </video>

            
<video class="vid" autoplay loop muted>
  <source src="{{ asset('assets/user_assets/shorts/redsari.mp4') }}" type="video/mp4" />
</video>
<video class="vid" autoplay loop muted>
  <source src="{{ asset('assets/user_assets/shorts/sari.mp4') }}" type="video/mp4">
  </source>
</video>
        </div>  
    </div>
    <div class="mb-5"></div><!-- margin -->
</main>


@stop
@push('scripts')    
<script src="https://vjs.zencdn.net/7.15.4/video.min.js"></script>
@endpush