<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{ getSiteSetting()->website_title ? getSiteSetting()->website_title: '' }} - @yield('title')</title>

    <meta name="keywords" content="HTML5 Template"/>
    <meta name="description" content="Porto - Bootstrap eCommerce Template">
    <meta name="author" content="SW-THEMES">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/user_assets/images/icons/favicon.ico') }}">
{{--    <link href="https://harshen.github.io/jquery-countdownTimer/CSS/jquery.countdownTimer.css">--}}


    <script type="text/javascript">
        WebFontConfig = {
            google: {families: ['Open+Sans:300,400,600,700,800', 'Poppins:300,400,500,600,700']}
        };
        (function (d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = '{{ asset('assets/user_assets/js/webfont.js') }}';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>

    <!-- Plugins CSS File -->
    <link rel="stylesheet" href="{{ asset('assets/user_assets/css/bootstrap.min.css') }}">

    <!-- Main CSS File -->
    <link rel="stylesheet" href="{{ asset('assets/user_assets/css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user_assets/css/general.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/user_assets/css/ravigeneral.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/user_assets/vendor/fontawesome-free/css/all.min.css') }}">
    <script src="{{ asset('assets/user_assets/js/jquery.min.js') }}"></script>

    <!-- Validation js -->
    <script src="{{asset('assets/js/jquery-validate/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery-validate/additional-methods.min.js')}}"></script>
    <script>
        var base_url = '{{ env('ADMIN_APP_URL') }}';
        var user_base_url = '{{ env('APP_URL') }}';
        var current_route = '{{ \Request::route()->getName() }}';
    </script>
    @yield('after-styles')

    @stack('styles')
</head>
<body>
    <div class="page-wrapper">
        <header class="header">

            <div class="header-middle sticky-header">
                <div class="container">
                    <div class="header-left">
                        <button class="mobile-menu-toggler" type="button">
                            <i class="icon-menu"></i>
                        </button>
                        <a href="{{ route('user.home') }}" class="logo">
                            <img src="{{ asset('assets/user_assets/images/logo.png') }}" alt="Porto Logo">
                        </a>
                        <nav class="main-nav">
                            <ul class="menu">
                                <li class="{{ Request::route()->getName() == 'user.home' ? 'active' : '' }}">
                                    <a href="{{ route('user.home') }}">Home</a>
                                </li>
                                <li>
                                    <a href="#">Categories</a>
                                    <div class="megamenu">
                                        <div class="row row-sm">
                                            @php
                                            $allmenus = allMainCategory();
                                            @endphp
                                            @if(!empty($allmenus))
                                            @foreach($allmenus as $menu)
                                            <div class="col-lg-4">
                                                <a href="{{ route('maincategories.catname',$menu->id) }}" class="nolink">{{ $menu->main_category_name }}</a>
                                                @php
                                                $allsubmenus = allSubCategory($menu->id);
                                                @endphp
                                                @if(!empty($allsubmenus))
                                                @foreach($allsubmenus as $submenu)
                                                <ul class="submenu">
                                                    <li><a href="#">{{ $submenu->sub_category_name }}</a></li>
                                                </ul>
                                                @endforeach
                                                @endif
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div><!-- End .megamenu -->
                                </li>
                                <li class="{{ Request::route()->getName() == 'product-list' ? 'active' : '' }}">
                                    <a href="{{ route('product-list') }}">Products</a>
                                </li>
                                <li class="{{ Request::route()->getName() == 'shop-form' ? 'active' : '' }}"><a href="{{ route('shop-form') }}" class="shop_inquiry">Shop Inquiry</a></li>
                                @foreach(headerpages() as $page)
                                <li><a href="{{ route('page',$page->slug) }}" title="{{ $page->title }}">{{ $page->title }}</a></li>
                                @endforeach
                                <li class="{{ Request::route()->getName() == 'contact.us' ? 'active' : '' }}"><a href="{{ route('contact.us') }}">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="header-right">
                        <div class="header-search">
                            <a href="#" class="search-toggle" role="button"><i class="icon-search-3"></i></a>
                            <form action="{{ route('product-list') }}" method="get">
                                <div class="header-search-wrapper">
                                    <input type="search" class="form-control" name="product" id="q"
                                    placeholder="Search by product" required="">
                                    <button class="btn" type="submit"><i class="icon-search-3"></i></button>
                                </div>
                            </form>
                        </div>

                        @if(Auth::user())
                        <a href="{{ route('wishlist.listing') }}" class="porto-icon"><i class="icon icon-wishlist-2"></i>
                            @if(wishlistCount(Auth::user()->id) > 0)
                            <span class="carts-count">{{ wishlistCount(Auth::user()->id) }}</span>
                            @else
                            <span class="carts-count">0</span>
                            @endif
                        </a>
                        @else
                        <a href="javascript:void(0);" class="porto-icon"><i class="icon icon-wishlist-2" data-toggle="modal" data-target="#loginModal"></i></a>
                        @endif

                        <div class="dropdown cart-dropdown">
                            <a href="{{ route('cart') }}" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" data-display="static">
                            <i class="minicart-icon {{ Auth::user() ? 'cart-icn' : '' }}"></i>
                            {!! Auth::user() ? '<span class="cart-count">'.count(cartProducts()).'</span>' : '' !!}</a>

                            <div class="dropdown-menu ajax-view-cart">
                                {{--Ajax response--}}
                            </div>
                        </div>
                            @if (!Auth::guest())
                        <div class="dropdown cart-dropdown">
                            <a href="#" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                            <i class="icon icon-user-2 menu-user"></i>
                        </a>

                        <div class="dropdown-menu">
                            <div class="dropdownmenu-wrapper">
                                <div class="dropdown-cart-action">
                                    <a href="{{ route('account') }}" class="btn btn-block lg-btn btn-sm">{{ __('Account') }}</a>
                                </div>
                                <div class="dropdown-cart-action">
                                    <a href="{{ route('logout') }}" class="btn btn-block lg-btn btn-sm" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <a href="javascript:void(0);" class="porto-icon"><i class="icon icon-user-2" data-toggle="modal" data-target="#loginModal"></i></a>
                    @endif
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div style="margin-bottom: 0rem; margin-top: 0rem;" class="product-cat-box owl-carousel owl-theme" data-toggle="owl" data-owl-options="{
            'margin': 0,
            'items': 2,
            'loop' : false,
            'autoplay' : false,
            'dots': false,
            'nav' : true,
            'responsive': {
            '0' : {
            'items' : 2
        },
        '768': {
        'items': 3
    },
    '992' : {
    'items' : 4
},
'1200': {
'items': 5
}
}
}">

@php
$allmenus = allMainCategory();
@endphp
@if(!empty($allmenus))
@foreach($allmenus as $menu)
<div class="product-cat flex-wrap" style="border: none">
    <img class="rounded-circle" src="{{ asset('public/upload/category/'.$menu->category_icon.'') }}" alt="banner" style="height: 67px; width: 67px;">
    <span class="w-100 text-center p-3"><a href="{{ route('maincategories.catname',$menu->id) }}">{{ $menu->main_category_name }}</a></span>
</div>
@endforeach
@endif
<div class="product-cat flex-wrap" style="border: none">
    <!--<i class="icon-category-saddle"></i>-->
    <img src="{{ asset('assets/user_assets/slider_boot.png') }}" alt="banner" style="height: 67px; width: 67px;">
    <span class="w-100 text-center p-3"><a href="{{ route('product-list') }}">New Arrivals</a></span>
</div>
<div class="product-cat flex-wrap" style="border: none">
    <!--                    <i class="icon-category-pedals font-lg"></i>-->
    <img src="{{ asset('assets/user_assets/lp.png') }}" alt="banner" style="height: 67px; width: 67px;">
    <span class="w-100 text-center p-3"><a href="{{ route('product-list') }}">Seasonal</a></span>
</div>
</div>
</div>

<div class="header-bottom">
    <div class="col-lg-4">
        <div class="service-widget">
            <i class="service-icon icon-shipping"></i>
            <div class="service-content">
                <h3 class="service-title">Return & Exchanges</h3>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="service-widget">
            <i class="service-icon icon-money"></i>
            <div class="service-content">
                <h3 class="service-title">money back guarantee</h3>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="service-widget">
            <i class="service-icon icon-support"></i>
            <div class="service-content">
                <h3 class="service-title">online support 24/7</h3>
            </div>
        </div>
    </div>
</div>
</header><!-- End .header -->

<main class="home main">
    @yield('content')
</main><!-- End .main -->

<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="newsletter-widget">
                <i class="icon-envolope"></i>

                <div class="newsletter-info">
                    <h3>Get Special Offers and Savings</h3>
                    <p>Get all the latest information on Events, Sales and Offers.</p>
                </div>

                <form method="post" id="emailsubscriberform">
                    @csrf
                    <div class="submit-wrapper">
                        <input type="search" class="form-control" name="email" id="emailsubscriber"
                        placeholder="Enter Your E-mail Address..." required="">
                        <button class="btn" type="submit">Sign Up</button>
                    </div>
                    <span id="email_radio-error" style="color: white;"></span>
                </form>
            </div>

            <div class="social-icons">
               @foreach(socialdata() as $social)
               <a href="{{ $social->url }}" target="_blank" class="social-icon"><i class="{{ $social->fontawesome_code }}"></i></a>
               @endforeach
           </div>
       </div>
   </div>
   <div class="footer-middle">
    <div class="container">
        <div class="row row-sm">
            <div class="col-md-12 text-center">
                <h4 class="pb-4 delivered_msg">
                    <i class="fab fa-alarm-clock"></i>
                    <span>Time Left For Today's Delivery </span>
                    <span id="hs_timer" class="text-danger"></span>
                </h4>
            </div>
            <div class="col-md-6 col-lg-3">
                <img src="{{ asset('assets/user_assets/images/logo-white.png') }}">
                <p>If you have any query please contact us.</p>
                <div class="social-link">
                    <h3 class="link-title">CALL</h3>
                    <a href="tel:{{ getSiteSetting()->phone ? getSiteSetting()->phone : '' }}">{{ getSiteSetting()->phone ? getSiteSetting()->phone : '' }}</a>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="widget">
                    <h3 class="widget-title">Social</h3>
                    <div class="widget-content row row-sm">
                        <ul class="col-xl-12">
                            @foreach(socialdata() as $social)
                            <li><a href="{{ $social->url }}" target="_blank">{{ ucfirst($social->title) }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="widget">
                    <h3 class="widget-title">About</h3>
                    <div class="widget-content row row-sm">
                        <ul class="col-xl-12">
                            @foreach(footerpages() as $page)
                            <li><a href="{{ route('page',$page->slug) }}" title="{{ $page->title }}">{{ $page->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="widget">
                    <h3 class="widget-title text-center">Contact Info</h3>
                    <div class="widget-content">
                        <ul class="footer-contact">
{{--                            <li><a href="javascript:void(0)"><i class="icon-location fa-2x text-white"></i></a>{{ getSiteSetting()->address ? getSiteSetting()->address : '' }}</li>--}}
                            <li><a href="tel:{{ getSiteSetting()->phone ? getSiteSetting()->phone: '' }}"><i class="icon-mobile fa-2x text-white"></i></a>{{ getSiteSetting()->phone ? getSiteSetting()->phone: '' }}</li>
                            <li><a href="mailto:{{ getSiteSetting()->email ? getSiteSetting()->email: '' }}"><i class="icon-mail-alt fa-2x text-white"></i></a>{{ getSiteSetting()->email ? getSiteSetting()->email : '' }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-bottom container">
    <p>{{ getSiteSetting()->website_title ? getSiteSetting()->website_title: '' }}. © {{ date('Y') }}. All Rights Reserved</p>
</div>
</footer><!-- End .footer -->
</div><!-- End .page-wrapper -->

<div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

<div class="mobile-menu-container">
    <div class="mobile-menu-wrapper">
        <span class="mobile-menu-close"><i class="icon-retweet"></i></span>
        <nav class="mobile-nav">
            <ul class="mobile-menu">
                <li class="{{ Request::route()->getName() == 'user.home' ? 'active' : '' }}"><a href="{{ route('user.home') }}">Home</a></li>
                <li>
                    <a href="#">Categories</a>
                    <ul>
                        @php
                            $allmenus = allMainCategory();
                        @endphp
                        @if(!empty($allmenus))
                            @foreach($allmenus as $menu)
                                <li>
                                    <a href="{{ route('maincategories.catname',$menu->id) }}">{{ $menu->main_category_name }}</a>
                                    @php
                                        $allsubmenus = allSubCategory($menu->id);
                                    @endphp
                                    @if(!empty($allsubmenus))
                                        <ul>
                                            @foreach($allsubmenus as $submenu)
                                                <li><a href="#">{{ $submenu->sub_category_name }}</a></li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                <li class="{{ Request::route()->getName() == 'product-list' ? 'active' : '' }}"><a href="{{ route('product-list') }}">Product List</a></li>
                <li class="{{ Request::route()->getName() == 'shop-form' ? 'active' : '' }}"><a href="{{ route('shop-form') }}">Shop Inquiry</a></li>
                <li class="{{ Request::route()->getName() == 'contact.us' ? 'active' : '' }}"><a href="{{ route('contact.us') }}">Contact Us</a></li>
            </ul>
        </nav><!-- End .mobile-nav -->

        <div class="social-icons">
            <a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
            <a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
            <a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
        </div><!-- End .social-icons -->
    </div><!-- End .mobile-menu-wrapper -->
</div><!-- End .mobile-menu-container -->

<!-- newsletter-popup-form -->
{{--<div class="newsletter-popup mfp-hide bg-img" id="newsletter-popup-form"
     style="background-image: url({{ asset('assets/user_assets/images/newsletter_popup_bg.jpg') }})">
    <div class="newsletter-popup-content">
        <img src="{{ asset('assets/user_assets/images/logo-black.png') }}" alt="Logo" class="logo-newsletter">
        <h2>BE THE FIRST TO KNOW</h2>
        <p>Subscribe to the Porto eCommerce newsletter to receive timely updates from your favorite products.</p>
        <form action="#">
            <div class="input-group">
                <input type="email" class="form-control" id="newsletter-email" name="newsletter-email"
                       placeholder="Email address" required>
                <input type="submit" class="btn" value="Go!">
            </div>
        </form>
        <div class="newsletter-subscribe">
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="1">
                    Don't show this popup again
                </label>
            </div>
        </div>
    </div>
</div>--}}

<!-- Add Cart Modal -->
<div class="modal fade" id="addCartModal" tabindex="-1" role="dialog" aria-labelledby="addCartModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body add-cart-box text-center">
                <p>You've just added this product to the<br>cart:</p>
                <h4 id="productTitle"></h4>
                <img src="#" id="productImage" width="100" height="100" alt="adding cart image">
                <div class="btn-actions">
                    <a href="cart.html">
                        <button class="btn-primary">Go to cart page</button>
                    </a>
                    <a href="#">
                        <button class="btn-primary" data-dismiss="modal">Continue</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Whishlist Modal -->
<div class="modal fade" id="addWhishlistModal" tabindex="-1" role="dialog" aria-labelledby="addWhishlistModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body add-cart-box text-center">
                <p>You've just <span class="cartmsg"></span> this product to the<br>whishlist:</p>
                <h4 id="productTitle"></h4>
                <img src="#" id="productImage" width="100" height="100" alt="adding cart image">
                <div class="btn-actions">
                    <a href="{{ route('wishlist.listing') }}">
                        <button class="btn-primary">Go to whishlist</button>
                    </a>
                    <a href="javascript:void(0)">
                        <button class="btn-primary" data-dismiss="modal">Continue</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="quickviewModalCenter" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header" style="height: 43px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="quickviewContent">
                    {{--Ajax response--}}
                </div>
            </div>
        </div>
    </div>

<a id="scroll-top" href="#top" title="Top" role="button"><i class="icon-angle-up"></i></a>
@include('auth.login_popup')
@include('auth.register_popup')

<!-- Plugins JS File -->
<script src="{{ asset('assets/user_assets/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/user_assets/js/plugins.min.js') }}"></script>
<script src="{{ asset('assets/user_assets/js/nouislider.min.js') }}"></script>
{{--<script src="https://refreshless.com/nouislider/documentation/assets/wNumb.js"></script>--}}
<script src="{{ asset('assets/user_assets/js/plugins/isotope-docs.min.js') }}"></script>

<!-- Main JS File -->
<script src="{{ asset('assets/user_assets/js/main.min.js') }}"></script>
<script src="{{asset('assets/js/bootstrap-notify.min.js')}}"></script>
<script src="{{ asset('assets/user_assets/js/general.js') }}"></script>
<script src="{{ asset('assets/user_assets/js/ravigeneral.js') }}"></script>
    <script src="{{ asset('assets/user_assets/js/jquery.countdownTimer.min.js') }}"></script>

    @php
        $timer = footerTimer();
        if ($timer['status']) {
            @endphp
                <script>
                    $(function(){
                        $("#hs_timer").countdowntimer({
                            hours: '{{ $timer['diff']->h }}',
                            minutes: '{{ $timer['diff']->i }}',
                            seconds: '{{ $timer['diff']->s }}',
                            size: "lg"
                        });
                    });
                </script>
            @php
        }else{
            echo "<script>$('.delivered_msg').html('Your order/package is accepted and delivered by tomorrow.')</script>";
        }
    @endphp
<script>
    @if(Session::has('status'))
    var msg = "{{ Session::get('status') }}";
    message(msg, 'success');
    @endif

    @if(Session::has('success'))
    var msg = "{{ Session::get('success') }}";
    message(msg, 'success');
    @endif

    @if(Session::has('error'))
    var msg = "{{ Session::get('error') }}";
    message(msg, 'danger');
    @endif

    @if(\Request::route()->getName() == 'login' || \Request::route()->getName() == 'register')
        window.location.href = user_base_url;
    @endif
</script>
@yield('after-scripts')
@stack('scripts')
</body>

</html>
