<div class="product-single-container product-single-default product-quick-view product-qv-content">
    <div class="row">
        <div class="col-lg-6 col-md-6 product-single-gallery">
            <div class="product-slider-container product-item">
                <div class="product-single-carousel owl-carousel owl-theme">
                    @php
                        $images = explode(',', $products->product_image);
                    @endphp
                    @if(!empty($images))
                        @foreach($images as $image)
                            <div class="product-item">
                                <img class="product-single-image qv-single-image" src="{{ asset('public/upload/product/'.$image.'') }}"
                                     data-zoom-image="{{ asset('public/upload/product/'.$image.'') }}"/>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="prod-thumbnail row owl-dots" id='carousel-custom-dots'>
                @php
                    $images = explode(',', $products->product_image);
                @endphp
                @if(!empty($images))
                    @foreach($images as $image)
                        <div class="col-3 owl-dot">
                            <img src="{{ asset('public/upload/product/thumbnail/'.$image.'') }}"/>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6">
            <div class="product-single-details">
                <h1 class="product-title">{{ $products->product_name }}</h1>
                <h3>{{ $products->product_sub_heading }}</h3>
                <div class="ratings-container"></div>

                <div class="price-box">
                    <span class="product-price">₹{{ $products->price }}</span>
                    @if(!empty($products))
                        @if($products->stock < 10 && $products->stock != 0)
                            <span class="product-remain-stock">Hurry, Only {{ $products->stock }} left!</span>
                        @elseif($products->stock == 0)
                            <span class="product-out-stock">Currently out of stock</span>
                        @else
                            <span class="product-in-stock">In Stock</span>
                        @endif
                    @endif
                </div>

                <div class="product-desc">
                    {!! Str::limit($products->description, 450, ' ...') !!}
                    @if(!empty($products->description))
                        @if(str_word_count($products->description) > 150)
                            <a href="{{ route('product-details',$products->id) }}">(Read more)</a>
                        @endif
                    @endif
                </div>

                <div class="product-filters-container">
                    <div class="product-single-filter">
                        @if($products->normalsize == 1)
                            <label>Size:</label>
                            <ul class="config-size-list">
                                @if(!empty($sizes))
                                    @foreach($sizes as $size)
                                        <li data-toggle="tooltip" data-size="{{ $size->sizes }}"
                                            data-stock="{{ $size->sizes_stock }}" title="{{ $size->sizes_stock }}"
                                            class="@if($size->sizes_stock != 0) {{ "normal-size-chart"}} @endif">
                                            <a class="@if($size->sizes_stock == 0) {{ "outofstock" }} @endif"
                                               href="javascript:void(0)">{{ $size->sizes }}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        @endif
                        @if($products->shooessize == 1)
                            <label>Size:</label>
                            <ul class="config-size-list">
                                @if(!empty($sizes))
                                    @foreach($sizes as $size)
                                        <li data-toggle="tooltip" data-size="{{ $size->sizes }}"
                                            data-stock="{{ $size->sizes_stock }}" title="{{ $size->sizes_stock }}"
                                            class="@if($size->sizes_stock != 0) {{ "shooes-size-chart" }} @endif">
                                            <a class="@if($size->sizes_stock == 0) {{ "outofstock" }} @endif"
                                               href="javascript:void(0)">{{ $size->sizes }}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        @endif
                    </div>
                </div>

                <div class="product-action product-all-icons">
                    <input type="hidden" name="selectednormalsize" class="selectednormalsize">
                    <input type="hidden" name="selectedshooessize" class="selectedshooessize">
                    <input type="hidden" name="selectedproduct" class="selectedproduct" value="{{ $products->id }}">
                    @if(Auth::user())
                        <a href="javascript:void(0)"
                           class="paction add-cart add-to-carts @if($products->stock == 0) {{ "outofstock" }} @endif"
                           title="Add to Cart" @if($products->stock == 0) disabled @endif>
                            Add to Cart
                        </a>
                    @else
                        <button class="btn-icon btn-add-cart paction add-cart" data-toggle="modal"
                                data-target="#loginModal">Add to Cart
                        </button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
</script>
