@extends('user.layouts.app')

@section('title', 'Product Details')



@section('content')

<link href="http://localhost/delivery_system/assets/user_assets/css/productlisting.css?v=1.0.0" rel="stylesheet" type="text/css" media="all" />

<style>/* Style the tab */

.tab {  float: left;   background-color: #f1f1f1;  width: 35%;  height: 400px;overflow-x: auto;}

/* Style the buttons inside the tab */

.tab button {  display: block;  background-color: inherit;  color: black;  padding: 15px 15px;  width: 100%;  border: none;  outline: none;  text-align: left;  cursor: pointer;  transition: 0.3s;  font-size: 11px;}/* Change background color of buttons on hover */.tab button:hover {  background-color: #ddd;}/* Create an active/current "tab button" class */.tab button.active {  background-color: #ccc;}/* Style the tab content */.tabcontent {  float: left;  padding: 0px 12px;  width: 65%;  border-left: none;  height: 400px; overflow-x: auto;}

.custom-line { border-bottom:1px solid #d8d6d6; padding:5px; }

.custom-control { margin-top: 2rem; }

.has-search .form-control {

    padding-left: 2.375rem;

}

.has-search .form-control-feedback {

    position: absolute;

    z-index: 2;

    display: block;

    width: 2.375rem;

    height: 2.375rem;

    line-height: 4.375rem;

    text-align: center;

    pointer-events: none;

    color: #aaa;

}

.config-swatch-list li.active a:before {

    left: 15%;

}

.config-swatch-list li a{

    margin-right: 1.5rem;

    width: auto;

}

.config-swatch-list li {

    display: -ms-flexbox;

    display: flex;

    -ms-flex-align: center;

    align-items: center;

    margin-right: 0;

    margin-bottom: 1.2rem;

    font-size: 1.2rem;

}

.config-swatch-list {

    margin-top: .3rem;

}

</style>

<main class="main">

    <div class="category-banner">

        <div class="container">

            <div class="content-left">

                <span>EXTRA</span>

                <h2>20% OFF</h2>

                <h4 class="cross-txt">BIKES</h4>

            </div>

            <div class="content-center">

                <img src="assets/images/banners/category_banner.png">

            </div>

            <div class="content-right">

                <p>Summer Sale</p>

                <button class="btn">Shop All Sale</button>

            </div>

        </div>

    </div>



    <nav aria-label="breadcrumb" class="breadcrumb-nav">

        <div class="container">

            <ol class="breadcrumb">

                <li class="breadcrumb-item"><a href="index.html">Home</a></li>

                <li class="breadcrumb-item active" aria-current="page">Bikes</li>

            </ol>

        </div>

    </nav>

    <div class="container">                

        <div class="row">

            <div class="col-lg-12">

                <nav class="toolbox">

                    <div class="toolbox-left">

                        <div class="toolbox-item toolbox-sort">

                            <div class="select-custom">

                                <select name="orderby" class="form-control">

                                    <option value="menu_order" selected="selected">Default sorting</option>

                                    <option value="popularity">Sort by popularity</option>

                                    <option value="rating">Sort by average rating</option>

                                    <option value="date">Sort by newness</option>

                                    <option value="price">Sort by price: low to high</option>

                                    <option value="price-desc">Sort by price: high to low</option>

                                </select>

                            </div><!-- End .select-custom -->



                            <a href="#" class="sorter-btn" title="Set Ascending Direction"><span class="sr-only">Set Ascending Direction</span></a>

                        </div><!-- End .toolbox-item -->

                    </div><!-- End .toolbox-left -->



                    <div class="toolbox-item toolbox-show">

                        <label>Showing 1–9 of 60 results</label>

                    </div><!-- End .toolbox-item -->



                    <div class="layout-modes">

                        <a href="category.html" class="layout-btn btn-grid active" title="Grid">

                            <i class="icon-mode-grid"></i>

                        </a>

                        <a href="category-list.html" class="layout-btn btn-list" title="List">

                            <i class="icon-mode-list"></i>

                        </a>

                    </div><!-- End .layout-modes -->

                </nav>



                <div class="row row-sm product-ajax-grid">

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                    <div class="col-6 col-md-4 col-xl-3">

                        <div class="product-default inner-quickview inner-icon">

                            <figure>

                                <a href="product.html">

                                    <img src="https://easytowndeal.com/public/upload/product/thumbnail/1627914106Pink_.jpg">

                                </a>

                                <div class="btn-icon-group">

                                    <button class="btn-icon btn-add-cart" data-toggle="modal" data-target="#addCartModal"><i class="icon-bag"></i></button>

                                </div>

                                <a href="ajax/product-quick-view.html" class="btn-quickview" title="Quick View">Quick View</a> 

                            </figure>

                            <div class="product-details">

                                <div class="category-wrap">

                                    <div class="category-list">

                                        <a href="category.html" class="product-category">category</a>

                                    </div>

                                    <a href="#" class="btn-icon-wish"><i class="icon-heart"></i></a>

                                </div>

                                <h2 class="product-title">

                                    <a href="product.html">Product Short Name</a>

                                </h2>

                                <div class="ratings-container">

                                    <div class="product-ratings">

                                        <span class="ratings" style="width:100%"></span><!-- End .ratings -->

                                        <span class="tooltiptext tooltip-top"></span>

                                    </div><!-- End .product-ratings -->

                                </div><!-- End .product-container -->

                                <div class="price-box">

                                    <span class="product-price">$49.00</span>

                                </div><!-- End .price-box -->

                            </div><!-- End .product-details -->

                        </div>

                    </div>

                </div><!-- End .row -->



                <div class="col-12 text-center loadmore mb-2">

                    <a href="#" class="btn btn-block btn-outline">Load More ...</a>

                </div>



                {{-- <nav class="toolbox toolbox-pagination">

                    <div class="toolbox-item toolbox-show">

                        <label>Showing 1–9 of 60 results</label>

                    </div><!-- End .toolbox-item -->



                    <ul class="pagination">

                        <li class="page-item disabled">

                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a>

                        </li>

                        <li class="page-item active">

                            <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>

                        </li>

                        <li class="page-item"><a class="page-link" href="#">2</a></li>

                        <li class="page-item"><a class="page-link" href="#">3</a></li>

                        <li class="page-item"><a class="page-link" href="#">4</a></li>

                        <li class="page-item"><span>...</span></li>

                        <li class="page-item"><a class="page-link" href="#">15</a></li>

                        <li class="page-item">

                            <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a>

                        </li>

                    </ul>

                </nav> --}}

            </div><!-- End .col-lg-9 -->



            {{-- <aside class="sidebar-shop col-lg-2 order-lg-first">

                <div class="sidebar-wrapper">

                    <div class="widget">

                        <h3 class="widget-title">

                            <a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">Men</a>

                        </h3>



                        <div class="collapse show" id="widget-body-2">

                            <div class="widget-body" style="overflow-y: scroll;max-height: 150px;overflow-y: scroll;">

                                <ul class="cat-list">

                                    <li><a href="#">Accessories</a></li>

                                    <li><a href="#">Watch Fashion</a></li>

                                    <li><a href="#">Tees, Knits & Polos</a></li>

                                    <li><a href="#">Pants & Denim</a></li>

                                    <li><a href="#">Pants & Denim</a></li>

                                    <li><a href="#">Pants & Denim</a></li>

                                    <li><a href="#">Pants & Denim</a></li>

                                    <li><a href="#">Pants & Denim</a></li>

                                </ul>

                            </div><!-- End .widget-body -->

                        </div><!-- End .collapse -->

                    </div><!-- End .widget -->



                    <div class="widget">

                        <h3 class="widget-title">

                            <a data-toggle="collapse" href="#widget-body-3" role="button" aria-expanded="true" aria-controls="widget-body-3">Price</a>

                        </h3>



                        <div class="collapse show" id="widget-body-3">

                            <div class="widget-body">

                                <form action="#">

                                    <div class="price-slider-wrapper">

                                        <div id="price-slider1"></div><!-- End #price-slider -->

                                    </div><!-- End .price-slider-wrapper -->



                                    <div class="filter-price-action">

                                        <button type="submit" class="btn btn-primary">Filter</button>



                                        <div class="filter-price-text">

                                            Price:

                                            <span id="filter-price-range1"></span>

                                        </div><!-- End .filter-price-text -->

                                    </div><!-- End .filter-price-action -->

                                </form>

                            </div><!-- End .widget-body -->

                        </div><!-- End .collapse -->

                    </div><!-- End .widget -->



                    <div class="widget">

                        <h3 class="widget-title">

                            <a data-toggle="collapse" href="#widget-body-4" role="button" aria-expanded="true" aria-controls="widget-body-4">Size</a>

                        </h3>



                        <div class="collapse show" id="widget-body-4">

                            <div class="widget-body">

                                <ul class="cat-list">

                                    <li><a href="#">Small</a></li>

                                    <li><a href="#">Medium</a></li>

                                    <li><a href="#">Large</a></li>

                                    <li><a href="#">Extra Large</a></li>

                                </ul>

                            </div><!-- End .widget-body -->

                        </div><!-- End .collapse -->

                    </div><!-- End .widget -->



                    <div class="widget">

                        <h3 class="widget-title">

                            <a data-toggle="collapse" href="#widget-body-5" role="button" aria-expanded="true" aria-controls="widget-body-5">Brand</a>

                        </h3>



                        <div class="collapse show" id="widget-body-5">

                            <div class="widget-body">

                                <ul class="cat-list">

                                    <li><a href="#">Adidas</a></li>

                                    <li><a href="#">Calvin Klein (26)</a></li>

                                    <li><a href="#">Diesel (3)</a></li>

                                    <li><a href="#">Lacoste (8)</a></li>

                                </ul>

                            </div><!-- End .widget-body -->

                        </div><!-- End .collapse -->

                    </div><!-- End .widget -->



                    <div class="widget">

                        <h3 class="widget-title">

                            <a data-toggle="collapse" href="#widget-body-6" role="button" aria-expanded="true" aria-controls="widget-body-6">Color</a>

                        </h3>



                        <div class="collapse show" id="widget-body-6">

                            <div class="widget-body">

                                <ul class="config-swatch-list">

                                    <li class="active">

                                        <a href="#">

                                            <span class="color-panel" style="background-color: #1645f3;"></span>

                                            <span>Blue</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="#">

                                            <span class="color-panel" style="background-color: #f11010;"></span>

                                            <span>Red</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="#">

                                            <span class="color-panel" style="background-color: #fe8504;"></span>

                                            <span>Orange</span>

                                        </a>

                                    </li>

                                    <li>

                                        <a href="#">

                                            <span class="color-panel" style="background-color: #2da819;"></span>

                                            <span>Green</span>

                                        </a>

                                    </li>

                                </ul>

                            </div><!-- End .widget-body -->

                        </div><!-- End .collapse -->

                    </div><!-- End .widget -->

                </div><!-- End .sidebar-wrapper -->

            </aside> --}}



            <!-- End .col-lg-3 -->

        </div><!-- End .row -->	

       

    </div><!-- End .container -->

    <div class="mb-5"></div><!-- margin -->

    <div class="container-fluid">

        <div class="sticky1">

           <div class="row">		

				<div class="ripple-container col-lg-6 col-md-6 col-sm-6">	

		

                 <button type="button" class="default btn primary flat results-btn border rounded-pill" data-toggle="modal" data-target="#bottom_modal">SORT</button>

			</div>	

			<div class="ripple-container col-lg-6 col-md-6 col-sm-6">						

				<button type="button" class="btn default btn primary flat results-btn border" data-toggle="modal" data-target="#filter_modal">FILTER</button>						

			</div>

	       </div>	

    	</div>

	</div>

</main>

<div class="modal modal-bottom fade" id="bottom_modal" tabindex="-1" role="dialog" aria-labelledby="bottom_modal">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-body">

      <ul class="list">

        <li class="sortby">SORT BY</li>

        <li class="">

          <div class="ripple-container ">

            <a class="default" id="">

              <span class="sortByValues"> Popularity</span>

            </a>

            <div class="ripple"></div>

          </div>

        </li>

        <li class="selected">

          <div class="ripple-container ">

            <a class="default" id="">

              <span class="sortByValues"> Latest</span>

            </a>

            <div class="ripple"></div>

          </div>

        </li>

        <li class="">

          <div class="ripple-container ">

            <a class="default">

              <span class="sortByValues"> Discount</span>

            </a>

            <div class="ripple"></div>

          </div>

        </li>

        <li class="">

          <div class="ripple-container">

            <a class="default">

              <span class="sortByValues"> Price: High to Low</span>

            </a>

            <div class="ripple"></div>

          </div>

        </li>

        <li class="">

          <div class="ripple-container ">

            <a class="default">

              <span class="sortByValues"> Price: Low to High </span>

            </a>

            <div class="ripple"></div>

          </div>

        </li>

        <li class="">

          <div class="ripple-container ">

            <a class="default">

              <span class="sortByValues"> Customer Rating </span>

            </a>

            <div class="ripple"></div>

          </div>

        </li>

      </ul>

      </div>

     

    </div>

  </div>

</div>





<div class="modal modal-bottom fade" id="filter_modal" tabindex="-1" role="dialog" aria-labelledby="filter_modal">

  <div class="modal-dialog" role="document">

      

    <div class="modal-content">

	<div class="modal-header">

			<div class="col-lg-6 col-md-6 col-sm-6">

				<h3>Filter</h3>

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6">

				 <button type="button" class="uncheck btn">Clear</button>

			</div>

      </div>

      <div class="modal-body">		

		<div class="tab">		

		

			<button class="tablinks" onclick="openCity(event, 'Gender')" id="defaultOpen">Gender</button>		  

			<button class="tablinks" onclick="openCity(event, 'Categories')">Categories</button>		

			<button class="tablinks" onclick="openCity(event, 'Size')">Size</button>		

			<button class="tablinks" onclick="openCity(event, 'Price')">Price</button>		

			<button class="tablinks" onclick="openCity(event, 'Color')">Color</button>		

			<button class="tablinks" onclick="openCity(event, 'Discount')">Discount Range</button>

			<button class="tablinks" onclick="openCity(event, 'Country')">Country Of Origin</button>

			<button class="tablinks" onclick="openCity(event, 'Rating')">Rating</button>		

			<button class="tablinks" onclick="openCity(event, 'Pattern')">Pattern</button>		

			<button class="tablinks" onclick="openCity(event, 'Sleeve')">Sleeve Length</button>		

			<button class="tablinks" onclick="openCity(event, 'Character')">Character</button>		

			<button class="tablinks" onclick="openCity(event, 'Closure')">Closure</button>		

			<button class="tablinks" onclick="openCity(event, 'Occasion')">Occasion</button>		

			<button class="tablinks" onclick="openCity(event, 'Multipack')">Multipack Set</button>		

			<button class="tablinks" onclick="openCity(event, 'Print')">Print or Pattern Type</button>

			<button class="tablinks" onclick="openCity(event, 'Washcare')">Wash care</button>		

			<button class="tablinks" onclick="openCity(event, 'Weave')">Weave Type</button>		

			<button class="tablinks" onclick="openCity(event, 'type')"> Type</button>		

			<button class="tablinks" onclick="openCity(event, 'Sport')"> Sport</button>		

			<button class="tablinks" onclick="openCity(event, 'Length')"> Length</button>		

			<button class="tablinks" onclick="openCity(event, 'Technology')">Technology</button>		

			<button class="tablinks" onclick="openCity(event, 'Neck')">Neck</button>		

			<button class="tablinks" onclick="openCity(event, 'Sustainable')">Sustainable</button>		

			<button class="tablinks" onclick="openCity(event, 'Fabric')">Fabric</button>		

			<button class="tablinks" onclick="openCity(event, 'Hood1')">Hood</button>		

		</div>	

		

		<div id="Gender" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="men">

			  <label class="custom-control-label" for="men">Men</label>

			  <label class="custom-control-label" style="float:right;" for="men">124</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="boys">

			  <label class="custom-control-label" for="boys">Boys</label>

			  <label class="custom-control-label" for="boys" style="float:right;">124</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="girls">

			  <label class="custom-control-label" for="girls">Girls</label>

			  <label class="custom-control-label" for="girls" style="float:right;">124</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="women">

			  <label class="custom-control-label" for="women">Women</label>

			  <label class="custom-control-label" for="women" style="float:right;">124</label>

			  <div class="custom-line"></div>

			</div>

		</div>			

		<div id="Categories" class="tabcontent">	

		 <div class="form-group has-search">

			<!--<span class="fa fa-search form-control-feedback"></span>-->

			<input type="text" class="form-control" placeholder="Search">

		  </div>

			<p>TOP PICKS</p>	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Sweatshirts">

			  <label class="custom-control-label" for="Sweatshirts">Sweatshirts</label>

			  <label class="custom-control-label" for="Sweatshirts" style="float:right;">469</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Sweaters">

			  <label class="custom-control-label" for="Sweaters">Sweaters</label>

			  <label class="custom-control-label" for="Sweaters" style="float:right;">469</label>

			  <div class="custom-line"></div>

			</div> 			

			<p>All Categories</p>	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Anklet">

			  <label class="custom-control-label" for="Anklet">Anklet</label>

			  <label class="custom-control-label" for="Anklet" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Bckpacks">

			  <label class="custom-control-label" for="Bckpacks">Bckpacks</label>

			  <label class="custom-control-label" for="Bckpacks" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Belts">

			  <label class="custom-control-label" for="Belts">Belts</label>

			  <label class="custom-control-label" for="Belts" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Blazers">

			  <label class="custom-control-label" for="Blazers">Blazers</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Bodysuits">

			  <label class="custom-control-label" for="Bodysuits">Bodysuit</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Boxers">

			  <label class="custom-control-label" for="Boxers">Boxers</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Bra">

			  <label class="custom-control-label" for="Bra">Bra</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Bracelet">

			  <label class="custom-control-label" for="Bracelet">Bracelet</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Caps">

			  <label class="custom-control-label" for="Caps">Caps</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Casual">

			  <label class="custom-control-label" for="Casual">Casual shoes</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Clothing">

			  <label class="custom-control-label" for="Clothing">Clothing set</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Coats">

			  <label class="custom-control-label" for="Coats">Coats</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Dresses">

			  <label class="custom-control-label" for="Dresses">Dresses</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Earrings">

			  <label class="custom-control-label" for="Earrings">Earrings</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Flats">

			  <label class="custom-control-label" for="Flats">Flats</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Formal">

			  <label class="custom-control-label" for="Formal">Formal shoes</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Hat">

			  <label class="custom-control-label" for="Hat">Hat</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Jackets">

			  <label class="custom-control-label" for="Jackets">Jackets</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Jeans">

			  <label class="custom-control-label" for="Jeans">Jeans</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Lounge">

			  <label class="custom-control-label" for="Lounge">Lounge tshirts</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Necklace">

			  <label class="custom-control-label" for="Necklace">Necklace and chains</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Sandles">

			  <label class="custom-control-label" for="Sandles">Sandles</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Shirts">

			  <label class="custom-control-label" for="Shirts">Shirts</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Shorts">

			  <label class="custom-control-label" for="Shorts">Shorts</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Socks">

			  <label class="custom-control-label" for="Socks">Socks</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

				  <input type="checkbox" class="custom-control-input" id="Sunglasses">

				  <label class="custom-control-label" for="Sunglasses">Sunglasses</label>

				  <label class="custom-control-label" style="float:right;">3</label>

				  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Suspenders">

			  <label class="custom-control-label" for="Suspenders">Suspenders</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Sweaters">

			  <label class="custom-control-label" for="Sweaters">Sweaters</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Swim">

			  <label class="custom-control-label" for="Swim">Swim bottoms</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Ties">

			  <label class="custom-control-label" for="Ties">Ties</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Tights">

			  <label class="custom-control-label" for="Tights">Tights</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Tops">

			  <label class="custom-control-label" for="Tops">Tops</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Track">

			  <label class="custom-control-label" for="Track">Track Pants</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

				  <input type="checkbox" class="custom-control-input" id="Trousers">

				  <label class="custom-control-label" for="Trousers">Trousers</label>

				  <label class="custom-control-label" style="float:right;">3</label>

				  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Trunk">

			  <label class="custom-control-label" for="Trunk">Trunk</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Tshirts">

			  <label class="custom-control-label" for="Tshirts">Tshirts</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 	

			

		</div>			

		<div id="Size" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="xs">

			  <label class="custom-control-label" for="xs">Xs</label>

			  <label class="custom-control-label" style="float:right;">152</label>

			  <div class="custom-line"></div>

			</div> 	

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="s">

			  <label class="custom-control-label" for="s">S</label>

			  <label class="custom-control-label" style="float:right;">152</label>

			  <div class="custom-line"></div>

			</div> 		

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="m">

			  <label class="custom-control-label" for="m">M</label>

			  <label class="custom-control-label" style="float:right;">152</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="L">

			  <label class="custom-control-label" for="L">L</label>

			  <label class="custom-control-label" style="float:right;">152</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="XL">

			  <label class="custom-control-label" for="XL">XL</label>

			  <label class="custom-control-label" style="float:right;">152</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="XXL">

			  <label class="custom-control-label" for="XXL">XXL</label>

			  <label class="custom-control-label" style="float:right;">152</label>

			  <div class="custom-line"></div>

			</div> 

			

		</div>

		<div id="Price" class="tabcontent">			  

			  <form action="#">

				<div class="price-slider-wrapper">

					<div id="price-slider"></div><!-- End #price-slider -->

				</div><!-- End .price-slider-wrapper -->



				<div class="filter-price-action">

					<button type="submit" class="btn btn-primary">Filter</button>



					<div class="filter-price-text">

						Price:

						<span id="filter-price-range"></span>

					</div><!-- End .filter-price-text -->

				</div><!-- End .filter-price-action -->

			</form>	

		</div>

		<div id="Color" class="tabcontent">	

			<div class="form-group has-search">

				<!--<span class="fa fa-search form-control-feedback"></span>-->

				<input type="text" class="form-control" placeholder="Search">

			 </div>		

			  <ul class="config-swatch-list">

					<li class="active">

						<a href="#">

							<span class="color-panel" style="background-color: #1645f3;"></span>

							<span>Blue</span>

						</a>

					</li>

					<li>

						<a href="#">

							<span class="color-panel" style="background-color: #f11010;"></span>

							<span>Red</span>

						</a>

					</li>

					<li>

						<a href="#">

							<span class="color-panel" style="background-color: #fe8504;"></span>

							<span>Orange</span>

						</a>

					</li>

					<li>

						<a href="#">

							<span class="color-panel" style="background-color: #2da819;"></span>

							<span>Green</span>

						</a>

					</li>

				</ul>

		</div>

		<div id="Discount" class="tabcontent">			  

			 <div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="20%">

			  <label class="custom-control-label" for="20%">20% and higher</label>

			  <label class="custom-control-label" style="float:right;">17</label>

			  <div class="custom-line"></div>

			</div>  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="30%">

			  <label class="custom-control-label" for="30%">30% and higher</label>

			  <label class="custom-control-label" style="float:right;">17</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="40%">

			  <label class="custom-control-label" for="40%">40% and higher</label>

			  <label class="custom-control-label" style="float:right;">17</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="50%">

			  <label class="custom-control-label" for="50%">50% and higher</label>

			  <label class="custom-control-label" style="float:right;">17</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Country" class="tabcontent">			  

			 <div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Countries">

			  <label class="custom-control-label" for="Countries">All Countries</label>

			  <label class="custom-control-label" style="float:right;">17</label>

			  <div class="custom-line"></div>

			</div>  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="India">

			  <label class="custom-control-label" for="India">India</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Rating" class="tabcontent">			  

			 <div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="3star">

			  <label class="custom-control-label" for="3star">3 <i class="fa fa-star"></i>& Above</label>

			  <label class="custom-control-label" style="float:right;">17</label>

			  <div class="custom-line"></div>

			</div>  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="4star">

			  <label class="custom-control-label" for="4star">4 <i class="fa fa-star"></i>& Above</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Pattern" class="tabcontent">			  

			 <div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Printed">

			  <label class="custom-control-label" for="Printed">Printed</label>

			  <label class="custom-control-label" style="float:right;">17</label>

			  <div class="custom-line"></div>

			</div>  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="SelfDesign">

			  <label class="custom-control-label" for="SelfDesign">Self Design</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Solid">

			  <label class="custom-control-label" for="Solid">Solid</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Striped">

			  <label class="custom-control-label" for="Striped">Striped</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Sleeve" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="LongSleeve">

			  <label class="custom-control-label" for="LongSleeve">Long Sleeve</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="ShortSleeve">

			  <label class="custom-control-label" for="ShortSleeve">Short Sleeve</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Character" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Nasa">

			  <label class="custom-control-label" for="Nasa">Nasa</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Tom&Jerry">

			  <label class="custom-control-label" for="Tom&Jerry">Tom & Jerry</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Clouser" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Button">

			  <label class="custom-control-label" for="Button">Button</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Zip">

			  <label class="custom-control-label" for="Zip">Zip</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Occasion" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Casual">

			  <label class="custom-control-label" for="Casual">Casual</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Outdoor">

			  <label class="custom-control-label" for="Outdoor">Outdoor</label>

			  <label class="custom-control-label" style="float:right;">5</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Multipack" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="3">

			  <label class="custom-control-label" for="3">3</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Print" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Abstract">

			  <label class="custom-control-label" for="Abstract">Abstract</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Camouflage">

			  <label class="custom-control-label" for="Camouflage">Camouflage</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Floral">

			  <label class="custom-control-label" for="Floral">Floral</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Graphics">

			  <label class="custom-control-label" for="Graphics">Graphics</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="SelfDesign">

			  <label class="custom-control-label" for="SelfDesign">Self Design</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Solid">

			  <label class="custom-control-label" for="Solid">Solid</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Striped">

			  <label class="custom-control-label" for="Striped">Striped</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Typography">

			  <label class="custom-control-label" for="Typography">Typography</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Washcare" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Handwash">

			  <label class="custom-control-label" for="Handwash">Hand Wash</label>

			  <label class="custom-control-label" style="float:right;">3</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="MachineWash">

			  <label class="custom-control-label" for="MachineWash">Machine Wash</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

		</div>	

		<div id="Weave" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Knitted">

			  <label class="custom-control-label" for="Knitted">Knitted</label>

			  <label class="custom-control-label" style="float:right;">97</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Knitted&woven">

			  <label class="custom-control-label" for="Knitted&woven">Knitted & Woven</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Woven">

			  <label class="custom-control-label" for="Woven">Woven</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div> 

		</div>	

		<div id="Type" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="FrontOpen">

			  <label class="custom-control-label" for="FrontOpen">Front Open</label>

			  <label class="custom-control-label" style="float:right;">8</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Pullover">

			  <label class="custom-control-label" for="Pullover">Pullover</label>

			  <label class="custom-control-label" style="float:right;">1</label>

			  <div class="custom-line"></div>

			</div>

			

		</div>	

		<div id="Sport" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Outdoor">

			  <label class="custom-control-label" for="Outdoor">Outdoor</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Running">

			  <label class="custom-control-label" for="Running">Running</label>

			  <label class="custom-control-label" style="float:right;">2</label>

			  <div class="custom-line"></div>

			</div>

			

		</div>	

		<div id="Length" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Regular">

			  <label class="custom-control-label" for="Regular">Regular</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

		</div>	

		<div id="Technology" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Rapid">

			  <label class="custom-control-label" for="Rapid">Rapid-Dry</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

		</div>	

		<div id="Neck" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Henley">

			  <label class="custom-control-label" for="Henley">Henley Neck</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="HighNeck">

			  <label class="custom-control-label" for="HighNeck">High Neck</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Hood">

			  <label class="custom-control-label" for="Hood">Hood</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="MockCollar">

			  <label class="custom-control-label" for="MockCollar">Mock Collar</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Round">

			  <label class="custom-control-label" for="Round">Round Neck</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="ShirtCollar">

			  <label class="custom-control-label" for="ShirtCollar">Shirt Collar</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

			

		</div>	

		<div id="Sustainable" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Regular">

			  <label class="custom-control-label" for="Regular">Regular</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Sustainable1">

			  <label class="custom-control-label" for="Sustainable1">Sustainable</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Fabric" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Cotton">

			  <label class="custom-control-label" for="Cotton">Cotton</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div>

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Polyester">

			  <label class="custom-control-label" for="Polyester">Polyester</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div> 

		</div>

		<div id="Hood1" class="tabcontent">			  

			<div class="custom-control custom-checkbox">

			  <input type="checkbox" class="custom-control-input" id="Hooded">

			  <label class="custom-control-label" for="Hooded">Hooded</label>

			  <label class="custom-control-label" style="float:right;">1	</label>

			  <div class="custom-line"></div>

			</div>

			

		</div>

		

		

      </div>

	  <div class="modal-footer">

			<div class="col-lg-6 col-md-6 col-sm-6">

				<button class="btn " type="button" data-dismiss="modal">Close</button>

			</div>

			<div class="col-lg-6 col-md-6 col-sm-6">

				<button class="btn">Apply</button>

			</div>

      </div>

    </div>

  </div>

</div>

@stop



@push('scripts')    

 <script type="text/javascript"	>



 function openCity(evt, cityName) {  var i, tabcontent, tablinks;  tabcontent = document.getElementsByClassName("tabcontent");  for (i = 0; i < tabcontent.length; i++) {    tabcontent[i].style.display = "none";  }  tablinks = document.getElementsByClassName("tablinks");  for (i = 0; i < tablinks.length; i++) {    tablinks[i].className = tablinks[i].className.replace(" active", "");  }  document.getElementById(cityName).style.display = "block";  evt.currentTarget.className += " active";}



document.getElementById("defaultOpen").click();



$(document).ready(function() {

	$(".uncheck").click(function(){

        $(".custom-control-input").prop("checked", false);

    });

var h=$(window).height();	

$('.sticky1').hide();	

if(h < 750)	

{			

$('.sticky1').show();	

}});



</script>

<script type="text/javascript">//$(document).ready(function() {	$h=$(window).height();	$('.sticky1').hide();	if($h < 800)	{			$('.sticky1').show();	}});

</script>

   

@endpush