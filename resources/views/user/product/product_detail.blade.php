@extends('user.layouts.app')
@section('title', 'Product Details')

@section('content')

<main class="main">
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Product</li>
            </ol>
        </div>
    </nav>

    <div class="container">
        <div class="row row-sm">
            <div class="col-lg-9 col-xl-10">
                <div class="product-single-container product-single-default">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 product-single-gallery">
                            <div class="product-slider-container product-item">
                                <div class="product-single-carousel owl-carousel owl-theme">
                                    @php
                                    $images = explode(',', $products->product_image);
                                    @endphp
                                    @if(!empty($images))
                                    @foreach($images as $image)
                                    <div class="product-item">
                                        <img class="product-single-image" src="{{ asset('public/upload/product/'.$image.'') }}" data-zoom-image="{{ asset('public/upload/product/'.$image.'') }}"/>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                                <span class="prod-full-screen">
                                    <i class="icon-plus"></i>
                                </span>
                            </div>
                            <div class="prod-thumbnail row owl-dots" id='carousel-custom-dots'>
                                @php
                                $images = explode(',', $products->product_image);
                                @endphp
                                @if(!empty($images))
                                @foreach($images as $image)
                                <div class="col-3 owl-dot">
                                    <img src="{{ asset('public/upload/product/thumbnail/'.$image.'') }}"/>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-6">
                            <div class="product-single-details">
                                <h1 class="product-title">{{ $products->product_name }}</h1>
                                <h3>{{ $products->product_sub_heading }}</h3>
                                <div class="ratings-container">
                                    {{-- <div class="product-ratings">
                                        <span class="ratings" style="width:100%"></span>
                                    </div> --}}
                                    {{-- <a href="#" class="rating-link">( 6 Reviews )</a> --}}
                                </div>

                                <div class="price-box">
                                    <span class="product-price">₹{{ $products->price }}</span>
                                    @if(!empty($products))
                                    @if($products->stock < 10 && $products->stock != 0)
                                    <span class="product-remain-stock">Hurry, Only {{ $products->stock }} left!</span>
                                    @elseif($products->stock == 0)
                                    <span class="product-out-stock">Currently out of stock</span>
                                    @else
                                    <span class="product-in-stock">In Stock</span>
                                    @endif
                                    @endif
                                </div><!-- End .price-box -->
                                <div class="product-desc">
                                    {!! Str::limit($products->description, 450, ' ...') !!}
                                    @if(!empty($products->description))
                                    @if(str_word_count($products->description) > 150)
                                    <a id="readmore-modal" data-toggle="modal" data-target="#exampleModal" href="javascript:void(0)">(Read more)</a>
                                    @endif
                                    @endif
                                </div><!-- End .product-desc -->

                                <div class="product-filters-container">
                                    <div class="product-single-filter">
                                        @if($products->normalsize == 1)
                                        <label>Size:</label>
                                        <ul class="config-size-list">
                                            @if(!empty($sizes))
                                            @foreach($sizes as $size)
                                            <li data-toggle="tooltip" data-size="{{ $size->sizes }}" data-stock="{{ $size->sizes_stock }}" title="{{ $size->sizes_stock }}" class="@if($size->sizes_stock != 0) {{ "normal-size-chart"}} @endif">
                                                <a class="@if($size->sizes_stock == 0) {{ "outofstock" }} @endif" href="javascript:void(0)">{{ $size->sizes }}</a>
                                            </li>
                                            @endforeach
                                            @endif
                                        </ul>
                                        @endif
                                        @if($products->shooessize == 1)
                                        <label>Size:</label>
                                        <ul class="config-size-list">
                                            @if(!empty($sizes))
                                            @foreach($sizes as $size)
                                            <li data-toggle="tooltip" data-size="{{ $size->sizes }}" data-stock="{{ $size->sizes_stock }}" title="{{ $size->sizes_stock }}" class="@if($size->sizes_stock != 0) {{ "shooes-size-chart" }} @endif">
                                                <a class="@if($size->sizes_stock == 0) {{ "outofstock" }} @endif" href="javascript:void(0)">{{ $size->sizes }}</a>
                                            </li>
                                            @endforeach
                                            @endif
                                        </ul>
                                        @endif
                                    </div>
                                </div><!-- End .product-filters-container -->

                                <div class="product-action product-all-icons">
                                    <input type="hidden" name="selectednormalsize" class="selectednormalsize">
                                    <input type="hidden" name="selectedshooessize" class="selectedshooessize">
                                    <input type="hidden" name="selectedproduct" class="selectedproduct" value="{{ $products->id }}">
                                    @if(Auth::user())
                                    <a href="javascript:void(0)" class="paction add-cart add-to-carts @if($products->stock == 0) {{ "outofstock" }} @endif" title="Add to Cart" @if($products->stock == 0) disabled @endif>
                                        Add to Cart
                                    </a>
                                    @else
                                    <button class="btn-icon btn-add-cart paction add-cart" data-toggle="modal" data-target="#loginModal">Add to Cart</button>
                                    @endif
                                </div><!-- End .product-action -->

                            </div><!-- End .product-single-details -->
                        </div><!-- End .col-lg-7 -->
                    </div><!-- End .row -->
                </div><!-- End .product-single-container -->

                <div class="product-single-tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content" role="tab" aria-controls="product-desc-content" aria-selected="true">Specification</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel" aria-labelledby="product-tab-desc">
                            <div class="product-desc-content">

                                <div class="index-tableContainer">
                                    @if(!empty($specifications))
                                    @foreach($specifications as $key_heading => $specification)
                                    <div class="index-row">
                                        <div class="index-rowKey">{!! $key_heading !!}</div>
                                        <div class="index-rowValue">{!! $specification !!}</div>
                                    </div>
                                    @endforeach
                                    @else
                                    Specifications details not available
                                    @endif
                                </div>

                            </div><!-- End .product-desc-content -->
                        </div><!-- End .tab-pane -->
                    </div><!-- End .tab-content -->
                </div><!-- End .product-single-tabs -->
            </div><!-- End .col-lg-9 -->

            <div class="sidebar-overlay"></div>
            <div class="sidebar-toggle"><i class="icon-right-open"></i></div>
            <aside class="sidebar-product col-lg-3 col-xl-2 padding-left-lg mobile-sidebar">
                <div class="sidebar-wrapper">

                    <div class="widget">
                        <h3 class="widget-title">
                            <a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true" aria-controls="widget-body-2">Featured Products</a>
                        </h3>

                        <div class="collapse show" id="widget-body-2">
                            <div class="widget-body">
                                <div class="product-intro">
                                    @if(!empty($featuresProducts))
                                    @foreach($featuresProducts as $featuresProduct)
                                    <div class="product-default left-details product-widget">
                                        <figure>
                                            <a href="{{ route('product-details',$featuresProduct->id) }}">
                                                @php
                                                $image = explode(',', $featuresProduct->product_image);
                                                @endphp
                                                @if(!empty($image))
                                                <img src="{{ asset('public/upload/product/thumbnail/'.$image[0].'') }}">
                                                @else
                                                @endif
                                            </a>
                                        </figure>
                                        <div class="product-details">
                                            <h2 class="product-title">
                                                <a data-toggle="tooltip" title="{{ $featuresProduct->product_name }}" href="{{ route('product-details',$featuresProduct->id) }}">{{ stringcutter($featuresProduct->product_name) }}</a>
                                            </h2>
                                            {{-- <div class="ratings-container">
                                                <div class="product-ratings">
                                                    <span class="ratings" style="width:100%"></span>
                                                    <span class="tooltiptext tooltip-top"></span>
                                                </div>
                                            </div> --}}
                                            <div class="price-box">
                                                <span class="product-price">₹{{ $featuresProduct->price }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div><!-- End .widget-body -->
                        </div><!-- End .collapse -->
                    </div><!-- End .widget -->
                </div>
            </aside><!-- End .col-md-3 -->
        </div><!-- End .row -->
    </div><!-- End .container -->

    <div class="with-border-top">
        <section class="container product-panel">
            <div class="section-title">
                <h2>Related Products</h2>
            </div>
            <div class="owl-carousel owl-theme" data-toggle="owl" data-owl-options="{
            'margin': 4,
            'items': 2,
            'autoplayTimeout': 5000,
            'dots': false,
            'nav' : false,
            'responsive': {
            '768': {
            'items': 3
        },
        '992' : {
        'items' : 4
    },
    '1200': {
    'items': 5
}
}
}">

@if(!empty($relatedProducts))
@foreach($relatedProducts as $relatedProduct)
<div class="product-default inner-quickview inner-icon center-details">
    <figure>
        <a href="{{ route('product-details',$relatedProduct->id) }}">
            @php
            $image = explode(',', $relatedProduct->product_image);
            @endphp
            @if(!empty($image))
            <img src="{{ asset('public/upload/product/thumbnail/'.$image[0].'') }}">
            @else
            @endif
        </a>
        <div class="btn-icon-group">
            <button class="btn-icon btn-add-cart" data-toggle="modal" @if(Auth::user()) data-target="#addWhishlistModal" id="whishlist-product" data-product-id="{{ $relatedProduct->id }}" @else data-target="#loginModal" @endif> @if(wishlist($relatedProduct->id)) <i class="icon-heart fillheart heart{{ $relatedProduct->id }}"></i> @else <i class="icon-heart heart{{ $relatedProduct->id }}"></i> @endif</button>
        </div>
        <a href="javascript:void(0)" class="btn-quickview" data-product-id="{{ $relatedProduct->id }}" title="Quick View">Quick View</a>
    </figure>
    <div class="product-details">
        <div class="category-wrap">
            <div class="category-list">
                <a href="category.html" class="product-category">{{ $relatedProduct->main_category_name }}</a>
            </div>
        </div>
        <h2 class="product-title">
            <a data-toggle="tooltip" data-placement="bottom" title="{{ $relatedProduct->product_name }}" href="{{ route('product-details',$relatedProduct->id) }}">{{ stringcutter($relatedProduct->product_name) }}</a>
        </h2>
        {{-- <div class="ratings-container">
            <div class="product-ratings">
                <span class="ratings" style="width:100%"></span>
                <span class="tooltiptext tooltip-top"></span>
            </div>
        </div> --}}
        <div class="price-box">
            <span class="product-price">₹{{ $relatedProduct->price }}</span>
        </div><!-- End .price-box -->
    </div><!-- End .product-details -->
</div>
@endforeach
@endif

</div>
</section>
</div>
</main><!-- End .main -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Product Description</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    {!! $products->description !!}
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>

@stop

@push('scripts')

<script type="text/javascript">



</script>

@endpush
