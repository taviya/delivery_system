@if(!empty($products->isNotEmpty()))

    @foreach($products as $product)
        <div class="col-6 col-md-4 col-xl-3">
            <div class="product-default inner-quickview inner-icon">
                <figure>
                    <a href="{{ route('product-details',$product->id) }}">
                        <img src="{{ asset('public/upload/product/thumbnail/'.explode(',', $product->product_image)[0]) }}">
                    </a>
                    <div class="btn-icon-group">
                        <button class="btn-icon btn-add-cart" data-toggle="modal" @if(Auth::user()) data-target="#addWhishlistModal" id="whishlist-product" data-product-id="{{ $product->id }}" @else data-target="#loginModal" @endif> @if(wishlist($product->id)) <i class="icon-heart fillheart heart{{ $product->id }}"></i> @else <i class="icon-heart heart{{ $product->id }}"></i> @endif</button>
                    </div>
                    <a href="javascript:void(0)" data-product-id="{{ $product->id }}" class="btn-quickview" title="Quick View">Quick
                        View</a>
                </figure>
                <div class="product-details">
                    <div class="category-wrap">
                        <div class="category-list">
                            <a href="javascript:void(0)" class="product-category">{{ $product->getMainCategory->main_category_name }}</a>
                        </div>
                    </div>
                    <h2 class="product-title">
                        <a data-toggle="tooltip" title="{{ $product->product_name }}" href="{{ route('product-details',$product->id) }}">{{ stringcutter($product->product_name) }}</a>
                    </h2>
                    {{--<div class="ratings-container">
                        <div class="product-ratings">
                            <span class="ratings" style="width:100%"></span>
                            <span class="tooltiptext tooltip-top"></span>
                        </div>
                    </div>--}}
                    <div class="price-box">
                        <span class="product-price">₹{{ $product->price }}</span>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

@else

    {{--    <div class="col-md-4"></div>--}}
    <div class="col-md-12 text-center">
        <div class="item" style="vertical-align: top;
    display: inline-block;
    text-align: center;">
            <img style="width: 150px;
    height: 150px;" src="{{ asset('assets/user_assets/images/no-search.png') }}"/>
            <hr style="margin: 1.5rem auto 1.2rem;">
            <span class="caption" style="display: block;">We couldn't find any matches</span>
        </div>
    </div>
    {{--    <div class="col-md-4"></div>--}}

@endif
