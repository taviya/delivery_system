@extends('user.layouts.app')
@section('title', 'Product List')

@section('content')

{{--
<div class="category-banner">
    <div class="container">
        <div class="content-left">
            <span>EXTRA</span>
            <h2>20% OFF</h2>
            <h4 class="cross-txt">BIKES</h4>
        </div>
        <div class="content-center">
            <img src="assets/user_assets/images/banners/category_banner.png">
        </div>
        <div class="content-right">
            <p>Summer Sale</p>
            <button class="btn">Shop All Sale</button>
        </div>
    </div>
</div>
--}}

<nav aria-label="breadcrumb" class="breadcrumb-nav">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Product</li>
        </ol>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <nav class="toolbox">
                <div class="toolbox-left">
                    <div class="toolbox-item toolbox-sort">
                        <div style="padding-right: 5px;">
                            <button type="button" class="form-control cursor-pointer clear_filter">Clear All</button>
                        </div>
                        <div class="select-custom">
                            <select name="order_by" class="form-control order_by">
                                <option value="shorting_default" selected="selected">Default sorting</option>
{{--                                <option value="date" selected>Sort by newness</option>--}}
                                <option value="asc">Sort by price: low to high</option>
                                <option value="desc">Sort by price: high to low</option>
                            </select>
                        </div><!-- End .select-custom -->

                        <a href="#" class="sorter-btn" title="Set Ascending Direction"><span class="sr-only">Set Ascending Direction</span></a>
                    </div>
                </div>

                <div class="toolbox-item toolbox-show">
{{--                    <label>Showing 1–9 of 60 results</label>--}}
                    <input type="search" value="" class="form-control search" placeholder="Search" aria-label="Search" />
                </div>

                {{--<div class="layout-modes">
                    <input type="search" id="form1" class="form-control" placeholder="Search" aria-label="Search" />
                    <a href="category.html" class="layout-btn btn-grid active" title="Grid">
                        <i class="icon-mode-grid"></i>
                    </a>
                    <a href="category-list.html" class="layout-btn btn-list" title="List">
                        <i class="icon-mode-list"></i>
                    </a>
                </div>--}}
            </nav>

            <!-- Image loader -->
            <!-- Image loader -->

            <div class="row row-sm product-ajax-grid">
{{--                <div class="col-sm-12" id='loader' style='display: none;'>--}}
                <div class="col-sm-12">
{{--                    <img src='{{ asset('assets/images/loader1.gif') }}'>--}}
                    <div class="loading" id='loader'>Loading&#8230;</div>
                </div>
                <div class="col-sm-12">
                    <div class="row append_product" id="product_list_id">
{{--                        ajax response--}}
                    </div>
                </div>
            </div>

            <div class="col-12 text-center mb-2 load_more">
                <button class="btn btn-block btn-outline">Load More ...</button>
            </div>

            <div class="col-12 text-center loading_btn mb-2">
                <button class="btn btn-block btn-outline">Loading ...</button>
            </div>

            {{--<nav class="toolbox toolbox-pagination">
                <div class="toolbox-item toolbox-show">
                    <label>Showing 1–9 of 60 results</label>
                </div>

                <ul class="pagination">
                    <li class="page-item disabled">
                        <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a>
                    </li>
                    <li class="page-item active">
                        <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                    <li class="page-item"><span>...</span></li>
                    <li class="page-item"><a class="page-link" href="#">15</a></li>
                    <li class="page-item">
                        <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a>
                    </li>
                </ul>
            </nav>--}}
        </div>

        <aside class="sidebar-shop col-lg-2 order-lg-first">
            <form action="" method="POST" id="product_form">
                <div class="sidebar-wrapper">
                    <div class="widget product_widget">
                        <h3 class="widget-title">
                            <a data-toggle="collapse" href="#widget-body-1" role="button" aria-expanded="true"
                               aria-controls="widget-body-1">Price</a>
                        </h3>

                        <div class="collapse show" id="widget-body-1">
                            <div class="widget-body">
                                <div class="price-slider-wrapper">
                                    <div id="steps-slider"></div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-md-12">
                                        <input type="text" name="min" id="input-with-keypress-0" value="" class="form-control" readonly>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="text" name="max" id="input-with-keypress-1" value="20000" class="form-control"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--Ajax response--}}
                    <div class="filter_append">

                    </div>
                </div>
            </form>
        </aside>
    </div>
</div>

<div class="mb-5"></div><!-- margin -->

@stop

@push('scripts')

    <script>
        var isProdUrl = '{{ $isProdUrl }}';
        var isCatUrl = parseInt('{{ $isCatUrl }}');
        var currentUrl = '{{ url()->current() }}';
        if(isCatUrl){
            window.history.pushState("object or string", "Title", currentUrl);
        }

        var globlPageCount = 0;
        var stepsSlider = document.getElementById('steps-slider');
        var input0 = document.getElementById('input-with-keypress-0');
        var input1 = document.getElementById('input-with-keypress-1');
        var inputs = [input0, input1];

        noUiSlider.create(stepsSlider, {
            start: [0, 50000],
            connect: true,
            // tooltips: [true, wNumb({decimals: 1})],
            range: {
                'min': [0],
                'max': 50000
            }
        });

        stepsSlider.noUiSlider.on('change', function (values, handle) {
            globlPageCount = 0;
            applyFilter();
        });

        stepsSlider.noUiSlider.on('update', function (values, handle) {
            inputs[handle].value = values[handle];
        });
        /*var stepsSlider = document.getElementById('steps-slider');
        var input0 = document.getElementById('input-with-keypress-0');
        var input1 = document.getElementById('input-with-keypress-1');
        var inputs = [input0, input1];

        noUiSlider.create(stepsSlider, {
            start: [0, 15000],
            connect: true,
            // tooltips: [true, wNumb({decimals: 1})],
            range: {
                'min': [0],
                'max': 50000
            }
        });

        stepsSlider.noUiSlider.on('change', function (values, handle) {
            globlPageCount = 0;
            applyFilter();
        });

        stepsSlider.noUiSlider.on('update', function (values, handle) {
            inputs[handle].value = values[handle];
        });*/

        // Listen to keydown events on the input field.
        /*inputs.forEach(function (input, handle) {

            input.addEventListener('change', function () {
                stepsSlider.noUiSlider.setHandle(handle, this.value);
            });

            input.addEventListener('keydown', function (e) {

                var values = stepsSlider.noUiSlider.get();
                var value = Number(values[handle]);

                // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
                var steps = stepsSlider.noUiSlider.steps();

                // [down, up]
                var step = steps[handle];

                var position;

                // 13 is enter,
                // 38 is key up,
                // 40 is key down.
                switch (e.which) {

                    case 13:
                        stepsSlider.noUiSlider.setHandle(handle, this.value);
                        break;

                    case 38:

                        // Get step to go increase slider value (up)
                        position = step[1];

                        // false = no step is set
                        if (position === false) {
                            position = 1;
                        }

                        // null = edge of slider
                        if (position !== null) {
                            stepsSlider.noUiSlider.setHandle(handle, value + position);
                        }

                        break;

                    case 40:

                        position = step[0];

                        if (position === false) {
                            position = 1;
                        }

                        if (position !== null) {
                            stepsSlider.noUiSlider.setHandle(handle, value - position);
                        }

                        break;
                }
            });
        });*/

        $(".clear_filter").click(function () {
            location.reload();
        });

        $(".load_more").click(function () {
            applyFilter();
        });

        /*$(".fm_check").change(function() {
            globlPageCount = 0;
            applyFilter();
        });*/

        $(".order_by").change(function() {
            globlPageCount = 0;
            applyFilter();
        });

        $(document).on('change','.fm_check',function(){
            globlPageCount = 0;
            applyFilter($(this).data('section'));
        });

        $(".search").keyup(function(){
            globlPageCount = 0;
            applyFilter();
        });

        applyFilter();

        function applyFilter(section = null) {
            var order_by = $('.order_by').val();
            var search = $('.search').val();
            if(isProdUrl != '') {
                search = isProdUrl;
                window.history.pushState("object or string", "Title", currentUrl);
                isProdUrl = '';
            }
            // $('#prod_url_data').val('');
            var serialize_data = $('#product_form').serialize()+ '&order_by=' + order_by+ '&search=' + search+ '&globlPageCount=' + globlPageCount+ '&section=' + section;

            $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('get-product')}}",
                    type: "POST",
                    data: serialize_data,
                    beforeSend: function () {
                        $('.append_product').hide();
                        $("#loader").show();
                        $('.load_more').css("display","none");
                        $('.loading_btn').css("display","block");
                    },
                    dataType: 'json',
                    success: function (response) {
                        if(response.status) {
                            globlPageCount = globlPageCount + 2;
                            $('.append_product').show();
                            $('.filter_append').html(response.product_filter_render);
                            $('.append_product').html(response.append_product);
                            $(window).trigger('resize');

                            $('.loading_btn').css("display","none");
                            if (response.isLoadShow) {
                                $('.load_more').css("display","block");
                            } else {
                                $('.load_more').css("display","none");
                            }
                        }
                    },
                    complete: function (data) {
                        // Hide image container
                        $("#loader").hide();
                    },
                    error: function (response) {
                        console.log(response);
                    },
                });
        }
    </script>

@endpush
