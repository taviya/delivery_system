
    @if(!empty($mainCategory))
        <div class="widget product_widget">
            <h3 class="widget-title">
                <a data-toggle="collapse" href="#widget-body-2" role="button" aria-expanded="true"
                   aria-controls="widget-body-2">Main Category</a>
            </h3>

            <div class="collapse show" id="widget-body-2">
                <div class="widget-body">
                    <ul class="config-swatch-list">
                        @foreach($mainCategory as $singleCat)
                            <li>
                                <input type="checkbox" class="color-panel fm_check" id="category_{{ $singleCat->id }}" name="main_category[]" value="{{ $singleCat->id }}" data-section="main_category">
                                <label href="javascript:void(0);" for="category_{{ $singleCat->id }}">{{ $singleCat->main_category_name }}</label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    @if(!empty($subCategory))
        <div class="widget product_widget">
            <h3 class="widget-title">
                <a data-toggle="collapse" href="#widget-body-3" role="button" aria-expanded="true" aria-controls="widget-body-3">Sub Category</a>
            </h3>

            <div class="collapse show" id="widget-body-3">
                <div class="widget-body">
                    <ul class="config-swatch-list">
                        @foreach($subCategory as $subCatKey => $singleSubCat)
                            <li>
                                <input type="checkbox" class="color-panel fm_check" id="sub_cat_{{ $subCatKey }}" name="sub_category[]" value="{{ $singleSubCat->id }}" data-section="sub_category" {{ ($singleSubCat->id == 11 || $singleSubCat->id == 12 ? 'checked="checked"' : '')  }}}}>
                                <label href="javascript:void(0);" for="sub_cat_{{ $subCatKey }}">{{ $singleSubCat->sub_category_name }}</label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    @if(!empty($clothingSize))
        <div class="widget product_widget">
            <h3 class="widget-title">
                <a data-toggle="collapse" href="#widget-body-4" role="button" aria-expanded="true" aria-controls="widget-body-4">Size</a>
            </h3>

            <div class="collapse show" id="widget-body-4">
                <div class="widget-body">
                    <ul class="config-swatch-list">
                        @foreach($clothingSize as $clothingSize => $cSize)
                            <li>
                                <input type="checkbox" class="color-panel fm_check" id="clothing_size_{{ $clothingSize }}" name="clothing_size[]" value="{{ $cSize }}" data-section="clothing_size">
                                <label href="javascript:void(0);" for="clothing_size_{{ $clothingSize }}">{{ $cSize }}</label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif

    @if(!empty($colors))
        <div class="widget product_widget">
            <h3 class="widget-title">
                <a data-toggle="collapse" href="#widget-body-5" role="button" aria-expanded="true"
                   aria-controls="widget-body-5">Color</a>
            </h3>

            <div class="collapse show" id="widget-body-5">
                <div class="widget-body">
                    <ul class="config-swatch-list">
                        @foreach($colors as $colorKey => $color)
                            <li>
                                <input type="checkbox" class="color-panel fm_check" id="color_{{ $colorKey }}" name="color[]" value="{{ $color }}" style="background-color: #f11010;" data-section="color">
                                <label href="javascript:void(0);" for="color_{{ $colorKey }}">{{ ucfirst($color) }}</label>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    @endif
