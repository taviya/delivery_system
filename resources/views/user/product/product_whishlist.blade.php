@extends('user.layouts.app')
@section('title', 'WishList')

@section('content')


<nav aria-label="breadcrumb" class="breadcrumb-nav">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
            <li class="breadcrumb-item active wishlist-page" aria-current="page">Whishlist</li>
        </ol>
    </div>
</nav>
<div class="container">
    <div class="row row-sm">
        <div class="col-lg-12">

            <div class="row row-sm">
                @if(!empty($products->isNotEmpty()))

                @foreach($products as $product)
                <div class="col-6 col-md-4 col-xl-3 product-remove{{ $product->id }}">
                    <div class="product-default inner-quickview inner-icon">
                        <figure>
                            <a href="{{ route('product-details',$product->id) }}">
                                <img src="{{ asset('public/upload/product/thumbnail/'.explode(',', $product->product_image)[0]) }}">
                            </a>
                            <div class="btn-icon-group">
                                <button class="btn-icon btn-add-cart" data-toggle="modal" @if(Auth::user()) data-target="#addWhishlistModal" id="whishlist-product" data-product-id="{{ $product->id }}" @else data-target="#loginModal" @endif> @if(wishlist($product->id)) <i class="icon-heart fillheart heart{{ $product->id }}"></i> @else <i class="icon-heart heart{{ $product->id }}"></i> @endif</button>
                            </div>
                            <a href="javascript:void(0)" class="btn-quickview" data-product-id="{{ $product->id }}" title="Quick View">Quick
                            View</a>
                        </figure>
                        <div class="product-details">
                            <div class="category-wrap">
                                <div class="category-list">
                                    <a href="javascript:void(0)" class="product-category">{{ $product->getMainCategory->main_category_name }}</a>
                                </div>
                            </div>
                            <h2 class="product-title">
                                <a data-toggle="tooltip" title="{{ $product->product_name }}" href="{{ route('product-details',$product->id) }}">{{ stringcutter($product->product_name) }}</a>
                            </h2>

                            <div class="price-box">
                                <span class="product-price">₹{{ $product->price }}</span>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                @else

                {{--    <div class="col-md-4"></div>--}}
                <div class="col-md-12 text-center">
                    <div class="item" style="vertical-align: top;
                    display: inline-block;
                    text-align: center;">
                    <img style="width: 150px;
                    height: 150px;" src="{{ asset('assets/user_assets/images/no-search.png') }}"/>
                    <hr style="margin: 1.5rem auto 1.2rem;">
                    <span class="caption" style="display: block;">We couldn't find any wishlist product</span>
                </div>
            </div>
            {{--    <div class="col-md-4"></div>--}}

            @endif
        </div><!-- End .row -->

        <nav class="toolbox toolbox-pagination">
            {{ $products->links() }}

           {{--  <ul class="pagination">
                <li class="page-item disabled">
                    <a class="page-link page-link-btn" href="#"><i class="icon-angle-left"></i></a>
                </li>
                <li class="page-item active">
                    <a class="page-link" href="#">1 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><span>...</span></li>
                <li class="page-item"><a class="page-link" href="#">15</a></li>
                <li class="page-item">
                    <a class="page-link page-link-btn" href="#"><i class="icon-angle-right"></i></a>
                </li>
            </ul> --}}
        </nav>
    </div><!-- End .col-lg-9 -->

</div><!-- End .row -->
</div><!-- End .container -->



<div class="mb-5"></div><!-- margin -->

@stop

@push('scripts')


@endpush
