@extends('user.layouts.app')
@if(!empty($page))
@section('title', $page->title)
@endif
@section('content')

<main class="main">
	<div class="page-header page-header-bg" style="background-image: url('assets/images/banners/banner-top.jpg');">
		<div class="container">
			<h1>@if(!empty($page)){{ $page->title }}@endif</h1>
		</div><!-- End .container -->
	</div><!-- End .page-header -->

	<nav aria-label="breadcrumb" class="breadcrumb-nav mt-0">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">@if(!empty($page)){{ $page->title }}@endif</li>
			</ol>
		</div>
	</nav>

	<div class="about-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2 class="subtitle" style="text-align: center;">@if(!empty($page)){{ $page->title }}@endif</h2>
					@if(!empty($page)){!! $page->body !!}@endif
				</div><!-- End .col-lg-7 -->
			</div><!-- End .row -->
		</div><!-- End .container -->
	</div><!-- End .about-section -->






</main><!-- End .main -->

@stop

@push('scripts')


@endpush