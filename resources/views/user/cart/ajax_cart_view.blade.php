<div class="dropdownmenu-wrapper">
    <div class="dropdown-cart-header">
        <span>{{ Auth::user() ? count(cartProducts()) : 0 }} Items</span>
        <a href="{{ route('cart') }}">View Cart</a>
    </div>
    <div class="dropdown-cart-products">
        @if(!empty($products))
            @if(count($products) > 0)
                @foreach($products as $product)
                    <div class="product">
                        <div class="product-details">
                            <h4 class="product-title">
                                <a href="{{ route('product-details',$product->id) }}">{{ $product->product_name }}</a>
                            </h4>
                            <span class="cart-product-info">
                            <span class="cart-product-qty">{{ $product->sel_quantity }}</span>
                            x ₹{{ $product->price }}
                            </span><br>
                            <span>Size: {{ $product->sel_size }}</span>
                        </div>
                        <figure class="product-image-container">
                            <a href="{{ route('product-details',$product->id) }}" class="product-image">
                                <img src="{{ asset('public/upload/product/thumbnail/'.explode(',', $product->product_image)[0]) }}" alt="product">
                            </a>
{{--                            <a data-product-id="{{ $product->id }}" data-cart-id="{{ $product->cart_id }}" href="javascript:void(0)" title="Remove product" class="btn-remove."><span class="sr-only">Remove</span></a>--}}
                            <a data-ajx-product-id="{{ $product->id }}" data-ajx-cart-id="{{ $product->cart_id }}" href="javascript:void(0)" class="ajax-cart-remove ajax-cart-remove" title="Remove Product"><i class="icon-retweet"></i></a>
                        </figure>
                    </div>
                @endforeach
            @else
                <div class="product">
                    <div class="product-details">
                        <span class="cart-product-qty">{{ 'Your cart is empty!' }}</span>
                    </div>
                </div>
            @endif
        @endif
    </div>
    <div class="dropdown-cart-total">
        <span>Total</span>
        <span class="cart-total-price">₹{{ subtotal() }}</span>
    </div>
    @if(!empty($products))
        @if(count($products) > 0)
            <div class="dropdown-cart-action">
                <a href="{{ route('checkout-shipping') }}" class="btn btn-block">Checkout</a>
            </div>
        @endif
    @endif
</div>
