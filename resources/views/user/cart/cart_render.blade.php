<style type="text/css">
    .qty {
        width: 40px;
        height: 25px;
        text-align: center;
    }
    input.qtyplus { width:25px; height:25px;}
    input.qtyminus { width:25px; height:25px;}
</style>
<div class="row">
    <div class="col-lg-8">
        <div class="cart-table-container">
            <table class="table table-cart">
                <thead>
                    <tr>
                        <th class="product-col">Product</th>
                        <th class="price-col">Price</th>
                        <th class="qty-col">Qty</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($products))
                    @if(count($products) > 0)
                    @foreach($products as $product)
                    <tr class="product-row">
                        <td class="product-col">
                            <figure class="product-image-container">
                                <a href="{{ route('product-details',$product->id) }}" class="product-image">
                                    <img src="{{ asset('public/upload/product/thumbnail/'.explode(',', $product->product_image)[0]) }}">
                                </a>
                            </figure>
                            <h2 class="product-title">
                                <a data-toggle="tooltip" title="{{ $product->product_name }}" href="{{ route('product-details',$product->id) }}">{{ stringcutter(ucfirst($product->product_name)) }}</a>
                            </h2>
                            <h2 class="product-title" style="margin-left: 10px;">
                                <a href="javascript:void(0)">Size: {{ $product->sel_size }}</a>
                            </h2>
                        </td>
                        <td>₹{{ $product->price }}</td>
                        <td>
                            <input data-product-id="{{ $product->id }}" data-cart-id="{{ $product->cart_id }}" type='button' value='-' class='qtyminus minus' field='quantity' min="1"/>
                            <input data-product-id="{{ $product->id }}" data-cart-id="{{ $product->cart_id }}" type='text' name='quantity' class='qty' value="{{ $product->sel_quantity }}" min="1"/>
                            <input data-product-id="{{ $product->id }}" data-cart-id="{{ $product->cart_id }}" type='button' value='+' class='qtyplus plus' field='quantity' min="1"/>
                        </td>
                        <td>₹{{ withquantity($product->price,$product->id,$product->cart_id) }}</td>
                    </tr>
                    <tr class="product-action-row">
                        <td colspan="4" class="clearfix">
                            <div class="float-left">
                                <a href="javascript:void(0)" data-product-id="{{ $product->id }}" data-cart-id="{{ $product->cart_id }}" class="btn-move movetowishlist">Move to Wishlist</a>
                            </div><!-- End .float-left -->

                            <div class="float-right">
                                <a data-product-id="{{ $product->id }}" data-cart-id="{{ $product->cart_id }}" href="javascript:void(0)" title="Remove product" class="btn-remove"><span class="sr-only">Remove</span></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr class="product-row">
                        <td class="product-col"></td>
                        <td>{{ 'Your cart is empty!' }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endif
                    @endif
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="4" class="clearfix">
                            <div class="float-right">
                                @if(count($products) > 0)
                                <a href="#" class="btn btn-outline-secondary btn-clear-cart">Clear Shopping Cart</a>
                                @endif
                                <a href="{{ route('product-list') }}" class="btn btn-outline-secondary">Continue Shopping</a>
                            </div><!-- End .float-right -->
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div><!-- End .cart-table-container -->

        <div class="cart-discount">
            <h4>Apply Discount Code</h4>
            <form action="#">
                <div class="input-group">
                    <input type="text" class="form-control form-control-sm" placeholder="Enter discount code"  required>
                    <div class="input-group-append">
                        <button class="btn btn-sm btn-primary" type="submit">Apply Discount</button>
                    </div>
                </div><!-- End .input-group -->
            </form>
        </div><!-- End .cart-discount -->
    </div><!-- End .col-lg-8 -->

    <div class="col-lg-4">
        <div class="cart-summary">
            <h3>Summary</h3>
            <!-- End #total-estimate-section -->

            <table class="table table-totals">
                <tbody>
                    <tr>
                        <td>Subtotal</td>
                        <td>₹{{ subtotal() }}</td>
                    </tr>

                    {{-- <tr>
                        <td>GST</td>
                        <td>₹{{ gstPrice() }}</td>
                    </tr> --}}
                </tbody>
                <tfoot>
                    <tr>
                        <td>Order Total</td>
                        <td>₹{{ grandtotal() }}</td>
                    </tr>
                </tfoot>
            </table>

            <div class="checkout-methods">
                <a href="{{ route('checkout-shipping') }}" class="btn btn-block btn-sm btn-primary">Go to Checkout</a>
            </div><!-- End .checkout-methods -->
        </div><!-- End .cart-summary -->
    </div><!-- End .col-lg-4 -->
</div>