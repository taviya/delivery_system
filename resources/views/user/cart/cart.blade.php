@extends('user.layouts.app')
@section('title', 'Cart')

@section('content')

<main class="main">

    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Shopping Cart</li>
            </ol>
        </div>
    </nav>

    <div class="container">
        <div id="rendervalues"></div>
    </div>
</main>

<div class="modal fade" id="addCartModal" tabindex="-1" role="dialog" aria-labelledby="addCartModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body add-cart-box text-center">
                <p>Are you sure <span class="cartmsg"></span> you want to clear <br>Cart:</p>
                <div class="btn-actions">
                    <a href="javascript:void(0)">
                        <button class="btn-primary" data-dismiss="modal">No</button>
                    </a>
                    <a href="javascript:void(0)">
                        <button class="btn-primary clear-cart" data-dismiss="modal">Yes</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="removeProductModal" tabindex="-1" role="dialog" aria-labelledby="removeProductModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body add-cart-box text-center">
                <p>Are you sure <span class="cartmsg"></span> you want to remove <br>this product:</p>
                <div class="btn-actions">
                    <input type="hidden" id="remove-product-id">
                    <input type="hidden" id="remove-cart-id">
                    <a href="javascript:void(0)">
                        <button class="btn-primary" data-dismiss="modal">No</button>
                    </a>
                    <a href="javascript:void(0)">
                        <button class="btn-primary clear-product" data-dismiss="modal">Yes</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="movewishlist" tabindex="-1" role="dialog" aria-labelledby="movewishlist" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body add-cart-box text-center">
                <p>Are you sure <span class="cartmsg"></span> you want to move to <br>Wishlist:</p>
                <div class="btn-actions">
                    <input type="hidden" id="remove-product-id">
                    <input type="hidden" id="remove-cart-id">
                    <a href="javascript:void(0)">
                        <button class="btn-primary" data-dismiss="modal">No</button>
                    </a>
                    <a href="javascript:void(0)">
                        <button class="btn-primary move-wishlist" data-dismiss="modal">Yes</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@push('scripts')

<script type="text/javascript">

    $(document).ready(function() {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url : user_base_url+'/get-cart',
            type: "POST",
            data: {  },
            dataType: 'json',
            beforeSend: function() {
                $('#loading').show();
            },
            success: function (data) {
                $('#loading').hide();
                if (data.status == 1) {
                    $('#rendervalues').html(data.data);
                }
            },
            error: function (data) {
                $('#loading').hide();
            },
        });

        $('body').on('click', '.plus', function() {
            let $input = $(this).prev('input.qty');
            let val = parseInt($input.val());
            $input.val( val+1 ).change();

            var productId = $(this).data('product-id');
            var cartId = $(this).data('cart-id');
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : user_base_url+'/get-cart',
                type: "POST",
                data: { 'productId': productId, 'quantity':val+1, 'cart_id':cartId },
                dataType: 'json',
                beforeSend: function() {
                    $('.loading-overlay').addClass('loader');
                },
                success: function (data) {
                    $('.loading-overlay').removeClass('loader');
                    if (data.status == 1) {
                        $('#rendervalues').html(data.data);
                    }
                },
                error: function (data) {
                    $('#loading').hide();
                },
            });

        });

        $('body').on('click', '.minus', function(e) {
            let $input = $(this).next('input.qty');
            var val = parseInt($input.val());
            
            if(val >= 2) {
                if (val > 1) {
                    $input.val( val-1 ).change();
                }

                var productId = $(this).data('product-id');
                var cartId = $(this).data('cart-id');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : user_base_url+'/get-cart',
                    type: "POST",
                    data: { 'productId': productId, 'quantity':val-1, 'cart_id':cartId },
                    dataType: 'json',
                    beforeSend: function() {
                        $('.loading-overlay').addClass('loader');
                    },
                    success: function (data) {
                        $('.loading-overlay').removeClass('loader');
                        if (data.status == 1) {
                            $('#rendervalues').html(data.data);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                    },
                });
            }
        });

        $('body').on('keyup', '.qty', function(e) {

            var productId = $(this).data('product-id');
            var cartId = $(this).data('cart-id');
            var quantity = $(this).val();

            if(quantity >= 1) {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : user_base_url+'/get-cart',
                    type: "POST",
                    data: { 'productId': productId, 'quantity': quantity, 'cart_id':cartId },
                    dataType: 'json',
                    beforeSend: function() {
                        $('.loading-overlay').addClass('loader');
                    },
                    success: function (data) {
                        $('.loading-overlay').removeClass('loader');
                        if (data.status == 1) {
                            $('#rendervalues').html(data.data);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                    },
                });
            }
        });

        $('body').on('click', '.btn-clear-cart', function(e) {
            $('#addCartModal').modal('show');
        });

        $('body').on('click', '.btn-remove', function(e) {
            var productId = $(this).data('product-id');
            var cartId = $(this).data('cart-id');
            $('#remove-product-id').val(productId);
            $('#remove-cart-id').val(cartId);
            $('#removeProductModal').modal('show');
        });

        $('body').on('click', '.movetowishlist', function(e) {
            var productId = $(this).data('product-id');
            var cartId = $(this).data('cart-id');
            $('#remove-product-id').val(productId);
            $('#remove-cart-id').val(cartId);
            $('#movewishlist').modal('show');
        });

        $('body').on('click', '.clear-cart', function(e) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : user_base_url+'/get-cart',
                type: "POST",
                data: { 'clearcart': 1 },
                dataType: 'json',
                beforeSend: function() {

                    $('.loading-overlay').addClass('loader');
                },
                success: function (data) {
                    $('.loading-overlay').removeClass('loader');
                    if (data.status == 1) {
                        $('#rendervalues').html(data.data);
                        message('Cart Clear Successfully', 'success');
                    }
                },
                error: function (data) {
                    $('#loading').hide();
                },
            });
        });

        $('body').on('click', '.clear-product', function(e) {

            var oneproductId = $('#remove-product-id').val();
            var cartId = $('#remove-cart-id').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : user_base_url+'/get-cart',
                type: "POST",
                data: { 'clearproduct': 1, 'oneproductId':oneproductId, 'cartId':cartId },
                dataType: 'json',
                beforeSend: function() {
                    $('.loading-overlay').addClass('loader');
                },
                success: function (data) {
                    $('.loading-overlay').removeClass('loader');
                    if (data.status == 1) {
                        $('#rendervalues').html(data.data);
                        message('Product Remove Successfully', 'success');
                    }
                },
                error: function (data) {
                    $('#loading').hide();
                },
            });
        });

        $('body').on('click', '.move-wishlist', function(e) {

            var oneproductId = $('#remove-product-id').val();
            var cartId = $('#remove-cart-id').val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url : user_base_url+'/get-cart',
                type: "POST",
                data: { 'movewishlist': 1, 'oneproductId':oneproductId, 'cartId':cartId },
                dataType: 'json',
                beforeSend: function() {
                    $('.loading-overlay').addClass('loader');
                },
                success: function (data) {
                    $('.loading-overlay').removeClass('loader');
                    if (data.status == 1) {
                        $('#rendervalues').html(data.data);
                        message('Product Move Successfully', 'success');
                        location.reload();
                    }
                },
                error: function (data) {
                    $('#loading').hide();
                },
            });
        });
    });
</script>

@endpush