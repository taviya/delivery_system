@extends('user.layouts.app')
@section('title', 'Home Page')

@section('content')

<style>
.img-circle{  
   display:block;  
   border-radius: 100px; 
   border: 3px #59b7b4 solid; 
   margin-bottom: 2rem !important;
 }
.img-circle::after {
  content: "";
  display: block;
  padding-bottom: 100%;
}
.circle {
    background-color: #f7f7f7;
    padding: 6px;
}
.img_title {
    margin-top: 5px;
    text-align: center;
    text-transform: uppercase;
    color:#000000;
}
@media screen and (max-width: 600px) {

    .circle { padding: 3px; }
    .img-circle{    border: 2px #59b7b4 solid; 
    margin-bottom: 2rem !important;}
    .img_title h4 { font-size: 1.2rem; } 
} 
</style>
<div class="top-slider owl-carousel owl-theme" data-toggle="owl" data-owl-options="{
'items' : 1,
'margin' : 0,
'nav': true,
'dots': false,
'autoplay': false
}">
<div class="home-slide">
    <div class="slide-content flex-column flex-lg-row">
        <div class="content-left mx-auto mr-lg-0 py-5">
            <span>EXTRA</span>
            <h2>20% OFF</h2>
            <h4 class="cross-txt">BIKES</h4>
            <h3 class="mb-2 mb-lg-8">Summer Sale</h3>
            <button class="btn">Shop All Sale</button>
        </div>
        <div class="image-container mx-auto py-5">
            <img src="{{ asset('assets/user_assets/images/slider/slide2.png') }}" class="slide-img1"
            alt="slide image">
            <div class="image-info mt-2 mt-lg-6 flex-column flex-sm-row">
                <div class="info-left">
                    <h4>only <span><sup>$</sup>399<sup>99</sup></span></h4>
                </div>
                <div class="info-right">
                    <h4>Start Shopping Right Now</h4>
                    <p>*Get Plus Discount Buying Package</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="home-slide">
    <div class="slide-content flex-column flex-lg-row">
        <img src="{{ asset('assets/user_assets/images/slider/slide1.png') }}" class="mx-auto mr-lg-0 py-5"
        alt="slide image">
        <div class="content-right order-first order-lg-1 mx-auto py-5">
            <span>EXTRA</span>
            <h2>20% OFF</h2>
            <h4 class="cross-txt">BIKES</h4>
            <h3 class="mb-2 mb-lg-8">Summer Sale</h3>
            <button class="btn">Shop All Sale</button>
        </div>
    </div>
</div>
</div>

<div class="container">
    <section class="product-panel"> 
            <div class="container-fluid">             
                <div class="section-title">                
                    <h2>Top picks for you</h2>            
                </div>          
                <div class="row mb-2">
                    @if(!empty($subCategories))
                    @foreach($subCategories as $main_categorie)
                        <div class="col-lg-2 col-md-2 col-4">            
                            <div class="circle"> 
                                <img src="{{ asset('public/upload/category/thumbnail/'.$main_categorie->subcategory_icon.'') }}" class="img-circle img-responsive">
                                <div class="img_title"><h4><a href="{{ route('demolisting') }}">{{ $main_categorie->sub_category_name }}</a></h4> </div>       
                            </div> 
                        </div>
                    @endforeach       
                    @endif                               
                 </div>
            </div> 
        </section> 
    <section class="product-panel">
        <div class="section-title">
            <h2>Most Popular Products</h2>
        </div>
        <div class="owl-carousel owl-theme" data-toggle="owl" data-owl-options="{
        'margin': 4,
        'items': 2,
        'autoplayTimeout': 5000,
        'dots': false,
        'nav' : true,
        'responsive': {
        '768': {
        'items': 3
    },
    '992' : {
    'items' : 4
},
'1200': {
'items': 5
}
}
}">
@if(!empty($products))
@foreach($products as $product)
<div class="product-default inner-quickview inner-icon center-details">
    <figure>
        <a href="{{ route('product-details',$product->id) }}">
            @php
            $image = explode(',', $product->product_image);
            @endphp
            @if(!empty($image))
            <img src="{{ asset('public/upload/product/thumbnail/'.$image[0].'') }}">
            @else
            @endif
        </a>
        <div class="label-group">
            <div class="product-label label-black" data-toggle="tooltip" title="stock">{{ $product->stock }}</div>
        </div>
        <div class="btn-icon-group">
            <button class="btn-icon btn-add-cart" data-toggle="modal" @if(Auth::user()) data-target="#addWhishlistModal" id="whishlist-product" data-product-id="{{ $product->id }}" @else data-target="#loginModal" @endif> @if(wishlist($product->id)) <i class="icon-heart fillheart heart{{ $product->id }}"></i> @else <i class="icon-heart heart{{ $product->id }}"></i> @endif</button>
        </div>
        <a href="javascript:void(0)" class="btn-quickview" data-product-id="{{ $product->id }}" title="Quick View">Quick
        View</a>
    </figure>
    <div class="product-details">
        <div class="category-wrap">
            <div class="category-list">
                <a href="category.html" class="product-category">{{ ucfirst($product->main_category_name) }}</a>
            </div>
        </div>
        <h2 class="product-title">
            <a data-toggle="tooltip" data-placement="bottom" title="{{ $product->product_name }}" href="{{ route('product-details',$product->id) }}">{{ stringcutter(ucfirst($product->product_name)) }}</a>
        </h2>
                        {{-- <div class="ratings-container">
                            <div class="product-ratings">
                                <span class="ratings" style="width:100%"></span>
                                <span class="tooltiptext tooltip-top"></span>
                            </div>
                        </div> --}}
                        <div class="price-box">
                            <span class="product-price">₹{{ $product->price }}</span>
                        </div><!-- End .price-box -->
                    </div><!-- End .product-details -->
                </div>
                @endforeach
                @endif
            </div>
        </section>

        <!-- You can start here -->
           
          
        
        
        <!-- End Here -->
    </div>

    

    

@stop
