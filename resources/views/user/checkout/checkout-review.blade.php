@extends('user.layouts.app')
@section('title', 'Checkout review')

@section('content')

<main class="main">

    <nav aria-label="breadcrumb" class="breadcrumb-nav">   
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">checkout</li>
            </ol>
        </div>
    </nav>

    <div class="container">

        <ul class="checkout-progress-bar">
            <li>
                <span>Shipping</span>
            </li>
            <li class="active">
                <span>Review &amp; Payments</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-lg-4">
                <div class="order-summary">
                    <h3>Summary</h3>

                    <h4>
                        <a data-toggle="collapse" href="#order-cart-section" class="collapsed" role="button" aria-expanded="false" aria-controls="order-cart-section">
                            @if(!empty($cartproducts))
                            {{ count($cartproducts) }}
                        @endif products in Cart</a>
                    </h4>

                    <div class="collapse" id="order-cart-section">
                        <table class="table table-mini-cart">
                            <tbody>
                                @if(!empty($cartproducts))
                                @foreach($cartproducts as $cartproduct)
                                <tr>
                                    <td class="product-col">
                                        <figure class="product-image-container">
                                            <a href="{{ route('product-details',$cartproduct->id) }}" class="product-image">
                                                <img src="{{ asset('public/upload/product/thumbnail/'.explode(',', $cartproduct->product_image)[0]) }}">
                                            </a>
                                        </figure>
                                        <div>
                                            <h2 class="product-title">
                                               <a data-toggle="tooltip" title="{{ $cartproduct->product_name }}" href="{{ route('product-details',$cartproduct->id) }}">{{ stringcutter(ucfirst($cartproduct->product_name)) }}</a>
                                           </h2>
                                           <span class="product-qty">Qty: {{ $cartproduct->sel_quantity }}</span>
                                           <span class="product-qty">Size: {{ $cartproduct->sel_size }}</span>
                                       </div>
                                   </td>
                                   <td class="price-col">₹{{ withquantity($cartproduct->price,$cartproduct->id,$cartproduct->cart_id) }}</td>
                               </tr>
                               @endforeach
                               @endif
                           </tbody>    
                       </table>
                   </div><!-- End #order-cart-section -->
               </div><!-- End .order-summary -->

               <div class="checkout-info-box">
                <h3 class="step-title">Ship To:
                    <a href="{{ route('checkout-shipping') }}" title="Edit" class="step-title-edit"><span class="sr-only">Edit</span><i class="icon-pencil"></i></a>
                </h3>

                <address>
                    {{ $shippingAddress->ship_first_name }} {{ $shippingAddress->ship_last_name }} <br>
                    {{ $shippingAddress->street_address_1 }}, {{ $shippingAddress->street_address_2 }} <br>
                    {{ $shippingAddress->city }} {{ $shippingAddress->pin_code }} <br>
                    {{ $shippingAddress->country }} <br>
                    {{ $shippingAddress->phone_number }} <br>
                </address>
            </div><!-- End .checkout-info-box -->

            <div class="checkout-info-box">
                <h3 class="step-title">Payment Method: 
                    <a href="{{ route('checkout-shipping') }}" title="Edit" class="step-title-edit"><span class="sr-only">Edit</span><i class="icon-pencil"></i></a>
                </h3>
                @if($alreadyShipping->payment_method == 'cod')
                <p>{{ 'Case On Delivery' }}</p>
                @else
                <p>{{ 'Online Payment' }}</p>
                @endif
            </div><!-- End .checkout-info-box -->
        </div><!-- End .col-lg-4 -->

        <div class="col-lg-8 order-lg-first">
            <div class="checkout-payment">
                <h2 class="step-title">Payment Method:</h2>

                <h4>Check / Money order</h4>

                <div class="form-group-custom-control">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="change-bill-address" value="1">
                        <label class="custom-control-label" for="change-bill-address">My billing and shipping address are the same</label>
                    </div><!-- End .custom-checkbox -->
                </div><!-- End .form-group -->

                <div id="checkout-shipping-address">
                    <address>
                        {{ $shippingAddress->ship_first_name }} {{ $shippingAddress->ship_last_name }} <br>
                        {{ $shippingAddress->street_address_1 }}, {{ $shippingAddress->street_address_2 }} <br>
                        {{ $shippingAddress->city }} {{ $shippingAddress->pin_code }} <br>
                        {{ $shippingAddress->country }} <br>
                        {{ $shippingAddress->phone_number }} <br>
                    </address>
                </div><!-- End #checkout-shipping-address -->

                <div id="new-checkout-address" class="show">
                    <form action="#">
                        <div class="form-group required-field">
                            <label>First Name </label>
                            <input type="text" class="form-control" required>
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Last Name </label>
                            <input type="text" class="form-control" required>
                        </div><!-- End .form-group -->

                        <div class="form-group">
                            <label>Company </label>
                            <input type="text" class="form-control">
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Street Address </label>
                            <input type="text" class="form-control" required>
                            <input type="text" class="form-control" required>
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>City  </label>
                            <input type="text" class="form-control" required>
                        </div><!-- End .form-group -->

                        <div class="form-group">
                            <label>State/Province</label>
                            <div class="select-custom">
                                <select class="form-control">
                                    <option value="CA">California</option>
                                    <option value="TX">Texas</option>
                                </select>
                            </div><!-- End .select-custom -->
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Zip/Postal Code </label>
                            <input type="text" class="form-control" required>
                        </div><!-- End .form-group -->

                        <div class="form-group">
                            <label>Country</label>
                            <div class="select-custom">
                                <select class="form-control">
                                    <option value="USA">United States</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="China">China</option>
                                    <option value="Germany">Germany</option>
                                </select>
                            </div><!-- End .select-custom -->
                        </div><!-- End .form-group -->

                        <div class="form-group required-field">
                            <label>Phone Number </label>
                            <div class="form-control-tooltip">
                                <input type="tel" class="form-control" required>
                                <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                            </div><!-- End .form-control-tooltip -->
                        </div><!-- End .form-group -->

                        <div class="form-group-custom-control">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="address-save">
                                <label class="custom-control-label" for="address-save">Save in Address book</label>
                            </div><!-- End .custom-checkbox -->
                        </div><!-- End .form-group -->
                    </form>
                </div><!-- End #new-checkout-address -->

                <div class="clearfix">
                    <a href="{{ route('place-order') }}" class="btn btn-primary float-right">Place Order</a>
                </div><!-- End .clearfix -->
            </div><!-- End .checkout-payment -->

        </div><!-- End .col-lg-8 -->
    </div><!-- End .row -->
</div><!-- End .container -->

</main><!-- End .main -->

@stop

@push('scripts')

<script>
    
</script>

@endpush