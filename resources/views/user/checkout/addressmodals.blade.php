
                <div class="modal-header">
                    <h3 class="modal-title" id="updateaddressModalLabel">Update Shipping Address</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div><!-- End .modal-header -->
                
                <div class="modal-body">
                    <div class="form-group required-field">
                        <label>First Name </label>
                        <input type="hidden" name="id" id="id" class="form-control form-control-sm" value="{{ $addressmodals->id }}" required>
                        <input type="text" name="ship_first_name" id="ship_first_name" class="form-control form-control-sm" value="{{ $addressmodals->ship_first_name }}" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Last Name </label>
                        <input type="text" name="ship_last_name" id="ship_last_name" class="form-control form-control-sm" value="{{ $addressmodals->ship_last_name }}" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Street Address 1</label>
                        <input type="text" name="street_address_1" id="street_address_1" class="form-control form-control-sm" value="{{ $addressmodals->street_address_1 }}" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Street Address 2</label>
                        <input type="text" name="street_address_2" id="street_address_2" class="form-control form-control-sm" value="{{ $addressmodals->street_address_2 }}" required>
                    </div>

                    <div class="form-group required-field">
                        <label>City  </label>
                        <input type="text" name="city" id="city" class="form-control form-control-sm" value="{{ $addressmodals->city }}" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>State</label>
                        <div class="select-custom">
                            <select class="form-control form-control-sm">
                                <option name="state" id="state" value="gujarat">Gujarat</option>
                            </select>
                        </div><!-- End .select-custom -->
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Pin Code </label>
                        <input type="text" name="pin_code" id="pin_code_check" value="{{ $addressmodals->pin_code }}"class="form-control form-control-sm" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Country</label>
                        <div class="select-custom">
                            <select class="form-control form-control-sm" name="country" id="country">
                                <option value="india">India</option>
                            </select>
                        </div><!-- End .select-custom -->
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Phone Number </label>
                        <div class="form-control-tooltip">
                            <input type="number" name="phone_number" id="phone_number" class="form-control form-control-sm"  value="{{ $addressmodals->phone_number }}"required>
                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                        </div><!-- End .form-control-tooltip -->
                    </div><!-- End .form-group -->

                </div><!-- End .modal-body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div><!-- End .modal-footer -->
            
       