@extends('user.layouts.app')
@section('title', 'Checkout shipping')

@section('content')

<main class="main">

    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Checkout</li>
            </ol>
        </div>
    </nav>

    <div class="container">

        <ul class="checkout-progress-bar">
            <li class="active">
                <span>Shipping</span>
            </li>
            <li>
                <span>Review &amp; Payments</span>
            </li>
        </ul>
        <div class="row">
            <div class="col-lg-8">
                <ul class="checkout-steps">
                    <li>
                        <h2 class="step-title">Shipping Address</h2>

                        <div class="shipping-step-addresses">
                            @if(!empty($shipping_address) && count($shipping_address) > 0)
                            @foreach($shipping_address as $key => $address)
                                <div class="shipping-address-box foractive{{ $address->id }} @if($key == 0) active @endif">
                                    <input type="hidden" class="address_id" value="@if($key == 0){{ $address->id }}@endif">
                                    <address>
                                        {{ $address->ship_first_name }} {{ $address->ship_last_name }} <br>
                                        {{ $address->street_address_1 }}, {{ $address->street_address_2 }} <br>
                                        {{ $address->city }} {{ $address->pin_code }} <br>
                                        {{ $address->country }} <br>
                                        {{ $address->phone_number }} <br>
                                    </address>

                                    <div class="address-box-action clearfix">
                                        <a href="javascript:void(0)" class="btn btn-sm btn-link edit-shipping-address" data-address="{{ $address->id }}">
                                            Edit
                                        </a>

                                        <a href="javascript:void(0)" class="btn btn-sm btn-outline-secondary float-right shipping-here" data-address="{{ $address->id }}">
                                            Ship Here
                                        </a>
                                    </div><!-- End .address-box-action -->
                                </div>
                            @endforeach
                            @else
                            <input type="hidden" class="address_id">
                            <center><h3>Please create your shipping address</h3></center>
                            @endif
                        </div><!-- End .shipping-step-addresses -->
                        <a href="#" class="btn btn-sm btn-outline-secondary btn-new-address" data-toggle="modal" data-target="#addressModal">+ New Address</a>
                    </li>


                    <li>
                        <div class="checkout-step-shipping">
                            <h2 class="step-title">Payments Methods</h2>

                            <table class="table table-step-shipping">
                                <tbody>
                                    @if(grandtotal() > 2000)
                                    <tr>
                                        <td><input type="radio" class="payment_method" name="shipping-method" value="cod"></td> 
                                        <td><strong>Cash on Delivery</strong></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td><input type="radio" class="payment_method" name="shipping-method" value="online"></td>
                                        <td><strong>Online payment</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div><!-- End .checkout-step-shipping -->
                    </li>
                </ul>
            </div><!-- End .col-lg-8 -->

            <div class="col-lg-4">
                <div class="order-summary">
                    <h3>Summary </h3>

                    <h4>
                        <a data-toggle="collapse" href="#order-cart-section" class="collapsed" role="button" aria-expanded="false" aria-controls="order-cart-section">
                            @if(!empty($cartproducts))
                            {{ count($cartproducts) }}
                        @endif products in Cart</a>
                    </h4>

                    <div class="collapse" id="order-cart-section">
                        <table class="table table-mini-cart">
                            <tbody>
                                @if(!empty($cartproducts))
                                @foreach($cartproducts as $cartproduct)
                                <tr>
                                    <td class="product-col">
                                        <figure class="product-image-container">
                                            <a href="{{ route('product-details',$cartproduct->id) }}" class="product-image">
                                                <img src="{{ asset('public/upload/product/thumbnail/'.explode(',', $cartproduct->product_image)[0]) }}">
                                            </a>
                                        </figure>
                                        <div>
                                            <h2 class="product-title">
                                               <a data-toggle="tooltip" title="{{ $cartproduct->product_name }}" href="{{ route('product-details',$cartproduct->id) }}">{{ stringcutter(ucfirst($cartproduct->product_name)) }}</a>
                                           </h2>
                                           <span class="product-qty">Qty: {{ $cartproduct->sel_quantity }}</span>
                                           <span class="product-qty">Size: {{ $cartproduct->sel_size }}</span>
                                       </div>
                                   </td>
                                   <td class="price-col">₹{{ withquantity($cartproduct->price,$cartproduct->id,$cartproduct->cart_id) }}</td>
                               </tr>
                               @endforeach
                               @endif
                           </tbody>    
                       </table>
                   </div><!-- End #order-cart-section -->
               </div><!-- End .order-summary -->
           </div><!-- End .col-lg-4 -->
       </div><!-- End .row -->

        <span class="error-shipping-page" style="color: red;"></span>
        <div class="row">
            <div class="col-lg-8">
                <div class="checkout-steps-action">
                    <a href="javascript:void(0)" class="btn btn-primary float-right on-next-shipping">NEXT</a>
                </div><!-- End .checkout-steps-action -->
            </div><!-- End .col-lg-8 -->
    </div><!-- End .row -->
</div><!-- End .container -->
</main>

<!-- Modal -->
<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="addressModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('shpping-address') }}" method="post" id="shipping-address-form">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h3 class="modal-title" id="addressModalLabel">Shipping Address</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div><!-- End .modal-header -->

                <div class="modal-body">
                    <div class="form-group required-field">
                        <label>First Name </label>
                        <input type="text" name="ship_first_name" id="ship_first_name" class="form-control form-control-sm" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Last Name </label>
                        <input type="text" name="ship_last_name" id="ship_last_name" class="form-control form-control-sm" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Street Address 1</label>
                        <input type="text" name="street_address_1" id="street_address_1" class="form-control form-control-sm" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Street Address 2</label>
                        <input type="text" name="street_address_2" id="street_address_2" class="form-control form-control-sm" required>
                    </div>

                    <div class="form-group required-field">
                        <label>City  </label>
                        <input type="text" name="city" id="city" class="form-control form-control-sm" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>State</label>
                        <div class="select-custom">
                            <select class="form-control form-control-sm">
                                <option name="state" id="state" value="gujarat">Gujarat</option>
                            </select>
                        </div><!-- End .select-custom -->
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Pin Code </label>
                        <input type="text" name="pin_code" id="pin_code" class="form-control form-control-sm" required>
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Country</label>
                        <div class="select-custom">
                            <select class="form-control form-control-sm" name="country" id="country">
                                <option value="india">India</option>
                            </select>
                        </div><!-- End .select-custom -->
                    </div><!-- End .form-group -->

                    <div class="form-group required-field">
                        <label>Phone Number </label>
                        <div class="form-control-tooltip">
                            <input type="number" name="phone_number" id="phone_number" class="form-control form-control-sm" required>
                            <span class="input-tooltip" data-toggle="tooltip" title="For delivery questions." data-placement="right"><i class="icon-question-circle"></i></span>
                        </div><!-- End .form-control-tooltip -->
                    </div><!-- End .form-group -->

                </div><!-- End .modal-body -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-link btn-sm" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary btn-sm">Save changes</button>
                </div><!-- End .modal-footer -->
            </form>
        </div><!-- End .modal-content -->
    </div><!-- End .modal-dialog -->
</div><!-- End .modal -->


<!-- Modal -->
<div class="modal fade" id="updateaddressModal" tabindex="-1" role="dialog" aria-labelledby="updateaddressModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('update-shpping-address') }}" method="post" id="update-shipping-address-form">
                {{ csrf_field() }}
            <span id="edi_data_wrap"></span>
            </form>
        </div><!-- End .modal-content -->
    </div><!-- End .modal-dialog -->
</div><!-- End .modal -->

@stop

@push('scripts')

<script type="text/javascript">

    $(document).ready(function() {

        $('.shipping-here').click(function() {
            var id = $(this).data('address');
            $('.shipping-address-box').removeClass('active');
            $('.foractive'+id+'').addClass('active');
            $('.address_id').val(id);
        });

        $('#shipping-address-form').validate({
            rules: {
                ship_first_name: {
                    required: true,
                },
                ship_last_name: {
                    required: true,
                },
                street_address_1: {
                    required: true,
                },
                street_address_2: {
                    required: true,
                },
                city: {
                    required: true,
                },
                pin_code: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    digits: true,
                    remote: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: user_base_url + "/check-pincode",
                    type: "post",
                    data: {
                        pin_code: function() {
                            return $('#pin_code').val();
                        }
                    },
                    dataFilter: function (data) {
                        var json = JSON.parse(data);
                        if (json.msg == "false") {
                            return "\"" + "We not deliver to this address, please choose diffrent pincode" + "\"";
                        } else {
                            return 'true';
                        }
                    }
                }
                },
                phone_number: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    digits: true,
                },
            },
            errorPlacement: function(error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#"+name+"_radio-error");
                }else {
                    error.appendTo(element.parent());
                }
            },
            messages:{
                ship_first_name: {
                    required: 'Please Enter First Name',
                },
                ship_last_name: {
                    required: 'Please Enter Last Name',
                },
                street_address_1: {
                    required: 'Please Enter Your Address',
                },
                street_address_2: {
                    required: 'Please Enter Your Address',
                },
                city: {
                    required: 'Please Enter Your City',
                },
                pin_code: {
                    required: 'Please Enter Your Pincode ',
                    minlength: 'Please Enter 6 Digit Pincode',
                },
                phone_number: {
                    required: 'Please Enter Your Mobile No',
                    minlength: 'Please Enter 10 Digit Mobile No',
                },
            },
        });

        $('.edit-shipping-address').click(function() {
            var address_id = $(this).data('address');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: user_base_url+'/edit-shipping-address',
                type: "POST",
                data: { 'address_id':address_id },
                dataType: 'json',
                success: function (data) {
                    $('#loading').hide();
                    $('#edi_data_wrap').html(data.data);
                    $('#updateaddressModal').modal('show');
                },
                error: function (data) {
                    $('#loading').hide();
                },
            });
        });


        $('#update-shipping-address-form').validate({
            rules: {
                ship_first_name: {
                    required: true,
                },
                ship_last_name: {
                    required: true,
                },
                street_address_1: {
                    required: true,
                },
                street_address_2: {
                    required: true,
                },
                city: {
                    required: true,
                },
                pin_code: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    digits: true,
                    remote: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: user_base_url + "/check-pincode",
                    type: "post",
                    data: {
                        pin_code: function() {
                            return $('#pin_code_check').val();
                        }
                    },
                    dataFilter: function (data) {
                        var json = JSON.parse(data);
                        if (json.msg == "false") {
                            return "\"" + "We not deliver to this address, please choose diffrent pincode" + "\"";
                        } else {
                            return 'true';
                        }
                    }
                }
                },
                phone_number: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    digits: true,
                },
            },
            errorPlacement: function(error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#"+name+"_radio-error");
                }else {
                    error.appendTo(element.parent());
                }
            },
            messages:{
                ship_first_name: {
                    required: 'Please Enter First Name',
                },
                ship_last_name: {
                    required: 'Please Enter Last Name',
                },
                street_address_1: {
                    required: 'Please Enter Your Address',
                },
                street_address_2: {
                    required: 'Please Enter Your Address',
                },
                city: {
                    required: 'Please Enter Your City',
                },
                pin_code: {
                    required: 'Please Enter Your Pincode ',
                    minlength: 'Please Enter 6 Digit Pincode',
                },
                phone_number: {
                    required: 'Please Enter Your Mobile No',
                    minlength: 'Please Enter 10 Digit Mobile No',
                },
            },
        });

    });

</script>

@endpush