@extends('user.layouts.app')
@section('title', 'Contact Us')

@section('content')

    <div class="container">
        <nav aria-label="breadcrumb" class="breadcrumb-nav">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact us</li>
            </ol>
        </nav>
    </div>

    <div class="page-header">
        <div class="container">
            <h1>Contact Us</h1>
        </div>
    </div>

    <div class="container">
{{--        <div id="map"></div>--}}

        <div class="row">
            <div class="col-md-8">
                <h2 class="light-title">Write <strong>Us</strong></h2>

                <form id="contact-form" method="POST" action="{{ route('contact.us.inquiry') }}">
                    @csrf
                    <div class="form-group required-field">
                        <label for="contact_name">Name</label>
                        <input type="text" class="form-control" id="contact_name" name="contact_name" value="{{ old('contact_name') }}">
                        @error('contact_name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group required-field">
                        <label for="contact_email">Email</label>
                        <input type="email" class="form-control" id="contact_email" name="contact_email" value="{{ old('contact_email') }}">
                        @error('contact_email')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group required-field">
                        <label for="contact_phone">Phone Number</label>
                        <input type="tel" class="form-control" id="contact_phone" name="contact_phone" value="{{ old('contact_phone') }}">
                        @error('contact_phone')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group required-field">
                        <label for="contact_message">What’s on your mind?</label>
                        <textarea cols="30" rows="1" id="contact_message" class="form-control" name="contact_message"
                        >{{ old('contact_message') }}</textarea>
                        @error('contact_message')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-footer">
                        <input type="submit" class="btn btn-primary btnSubmit" value="Submit">
                    </div>
                </form>
            </div>

            <div class="col-md-4">
                <h2 class="light-title">Contact <strong>Details</strong></h2>

                <div class="contact-info">
                    <div>
                        <i class="icon-mobile"></i>
                        <p><a href="tel:{{ $generalSettings->phone }}">{{ $generalSettings->phone }}</a></p>
                    </div>
                    <div>
                        <i class="icon-mail-alt"></i>
                        <p><a href="mailto:{{ $generalSettings->email }}">{{ $generalSettings->email }}</a></p>
                    </div>
                    {{--<div>
                        <i class="icon-location"></i>
                        <p>{{ $generalSettings->address }}</p>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>

@stop

@push('scripts')

    <script>
        $('#contact-form').validate({
            rules: {
                contact_name: {
                    required: true,
                },
                contact_email: {
                    required: true,
                    email: true,
                },
                contact_phone: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                    digits: true,
                },
                contact_message: {
                    required: true,
                },
            },
            messages: {
                contact_name: {
                    required: 'Please enter name',
                },
                contact_email: {
                    required: 'Please enter email',
                },
                contact_phone: {
                    required: 'Please Enter Your Mobile No',
                    minlength: 'Please Enter 10 Digit Mobile No',
                },
                contact_message: {
                    required: 'Please enter message',
                },
            },
            submitHandler: function (form) {
                $(".btnSubmit").val("Please Wait...").attr('disabled', 'disabled');
                return true;
            }
        });
    </script>

@endpush
