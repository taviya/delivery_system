<div id="addModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Shop</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" role="form" id="addForm" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="register-form-errors"></div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Shop Name</label>
                        <div class="col-sm-10">
                            <input name="shop_name" type="text" class="form-control" value="{{ $mode == 'Edit' ? $shop->shop_name : old('shop_name') }}">
                            @if ($errors->has('shop_name'))
                                <span style="color:red;">{{$errors->first('shop_name')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Shop Address</label>
                        <div class="col-sm-10">
                            <textarea name="address" class="form-control">{{ old('address') }}</textarea>
                            @if ($errors->has('address'))
                                <span style="color:red;">{{$errors->first('address')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Select Turnover</label>
                        <div class="col-sm-10">
                            <select name="turnover" class="form-control">
                                <option value="">Select One Value Only</option>
                                @if(!empty(Config::get('constants.shop_turnover')))
                                    @foreach (Config::get('constants.shop_turnover') as $key => $value)
                                        <option
                                            value="{{ $value }}">{{ $value }}
                                            Lacs
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Shop image</label>
                        <div class="col-sm-10">
                            <input type="file" name="shop_image" id="fileUploader" class="form-control">
                        </div>
                        <div class="col-sm-10">
                            <img style="height: 100px; width: 100px; display: none;" id="display_image" class="img-thumbnail" src="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="editModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Shop</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form  method="post" role="form" id="editForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="register-form-errors"></div>
                {{csrf_field()}}
                <div id="edi_data_wrap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
        </div>
    </div>
</div>


<div class="modal fade in" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="width: 400px;">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <h4 class="modal-title">Confirm Delete</h4>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
            </div>
            <!--Modal body-->
            <div class="modal-body">
                <p>Are you sure you want to delete this shop?</p>
                <div class="text-right">
                    <form id="idForm" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" class="shop_del_id" value="" name="del_id">
                        <button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close">
                            Close
                        </button>
                        <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
