@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Order List</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Order List</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								{{--<div class="card-header table-card-header">
									<h5>Order List</h5>
									<div class="float-right">
										<a href="javascript:;" class="btn btn-primary" id="addAjaxUser"><i class="fa fa-plus"></i> Add Shop </a>
									</div>
								</div>--}}
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="orders-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>Id</th>
													<th>Order Id</th>
													<th>Customer Name</th>
													<th>Mobile</th>
													<th>Price</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
                                                {{--Ajax response--}}
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- Users Management end -->
						</div>
						<!-- Page-body end -->
					</div>
				</div>
				<!-- Main-body start -->

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@push('scripts')

    <script type="text/javascript">
        $(function () {
            oTable = $('#orders-table').DataTable({
                "processing": true,
                "serverSide": true,
                ajax: "{{ route('orders.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'order_number', name: 'order_number'},
                    {data: 'customer_name', name: 'customer_name', orderable: false,},
                    {data: 'shop_image', name: 'shop_image', orderable: false,},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });

            $(document).on('click', '.status_list', function() {
                $.ajax({
                    type: "get",
                    url: "{{ route('shop.status.update', '') }}/"+$(this).data('id'),
                    data: {},
                    success: function(res) {
                        if (res.status) {
                            message(res.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    }
                });
            });
        });
    </script>

@endpush

