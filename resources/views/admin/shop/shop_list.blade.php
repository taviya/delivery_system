@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Shop Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Shop Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Shop Management</h5>
									<div class="float-right">
										<a href="javascript:;" class="btn btn-primary" id="addAjaxUser"><i class="fa fa-plus"></i> Add Shop </a>
									</div>
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="shop-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Shop Name</th>
													<th>Turnover</th>
													<th>Shop Image</th>
                                                    <th>Shop Commision</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
                                                {{--Ajax response--}}
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- Users Management end -->
						</div>
						<!-- Page-body end -->
					</div>
				</div>
				<!-- Main-body start -->

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.shop.modals')
@endsection

@push('scripts')

    <script type="text/javascript">
        $(function () {
            oTable = $('#shop-table').DataTable({
                "processing": true,
                "serverSide": true,
                ajax: "{{ route('shop.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'shop_name', name: 'shop_name'},
                    {data: 'turnover', name: 'turnover', orderable: false },
                    {data: 'shop_image', name: 'shop_image', orderable: false },
                    {data: 'shop_commission', name: 'shop_commission', orderable: true },
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });

            $(document).on('click', '.status_list', function() {
                $.ajax({
                    type: "get",
                    url: "{{ route('shop.status.update', '') }}/"+$(this).data('id'),
                    data: {},
                    success: function(res) {
                        if (res.status) {
                            message(res.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    }
                });
            });
        });

        $('#addAjaxUser').click(function (event) {
            $('.modal-body').find("input,textarea,select").val('').end();
            $('#display_image').attr("src", "");
            $("#display_image").css("display", "none");
            $('.register-form-errors').html('');
            $('#addModal').modal('show');
        })

        $('#addForm').validate({
            rules: {
                shop_name: {
                    required: true,
                },
                address: {
                    required: true,
                },
                shop_commission: {
                    required: true,
                    number: true,
                    min: 0,
                    max: 100,
                },
                turnover: {
                    required: true,
                },
                shop_image: {
                    required: true,
                    extension: "jpeg|png|jpg"
                }
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                shop_name: {
                    required: 'Please Enter Shop Name',
                },
                address: {
                    required: 'Please Enter Your Shop Address',
                },
                turnover: {
                    required: 'Please select turnover',
                },
                shop_image: {
                    required: 'Please select shop image',
                }
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('add.shop')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            window.location.href = "{{ route('admin.users')}}";
                            /*$('#addModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);*/
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $('#editForm').validate({
            rules: {
                shop_name: {
                    required: true,
                },
                address: {
                    required: true,
                },
                shop_commission: {
                    required: true,
                    number: true,
                    min: 0,
                    max: 100,
                },
                turnover: {
                    required: true,
                },
                shop_image: {
                    extension: "jpeg|png|jpg"
                }
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                shop_name: {
                    required: 'Please Enter Shop Name',
                },
                address: {
                    required: 'Please Enter Your Shop Address',
                },
                turnover: {
                    required: 'Please select turnover',
                },
                shop_image: {
                    required: 'Please select shop image',
                }
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('shop.update')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#editModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $(document).on('click','.data_edit',function(){
            $.ajax({
                type: "get",
                url: "{{ route('shop.edit', '') }}/"+$(this).data('id'),
                data: {},
                success: function(res) {
                    if (res.status) {
                        $('#editModal').modal('show');
                        $('#edi_data_wrap').html(res.data);
                    }
                }
            });

        });

        var urlId = "{{ Request::segment(3) }}";
        if(urlId && urlId != ''){
            edit(urlId);
        }
        function edit(id = ''){
            $.ajax({
                type: "get",
                url: "{{ route('shop.edit', '') }}/"+id,
                data: {},
                success: function(res) {
                    if (res.status) {
                        $('#editModal').modal('show');
                        $('#edi_data_wrap').html(res.data);
                    }
                }
            });
        }

        $(document).on('click','.data_delete',function(){
            $('#delete_modal').modal('show');
            $('.shop_del_id').val($(this).data('id'));
        });

        $("#idForm").submit(function (e) {
            var form = $('#idForm')[0];
            e.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('shop.delete')}}",
                type: "POST",
                data: new FormData(form),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    if (data.status) {
                        $('#delete_modal').modal('hide');
                        message(data.message, 'success');
                        oTable.rows().invalidate('data').draw(false);
                    }
                }
            });
        });

        function readIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        var _URL = window.URL || window.webkitURL;
        $("#fileUploader").change(function (e) {
            var file, img;
            var $this = this;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    if (this.width != 225 || this.height != 225) {
                        message('For shop image select height: 225px and width: 225px image.', 'danger');
                        $('#fileUploader').val('');
                        $("#display_image").css("display", "none");
                    } else {
                        $("#display_image").css("display", "block");
                        readIMG($this);
                    }
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        });

        /*$("#fileUploader").change(function () {
            $("#display_image").css("display", "block");
            console.log(this);
            readIMG(this);
        });*/

        function editReadIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#edit_display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#editFileUploader', function() {
            var file, img;
            var $this = this;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    if (this.width != 225 || this.height != 225) {
                        message('For shop image select height: 225px and width: 225px image.', 'danger');
                        $('#editFileUploader').val('');
                    } else {
                        $("#edit_display_image").css("display", "block");
                        editReadIMG($this);
                    }
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        });
    </script>

@endpush
