<div class="register-form-errors"></div>
<div class="form-group row">
    <input name="update_id" type="hidden" class="form-control" value="{{ $mode == 'Edit' ? $shop->id : '' }}">
    <label class="col-sm-2 col-form-label">Shop Name</label>
    <div class="col-sm-10">
        <input name="shop_name" type="text" class="form-control"
               value="{{ $mode == 'Edit' ? $shop->shop_name : old('shop_name') }}">
        @if ($errors->has('shop_name'))
            <span style="color:red;">{{$errors->first('shop_name')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Shop Address</label>
    <div class="col-sm-10">
        <textarea name="address" class="form-control">{{ $mode == 'Edit' ? $shop->address : old('address') }}</textarea>
        @if ($errors->has('address'))
            <span style="color:red;">{{$errors->first('address')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Shop Commission</label>
    <div class="col-sm-10">
        <input name="shop_commission" type="number" class="form-control" value="{{ $mode == 'Edit' ? $shop->shop_commission : old('shop_commission') }}">
        @if ($errors->has('shop_commission'))
            <span style="color:red;">{{$errors->first('shop_commission')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Select Turnover</label>
    <div class="col-sm-10">
        <select name="turnover" class="form-control">
            <option value="">Select One Value Only</option>
            @if(!empty(Config::get('constants.shop_turnover')))
                @foreach (Config::get('constants.shop_turnover') as $key => $value)
                    <option value="{{ $value }}" {{ $shop->turnover == $value ? 'selected' : '' }}>{{ $value }} Lacs</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Shop image</label>
    <div class="col-sm-10">
        <input type="file" name="shop_image" id="editFileUploader" class="form-control">
    </div>
    <div class="col-sm-10">
        <img style="height: 100px; width: 100px;" id="edit_display_image" class="img-thumbnail" src="{{ $mode == 'Edit' ? asset("public/upload/shop/thumbnail/".$shop->shop_image) : old('shop_image') }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Status</label>
    <div class="col-sm-10">
        <div class="radio radio-inline">
            <input type="radio" name="status" value="1" @if($shop->status == 1) checked @endif>
            Active
        </div>
        <div class="radio radio-inline">
            <input type="radio" name="status" value="0" @if($shop->status == 0) checked @endif>
            Deactive
        </div>
        @if ($errors->has('status'))
            <span style="color:red;">{{$errors->first('status')}}</span>
        @endif
    </div>
</div>


