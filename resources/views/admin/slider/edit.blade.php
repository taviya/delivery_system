<div class="register-form-errors"></div>
<div class="form-group row">
    <input name="update_id" type="hidden" class="form-control" value="{{ $slider->id }}">
    <label class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-10">
        <input name="title" type="text" class="form-control"
               value="{{ $slider->title }}">
        @if ($errors->has('title'))
            <span style="color:red;">{{$errors->first('title')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Subtitle</label>
    <div class="col-sm-10">
        <input name="subtitle" type="text" class="form-control"
               value="{{ $slider->subtitle }}">
        @if ($errors->has('subtitle'))
            <span style="color:red;">{{$errors->first('subtitle')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-10">
        <textarea rows="5" cols="5" name="description" class="form-control">{{ $slider->description }}</textarea>
        @if ($errors->has('description'))
            <span style="color:red;">{{$errors->first('description')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Optional</label>
    <div class="col-sm-10">
        <input name="optional" type="text" class="form-control" value="{{ $slider->optional }}">
        @if ($errors->has('optional'))
            <span style="color:red;">{{$errors->first('optional')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Button Name</label>
    <div class="col-sm-10">
        <input name="btn_name" type="text" class="form-control" value="{{ $slider->btn_name }}">
        @if ($errors->has('btn_name'))
            <span style="color:red;">{{$errors->first('btn_name')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Button Link</label>
    <div class="col-sm-10">
        <input name="btn_link" type="text" class="form-control" value="{{ $slider->btn_link }}">
        @if ($errors->has('btn_link'))
            <span style="color:red;">{{$errors->first('btn_link')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Slider image</label>
    <div class="col-sm-10">
        <input type="file" name="slider_image" id="editFileUploader" class="form-control">
    </div>
    <div class="col-sm-10">
        <img style="height: 100px; width: 100px;" id="edit_display_image" class="img-thumbnail" src="{{ asset("public/upload/slider/thumbnail/".$slider->slider_image) }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Status</label>
    <div class="col-sm-10">
        <div class="radio radio-inline">
            <input type="radio" name="status" value="1" @if($slider->status == 1) checked @endif>
            Active
        </div>
        <div class="radio radio-inline">
            <input type="radio" name="status" value="0" @if($slider->status == 0) checked @endif>
            Deactive
        </div>
        @if ($errors->has('status'))
            <span style="color:red;">{{$errors->first('status')}}</span>
        @endif
    </div>
</div>


