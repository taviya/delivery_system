<div id="addModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Slider</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" role="form" id="addForm" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="register-form-errors"></div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <input name="title" type="text" class="form-control" value="{{ old('title') }}">
                            @if ($errors->has('title'))
                                <span style="color:red;">{{$errors->first('title')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Subtitle</label>
                        <div class="col-sm-10">
                            <input name="subtitle" type="text" class="form-control" value="{{ old('subtitle') }}">
                            @if ($errors->has('subtitle'))
                                <span style="color:red;">{{$errors->first('subtitle')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" name="description" class="form-control">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <span style="color:red;">{{$errors->first('description')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Optional</label>
                        <div class="col-sm-10">
                            <input name="optional" type="text" class="form-control" value="{{ old('optional') }}">
                            @if ($errors->has('optional'))
                                <span style="color:red;">{{$errors->first('optional')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Button Name</label>
                        <div class="col-sm-10">
                            <input name="btn_name" type="text" class="form-control" value="{{ old('btn_name') }}">
                            @if ($errors->has('btn_name'))
                                <span style="color:red;">{{$errors->first('btn_name')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Button Link</label>
                        <div class="col-sm-10">
                            <input name="btn_link" type="text" class="form-control" value="{{ old('btn_link') }}">
                            @if ($errors->has('btn_link'))
                                <span style="color:red;">{{$errors->first('btn_link')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Slider Image</label>
                        <div class="col-sm-10">
                            <input type="file" name="slider_image" id="fileUploader" class="form-control">
                        </div>
                        <div class="col-sm-10">
                            <img style="height: 100px; width: 100px; display: none;" id="display_image" class="img-thumbnail" src="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="editModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Slider</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form  method="post" role="form" id="editForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="register-form-errors"></div>
                {{csrf_field()}}
                <div id="edi_data_wrap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
        </div>
    </div>
</div>


<div class="modal fade in" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="width: 400px;">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <h4 class="modal-title">Confirm Delete</h4>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
            </div>
            <!--Modal body-->
            <div class="modal-body">
                <p>Are you sure you want to delete this slider?</p>
                <div class="text-right">
                    <form id="idForm" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" class="slider_del_id" value="" name="del_id">
                        <button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close">
                            Close
                        </button>
                        <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
