@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Slider Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Slider Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Slider Management</h5>
									<div class="float-right">
										<a href="javascript:;" class="btn btn-primary" id="addAjaxUser"><i class="fa fa-plus"></i> Add Slider </a>
									</div>
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="slider-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Title</th>
													<th>Subtitle</th>
													<th>Button Name</th>
													<th>Button Link</th>
													<th>Slider Image</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
                                                {{--Ajax response--}}
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- Users Management end -->
						</div>
						<!-- Page-body end -->
					</div>
				</div>
				<!-- Main-body start -->

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.slider.modals')
@endsection

@push('scripts')

    <script type="text/javascript">
        $(function () {
            oTable = $('#slider-table').DataTable({
                "processing": true,
                "serverSide": true,
                ajax: "{{ route('slider.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'title', name: 'title'},
                    {data: 'subtitle', name: 'subtitle', orderable: false,},
                    {data: 'btn_name', name: 'btn_name'},
                    {data: 'btn_link', name: 'btn_link'},
                    {data: 'slider_image', name: 'slider_image', orderable: false,},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });

            $(document).on('click', '.status_list', function() {
                $.ajax({
                    type: "get",
                    url: "{{ route('slider.status.update', '') }}/"+$(this).data('id'),
                    data: {},
                    success: function(res) {
                        if (res.status) {
                            message(res.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    }
                });
            });
        });

        $('#addAjaxUser').click(function (event) {
            $('.modal-body').find("input,textarea,select").val('').end();
            $('#display_image').attr("src", "");
            $("#display_image").css("display", "none");
            $('.register-form-errors').html('');
            $('#addModal').modal('show');
        })

        $('#addForm').validate({
            rules: {
                title: {
                    required: true,
                },
                subtitle: {
                    required: true,
                },
                description: {
                    required: true,
                },
                slider_image: {
                    required: true,
                    extension: "jpeg|png|jpg"
                },
                btn_name: {
                    required: true,
                },
                btn_link: {
                    required: true,
                },
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                title: {
                    required: 'Please enter title',
                },
                subtitle: {
                    required: 'Please enter subtitle',
                },
                description: {
                    required: 'Please enter description',
                },
                slider_image: {
                    required: 'Please select slider image',
                },
                btn_name: {
                    required: 'Please enter button name',
                },
                btn_link: {
                    required: 'Please enter button link',
                }
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('add.slider')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#addModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $('#editForm').validate({
            rules: {
                title: {
                    required: true,
                },
                subtitle: {
                    required: true,
                },
                description: {
                    required: true,
                },
                slider_image: {
                    extension: "jpeg|png|jpg"
                },
                btn_name: {
                    required: true,
                },
                btn_link: {
                    required: true,
                },
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                title: {
                    required: 'Please enter title',
                },
                subtitle: {
                    required: 'Please enter subtitle',
                },
                description: {
                    required: 'Please enter description',
                },
                slider_image: {
                    required: 'Please select slider image',
                },
                btn_name: {
                    required: 'Please enter button name',
                },
                btn_link: {
                    required: 'Please enter button link',
                }
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('slider.update')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#editModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $(document).on('click','.data_edit',function(){
            $.ajax({
                type: "get",
                url: "{{ route('slider.edit', '') }}/"+$(this).data('id'),
                data: {},
                success: function(res) {
                    if (res.status) {
                        $('#editModal').modal('show');
                        $('#edi_data_wrap').html(res.data);
                    }
                }
            });

        });

        $(document).on('click','.data_delete',function(){
            $('#delete_modal').modal('show');
            $('.slider_del_id').val($(this).data('id'));
        });

        $("#idForm").submit(function (e) {
            var form = $('#idForm')[0];
            e.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('slider.delete')}}",
                type: "POST",
                data: new FormData(form),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    if (data.status) {
                        $('#delete_modal').modal('hide');
                        message(data.message, 'success');
                        oTable.rows().invalidate('data').draw(false);
                    }
                }
            });
        });

        function readIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#fileUploader").change(function () {
            $("#display_image").css("display", "block");
            readIMG(this);
        });

        function editReadIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#edit_display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#editFileUploader', function() {
            editReadIMG(this);
        });
    </script>

@endpush
