<div id="main-category-modal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Main Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" role="form" id="add-main-category-form" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
          <div class="register-form-errors"></div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Main Category Name</label>
            <div class="col-sm-10">
              <input name="main_category_name" id="MainCategoryName" type="text" class="form-control" value="{{ old('main_category_name') }}">
              @if ($errors->has('main_category_name'))
              <span style="color:red;">{{ $errors->first('main_category_name') }}</span>
              @endif
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-2 col-form-label">Category image</label>
            <div class="col-sm-10">
              <input type="file" name="category_icon" id="fileUploader" class="form-control">
            </div>
            <div class="col-sm-10">
              <img style="height: 100px; width: 100px; display: none;" id="display_image" class="img-thumbnail" src="">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="editModal" tabindex="-1" role="dialog"
aria-labelledby="addServiceTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="deleteModalLongTitle">Edit Main Category</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <form  method="post" role="form" id="editForm" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="register-form-errors"></div>
        {{csrf_field()}}
        <div id="edi_data_wrap"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
    </form>
  </div>
</div>
</div>

<div class="modal fade in" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" style="width: 400px;">
    <div class="modal-content">
      <!--Modal header-->
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete</h4>
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <p>Are you sure you want to delete this category?</p>
        <div class="text-right">
          <form method="post" role="form" id="deleteForm" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="deleteid">
            <button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close">Close</button>
            <button type="button" class="btn btn-danger btn-sm delete_btn">Delete</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>