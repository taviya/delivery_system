@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Main Category Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Main Category Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Main Category Management</h5>
									<div class="float-right">
										<a href="javascript:;" class="btn btn-primary" id="add-main-category"><i class="fa fa-plus"></i> Add Main Category </a>
									</div>
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="users-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Category Image</th>
													<th>Category Name</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
											<tfoot>
												<tr>
													<th>ID</th>
													<th>Category Image</th>
													<th>Category Name</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
							<!-- Users Management end -->
						</div>
						<!-- Page-body end -->
					</div>
				</div>
				<!-- Main-body start -->

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.maincategory.maincategorymodal')
@endsection

@push('scripts')

<script type="text/javascript">

	$(function () {
		oTable = $('#users-table').DataTable({
			"processing": true,
			"serverSide": true,
			ajax: "{{ route("maincategory.index") }}",
			columns: [
			{data: 'DT_RowIndex', name: 'DT_RowIndex'},
			{data: 'category_icon', name: 'category_icon', orderable: false},
			{data: 'main_category_name', name: 'main_category_name', orderable: false},
			{data: 'status', name: 'status', orderable: true, searchable: false},
			{data: 'action', name: 'action', orderable: false, searchable: false},
			],
		});

		$(document).on('click', '.status-update', function() {
			$.ajax({
				type: "get",
				url: "{{ route('maincategory.status.update', '') }}/"+$(this).data('id'),
				data: {},
				success: function(res) {
					if (res.status) {
						message(res.message, 'success');
						oTable.rows().invalidate('data').draw(false);
					}
				}
			});
		});
	});
	
	$('#add-main-category').click(function(event) {
		$('.modal-body').find("input,textarea,select").val('').end();
		$('.register-form-errors' ).html('');
		$('#main-category-modal').modal('show');
	})

	$('#add-main-category-form').validate({
		rules: {
			main_category_name: {
				required: true,
				remote: {
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: base_url + "/maincategory/checkcategory",
					type: "post",
					data: {
						categoryName: function() {
							return $('#MainCategoryName').val();
						}
					},
					dataFilter: function (data) {
						var json = JSON.parse(data);
						if (json.msg == "true") {
							return "\"" + "Category already exist, please add different category name" + "\"";
						} else {
							return 'true';
						}
					}
				}
			},
			category_icon: {
				required: true,
				extension: "jpeg|png|jpg"
			},
		},
		errorPlacement: function(error, element) {
			if (element.is(":radio")) {
				var name = element.attr('name');
				error.insertAfter("#"+name+"_radio-error");
			}else {
				error.appendTo(element.parent());
			}
		},
		messages:{
			main_category_name: {
				required: 'Please enter category name',
				remote: {
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: base_url + "/maincategory/checkcategory",
					type: "post",
					data: {
						categoryName: function() {
							return $('#MainCategoryName').val();
						}
					},
					dataFilter: function (data) {
						var json = JSON.parse(data);
						if (json.msg == "true") {
							return "\"" + "Category already exist, please add different category name" + "\"";
						} else {
							return 'true';
						}
					}
				}
			},
			category_icon: {
				required: 'Please select Category image',
			},
		},
		submitHandler: function(form) {
			$('#loading').show();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{route('maincategory.store')}}",
				type: "POST",
				data: new FormData(form),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData:false,
				success: function (data) {
					$('#loading').hide();
					if(data.status==1){
						$('#main-category-modal').modal('hide');
						message('Category Add Successfully', 'success');
						oTable.rows().invalidate('data').draw(false);
					}
				},
				error: function (data) {
					$('#loading').hide();
					var errorString = '<ul>';
					$.each(data.responseJSON.errors, function( key, value) {
						errorString += '<li>' + value + '</li>';
					});
					errorString += '</ul>';
					message(errorString, 'danger');
				},
			});
			return false;
		}
	});

	function deleteMainCategoryModel($id)
	{
		$('#deleteid').val($id);
		$('#delete_modal').modal('show');
	}

	function editMainCategoryModel(id)
	{
		var getUrl = window.location;
		var routeUrl = getUrl + '/edit' + "/" + id;

		$('#edi_data_wrap').html('');
		$('#loading').show();

		$.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url: routeUrl,
			type: "get",
			dataType: 'json',
			success: function (data) {
				$('#loading').hide();
				$('#edi_data_wrap').html(data.data);
				$('#editModal').modal('show');
			},
			error: function (data) {
			},
		});
	}

	$('#editForm').validate({
		rules: {
			main_category_name: {
				required: true,
				remote: {
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: base_url + "/maincategory/checkcategory",
					type: "post",
					data: {
						categoryName: function() {
							return $('#editMainCategoryName').val();
						},
						categoryID: function() {
							return $('#updateId').val();
						}
					},
					dataFilter: function (data) {
						var json = JSON.parse(data);
						if (json.msg == "true") {
							return "\"" + "Category already exist, please add different category name" + "\"";
						} else {
							return 'true';
						}
					}
				}
			},
			category_icon: {
				extension: "jpeg|png|jpg"
			},
		},
		errorPlacement: function(error, element) {
			if (element.is(":radio")) {
				var name = element.attr('name');
				error.insertAfter("#"+name+"_radio-error");
			}else {
				error.appendTo(element.parent());
			}
		},
		messages:{
			main_category_name: {
				required: 'Please enter category name',
			},
		},
		submitHandler: function(form) {
			$('#loading').show();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{ route('maincategory.update') }}",
				type: "POST",
				data: new FormData(form),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData: false,
				success: function (data) {
					$('#loading').hide();
					if(data.status==1){
						$("#editModal").modal('hide');
						message('Category Update Successfully', 'success');
						oTable.rows().invalidate('data').draw(false);
					}
				},
				error: function (data) {
					$('#loading').hide();
					var errorString = '<ul>';
					$.each(data.responseJSON.errors, function( key, value) {
						errorString += '<li>' + value + '</li>';
					});
					errorString += '</ul>';
					message(errorString, 'danger');
				},
			});
			return false;
		}
	});

	$(document).on('click','.delete_btn', function() {
		$.ajax({
			type: "get",
			url: "{{ route('maincategory.delete','') }}/"+$('#deleteid').val(),
			data: {},
			success: function(data) {
				if(data.status==1){
					$('#delete_modal').modal('hide');
					message(data.messages, 'success');
					oTable.rows().invalidate('data').draw(false);
				}
			}
		});
	});

	function readIMG(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#display_image').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#fileUploader").change(function () {
		$("#display_image").css("display", "block");
		readIMG(this);
	});

	function editReadIMG(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$('#edit_display_image').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).on('change', '#editFileUploader', function() {
		editReadIMG(this);
	});

</script>

@endpush