<div class="form-group row">
    	<label class="col-sm-2 col-form-label">Main Category Name</label>
    	<input name="update_id" id="updateId" type="hidden" class="form-control" value="{{ $maincategory->id }}">
        <div class="col-sm-10">
            <input name="main_category_name" id="editMainCategoryName" type="text" class="form-control" value="{{ $maincategory->main_category_name }}">
            @if ($errors->has('main_category_name'))
            <span style="color:red;">{{ $errors->first('main_category_name') }}</span>
             @endif
        </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Category image</label>
    <div class="col-sm-10">
    	<input type="file" name="category_icon" id="editFileUploader" class="form-control">
    </div>
    <div class="col-sm-10">
        <img style="height: 100px; width: 100px;" id="edit_display_image" class="img-thumbnail" src="{{ $mode == 'Edit' ? asset("public/upload/category/thumbnail/".$maincategory->category_icon) : old('category_icon') }}">
    </div>
</div>
<div class="form-group row">
		<label class="col-sm-2 col-form-label">Status</label>
		<div class="col-sm-10">
			<div class="radio radio-inline">
				<input type="radio" name="status" value="1" @if($maincategory->status == 1) checked @endif>
				Active
			</div>
			<div class="radio radio-inline">
				<input type="radio" name="status" value="0" @if($maincategory->status == 0) checked @endif>
				Deactive
			</div>
			@if ($errors->has('status'))
				<span style="color:red;">{{$errors->first('status')}}</span>
			@endif
		</div>
</div>