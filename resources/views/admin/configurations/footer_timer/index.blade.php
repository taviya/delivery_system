@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">

				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Timer</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Timer Settings</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page header end -->
						<!-- Page body start -->
						<div class="page-body">
							<div class="row">
								<div class="col-sm-12">
									<!-- Basic Form Inputs card start -->
									@include('admin.messages')
									<div class="card">
										<div class="card-header">
											<h5>Timer Settings</h5>
											<div class="card-header-right">
												<i class="icofont icofont-rounded-down"></i>
												<i class="icofont icofont-refresh"></i>
												<i class="icofont icofont-close-circled"></i>
											</div>
										</div>
										<div class="card-block">
											<form role="form" id="configform" method="POST" action="{{route('admin.TimerSettingSave')}}">
												{{csrf_field()}}
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Select Start Time(Morning)</label>
                                                    <div class="col-sm-10">
                                                        <select name="start_time" class="form-control">
                                                            <option value="">Select Start Time</option>
                                                            @if(!empty(Config::get('constants.start_time')))
                                                                @foreach (Config::get('constants.start_time') as $key => $value)
                                                                    <option value="{{ $value }}" {{ (int)$start_time == $key ? 'selected' : '' }}>{{ $value }} O' clock</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        @if ($errors->has('start_time'))
                                                            <span style="color:red;">{{$errors->first('start_time')}}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">Select Close Time(Evening)</label>
                                                    <div class="col-sm-10">
                                                        <select name="close_time" class="form-control">
                                                            <option value="">Select Close Time</option>
                                                            @if(!empty(Config::get('constants.close_time')))
                                                                @foreach (Config::get('constants.close_time') as $key => $value)
                                                                    <option value="{{ $key }}" {{ (int)$close_time == $key ? 'selected' : '' }}>{{ $value }} O' clock</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>

												<div class="row">
													<label class="col-sm-2"></label>
													<div class="col-sm-10">
														<button type="submit" class="btn btn-primary m-b-0">Submit</button>
														<a class="btn btn-primary m-b-0" href="{{route('admin.dashboard')}}">Back</a>
													</div>
												</div>
											</form>
										</div>
									</div>

								</div>
							</div>
						</div>
						<!-- Page body end -->
					</div>
				</div>
				<!-- Main-body end -->
				<div id="styleSelector">

				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')


<script type="text/javascript">
	$('#configform').validate({
            rules: {
                start_time: {
                    required: true,
                },
                close_time: {
                    required: true,
                },
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                start_time: {
                    required: 'Please select start time',
                },
                close_time: {
                    required: 'Please select close time',
                },
            },
        });
</script>


@endpush
