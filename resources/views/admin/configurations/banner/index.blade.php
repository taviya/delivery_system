@extends('admin.layouts.app')
@section('content')
    @include('admin.include.sidebar')

    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">

                    <!-- Main-body start -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- Page header start -->
                            <div class="page-header">
                                <div class="page-header-title">
                                    <h4>Banner</h4>
                                </div>
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('admin.dashboard') }}">
                                                <i class="icofont icofont-home"></i>
                                            </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="javascript:;">Banner Settings</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Page header end -->
                            <!-- Page body start -->
                            <div class="page-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- Basic Form Inputs card start -->
                                        @include('admin.messages')
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Banner Settings</h5>
                                                <div class="card-header-right">
                                                    <i class="icofont icofont-rounded-down"></i>
                                                    <i class="icofont icofont-refresh"></i>
                                                    <i class="icofont icofont-close-circled"></i>
                                                </div>
                                            </div>
                                            <div class="card-block">
                                                <form role="form" id="banner_form" method="POST"
                                                      action="">
                                                    {{csrf_field()}}
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Banner 1</label>
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <input type="file" id="banner1FileUploader" name="banner1" class="form-control">
                                                                    <span>Note - Image dimension(640px X 640px)</span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    @if(!empty($banner_1))
                                                                        <img id="banner1_display_image" src="{{ asset("public/upload/banner/banner1/".$banner_1->value) }}" style="height: 100px; width: 100px">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Banner 2</label>
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <input name="banner2_text" type="text" class="form-control" value="{{ !empty($banner_2) ? json_decode($banner_2->value, true)['banner_text'] : '' }}">

                                                                    @if ($errors->has('banner2_text'))
                                                                        <span style="color:red;">{{$errors->first('banner2_text')}}</span>
                                                                    @endif
                                                                </div>
                                                                <br>
                                                                <br>
                                                                <div class="col-md-6">
                                                                    <input type="file" id="banner2FileUploader" name="banner2_image" class="form-control">
                                                                    <span>Note - Image dimension(1270px X 640px)</span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    @if(!empty($banner_2))
                                                                        <img id="banner2_display_image" src="{{ asset("public/upload/banner/banner2/".json_decode($banner_2->value, true)['banner_image']) }}" style="height: 100px; width: 100px">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Banner 3</label>
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <input name="banner3_text" type="text" class="form-control" value="{{ !empty($banner_3) ? json_decode($banner_3->value, true)['banner_text'] : '' }}">

                                                                    @if ($errors->has('banner3_text'))
                                                                        <span style="color:red;">{{$errors->first('banner3_text')}}</span>
                                                                    @endif
                                                                </div>
                                                                <br>
                                                                <br>
                                                                <div class="col-md-6">
                                                                    <input type="file" id="banner3FileUploader" name="banner3_image" class="form-control">
                                                                    <span>Note - Image dimension(240px X 152px)</span>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    @if(!empty($banner_3))
                                                                        <img id="banner3_display_image" src="{{ asset("public/upload/banner/banner3/".json_decode($banner_3->value, true)['banner_image']) }}" style="height: 100px; width: 100px">
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>

                                                    <div class="row">
                                                        <label class="col-sm-2"></label>
                                                        <div class="col-sm-10">
                                                            <button type="submit" class="btn btn-primary m-b-0">Submit
                                                            </button>
                                                            <a class="btn btn-primary m-b-0"
                                                               href="{{route('admin.dashboard')}}">Back</a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- Page body end -->
                        </div>
                    </div>
                    <!-- Main-body end -->
                    <div id="styleSelector">

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')


    <script type="text/javascript">
        $('#banner_form').validate({
            rules: {
                banner2_text: {
                    required: true,
                    rangelength: [2, 30]
                },
                banner3_text: {
                    required: true,
                    rangelength: [2, 30]
                },
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            submitHandler: function (form) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('admin.banner.setting.save')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            message(data.message, 'success');
                            location.reload();
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            },
            messages: {
                banner1: {
                    required: 'Please select image',
                },
                banner2_text: {
                    required: 'Please enter banner title',
                },
                banner3_text: {
                    required: 'Please enter banner title',
                },
            },
        });

        var _URL = window.URL || window.webkitURL;
        $("#banner1FileUploader").change(function (e) {
            var file, img;
            var $this1 = this;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    if (this.width != 640 || this.height != 640) {
                        message('Please select height: 640px and width: 640px image.', 'danger');
                        $('#banner2FileUploader').val('');
                    } else {
                        readIMG1($this1);
                    }
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        });

        function readIMG1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#banner1_display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        /*$("#banner1FileUploader").change(function () {
            readIMG1(this);
        });*/

        $("#banner2FileUploader").change(function (e) {
            var file, img;
            var $this2 = this;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    if (this.width != 1270 || this.height != 640) {
                        // alert(this.width + " " + this.height);
                        message('Please select height: 640px and width: 1270px image.', 'danger');
                        $('#banner2FileUploader').val('');
                    } else {
                        readIMG2($this2);
                    }
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        });

        function readIMG2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#banner2_display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#banner3FileUploader").change(function (e) {
            var file, img;
            var $this3 = this;
            if ((file = this.files[0])) {
                img = new Image();
                var objectUrl = _URL.createObjectURL(file);
                img.onload = function () {
                    if (this.width != 260 || this.height != 152) {
                        message('Please select height: 152px and width: 260px image.', 'danger');
                        $('#banner3FileUploader').val('');
                    } else {
                        readIMG3($this3);
                    }
                    _URL.revokeObjectURL(objectUrl);
                };
                img.src = objectUrl;
            }
        });

        function readIMG3(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#banner3_display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        /*$("#banner3FileUploader").change(function () {
            readIMG3(this);
        });*/
    </script>


@endpush
