<div id="addModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Pincode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" role="form" id="addForm" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="register-form-errors"></div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Pincode</label>
                        <div class="col-sm-10">
                            <input name="pincode" pattern="[0-9]*" type="text" class="form-control" value="{{ old('pincode') }}">
                            @if ($errors->has('pincode'))
                                <span style="color:red;">{{$errors->first('pincode')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="editModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Pincode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form  method="post" role="form" id="editForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="register-form-errors"></div>
                {{csrf_field()}}
                <div id="edi_data_wrap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
        </div>
    </div>
</div>


<div class="modal fade in" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="width: 400px;">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <h4 class="modal-title">Confirm Delete</h4>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
            </div>
            <!--Modal body-->
            <div class="modal-body">
                <p>Are you sure you want to delete this Pincode?</p>
                <div class="text-right">
                    <form id="idForm" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" class="brand_del_id" value="" name="del_id">
                        <button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close">
                            Close
                        </button>
                        <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
