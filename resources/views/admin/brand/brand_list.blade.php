@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Brand Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Brand Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Brand Management</h5>
									<div class="float-right">
										<a href="javascript:;" class="btn btn-primary" id="addAjaxUser"><i class="fa fa-plus"></i> Add Brand </a>
									</div>
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="brand-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Brand Name</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
                                                {{--Ajax response--}}
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.brand.modals')
@endsection

@push('scripts')

    <script type="text/javascript">
        $(function () {
            oTable = $('#brand-table').DataTable({
                "processing": true,
                "serverSide": true,
                ajax: "{{ route('brand.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'brand_name', name: 'brand_name'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });

            $(document).on('click', '.status_list', function() {
                $.ajax({
                    type: "get",
                    url: "{{ route('brand.status.change', '') }}/"+$(this).data('id'),
                    data: {},
                    success: function(res) {
                        if (res.status) {
                            message(res.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    }
                });
            });
        });

        $('#addAjaxUser').click(function (event) {
            $('.modal-body').find("input,textarea,select").val('').end();
            $('#addModal').modal('show');
        })

        $('#addForm').validate({
            rules: {
                brand_name: {
                    required: true,
                }
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                brand_name: {
                    required: 'Please Enter Brand Name',
                },
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('add.brand')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#addModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $('#editForm').validate({
            rules: {
                brand_name: {
                    required: true,
                },
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                brand_name: {
                    required: 'Please Enter Brand Name',
                },
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('brand.update')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#editModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $(document).on('click','.data_edit',function(){
            $.ajax({
                type: "get",
                url: "{{ route('brand.edit', '') }}/"+$(this).data('id'),
                data: {},
                success: function(res) {
                    if (res.status) {
                        $('#editModal').modal('show');
                        $('#edi_data_wrap').html(res.data);
                    }
                }
            });

        });

        $(document).on('click','.data_delete',function(){
            $('#delete_modal').modal('show');
            $('.brand_del_id').val($(this).data('id'));
        });

        $("#idForm").submit(function (e) {
            var form = $('#idForm')[0];
            e.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('brand.delete')}}",
                type: "POST",
                data: new FormData(form),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    if (data.status) {
                        $('#delete_modal').modal('hide');
                        message(data.message, 'success');
                        oTable.rows().invalidate('data').draw(false);
                    }
                }
            });
        });
    </script>

@endpush
