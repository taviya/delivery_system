<div class="register-form-errors"></div>
<div class="form-group row">
    <input name="update_id" type="hidden" class="form-control" value="{{ $brand->id }}">
    <label class="col-sm-2 col-form-label">Brand Name</label>
    <div class="col-sm-10">
        <input name="brand_name" type="text" class="form-control"
               value="{{ $mode == 'Edit' ? $brand->brand_name : old('brand_name') }}">
        @if ($errors->has('brand_name'))
            <span style="color:red;">{{$errors->first('brand_name')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Status</label>
    <div class="col-sm-10">
        <div class="radio radio-inline">
            <input type="radio" name="status" value="1" @if($brand->status == 1) checked @endif>
            Active
        </div>
        <div class="radio radio-inline">
            <input type="radio" name="status" value="0" @if($brand->status == 0) checked @endif>
            Deactive
        </div>
        @if ($errors->has('status'))
            <span style="color:red;">{{$errors->first('status')}}</span>
        @endif
    </div>
</div>


