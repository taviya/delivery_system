@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Email Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Email Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Email Management</h5>
									{{-- <div class="float-right">
										<a href="javascript:;" class="btn btn-primary" id="add-main-category"><i class="fa fa-plus"></i> Add Sub Category </a>
									</div> --}}
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="users-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Email</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
											<tfoot>
												<tr>
													<th>ID</th>
													<th>Email</th>
													<th>Actions</th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
							<!-- Users Management end -->
						</div>
						<!-- Page-body end -->
					</div>
				</div>
				<!-- Main-body start -->

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.emailsubscriber.emailmodal')
@endsection

@push('scripts')

<script type="text/javascript">

		$(function () {
            oTable = $('#users-table').DataTable({
                "processing": true,
                "serverSide": true,
                ajax: "{{ route("email.index") }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'email', name: 'email', orderable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });
        });


	function deleteMainCategoryModel($id)
	{
		$('#deleteid').val($id);
		$('#delete_modal').modal('show');
	}

    
	$(document).on('click','.delete_btn', function() {
        $.ajax({
            type: "get",
            url: "{{ route('email.delete','') }}/"+$('#deleteid').val(),
            data: {},
            success: function(data) {
            	if(data.status==1){
					$('#delete_modal').modal('hide');
					message(data.messages, 'success');
                    oTable.rows().invalidate('data').draw(false);
				}
            }
        });
    });

</script>

@endpush