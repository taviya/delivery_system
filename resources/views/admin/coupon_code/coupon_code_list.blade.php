@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Coupon Code Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Coupon Code Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<div class="page-body">
							@include('admin.messages')
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Coupon Code Management</h5>
									<div class="float-right">
										<a href="javascript:;" class="btn btn-primary" id="addAjaxUser"><i class="fa fa-plus"></i> Add Coupon Code </a>
									</div>
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="slider-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Name</th>
													<th>Percentage Discount</th>
													<th>Max Discount Amount</th>
													<th>Expired Date</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
                                                {{--Ajax response--}}
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.coupon_code.modals')
@endsection

@push('scripts')

    <script type="text/javascript">
        $(function () {
            oTable = $('#slider-table').DataTable({
                "processing": true,
                "serverSide": true,
                ajax: "{{ route('coupon.code.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'percentage_discount', name: 'percentage_discount', orderable: false,},
                    {data: 'expired_date', name: 'expired_date'},
                    {data: 'max_discount_amount', name: 'max_discount_amount'},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });

            $("#expired_date").datepicker({
                dateFormat: 'dd-mm-yy',
                minDate: 0
            });

            $(document).on('click', '.status_list', function() {
                $.ajax({
                    type: "get",
                    url: "{{ route('coupon.code.status.change', '') }}/"+$(this).data('id'),
                    data: {},
                    success: function(res) {
                        if (res.status) {
                            message(res.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    }
                });
            });
        });

        $('#addAjaxUser').click(function (event) {
            $('.modal-body').find("input,textarea,select").val('').end();
            $('.register-form-errors').html('');
            $('#addModal').modal('show');
        })

        $('#addForm').validate({
            rules: {
                name: {
                    required: true,
                    remote: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('coupon.code.check')}}",
                        type: "post",
                        data: {
                            name: function () {
                                return $('#coupon_code_name').val();
                            }
                        },
                        dataFilter: function (data) {
                            var json = JSON.parse(data);
                            if (json.msg == "true") {
                                return "\"" + "Coupon already exist, please add different coupon code" + "\"";
                            } else {
                                return 'true';
                            }
                        }
                    }
                },
                percentage_discount: {
                    required: true,
                    max: 100,
                    min: 0.01
                },
                max_discount_amount: {
                    required: true,
                    max: 3000,
                    min: 0.01
                },
                description: {
                    required: true,
                },
                expired_date: {
                    required: true,
                }
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                name: {
                    required: 'Please enter coupon name',
                },
                percentage_discount: {
                    required: 'Please enter percentage discount',
                },
                max_discount_amount: {
                    required: 'Please enter maximum discount amount',
                },
                description: {
                    required: 'Please enter description',
                },
                expired_date: {
                    required: 'Please select expired date',
                }
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('add.coupon.code')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#addModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $('#editForm').validate({
            rules: {
                name: {
                    required: true,
                    remote: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "{{route('coupon.code.check')}}",
                        type: "post",
                        data: {
                            name: function () {
                                return $('#edit_coupon_code_name').val();
                            },
                            update_id: function () {
                                return $('#update_id').val();
                            },
                        },
                        dataFilter: function (data) {
                            var json = JSON.parse(data);
                            if (json.msg == "true") {
                                return "\"" + "Coupon already exist, please add different coupon code" + "\"";
                            } else {
                                return 'true';
                            }
                        }
                    }
                },
                percentage_discount: {
                    required: true,
                    max: 100,
                    min: 0.01
                },
                max_discount_amount: {
                    required: true,
                    max: 3000,
                    min: 0.01
                },
                description: {
                    required: true,
                },
                expired_date: {
                    required: true,
                }
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                name: {
                    required: 'Please enter coupon name',
                },
                percentage_discount: {
                    required: 'Please enter percentage discount',
                },
                max_discount_amount: {
                    required: 'Please enter maximum discount amount',
                },
                description: {
                    required: 'Please enter description',
                },
                expired_date: {
                    required: 'Please select expired date',
                }
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('coupon.code.update')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#editModal').modal('hide');
                            message(data.message, 'success');
                            oTable.rows().invalidate('data').draw(false);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $(document).on('click','.data_edit',function(){
            $.ajax({
                type: "get",
                url: "{{ route('coupon.code.edit', '') }}/"+$(this).data('id'),
                data: {},
                success: function(res) {
                    if (res.status) {
                        $('#editModal').modal('show');
                        $('#edi_data_wrap').html(res.data);
                        $("#edit_expired_date").datepicker({
                            dateFormat: 'dd-mm-yy',
                            minDate: 0
                        });
                    }
                }
            });

        });

        $(document).on('click','.data_delete',function(){
            $('#delete_modal').modal('show');
            $('.coupon_code_del_id').val($(this).data('id'));
        });

        $("#idForm").submit(function (e) {
            var form = $('#idForm')[0];
            e.preventDefault();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('coupon.code.delete')}}",
                type: "POST",
                data: new FormData(form),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(data) {
                    if (data.status) {
                        $('#delete_modal').modal('hide');
                        message(data.message, 'success');
                        oTable.rows().invalidate('data').draw(false);
                    }
                }
            });
        });

        function readIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#fileUploader").change(function () {
            $("#display_image").css("display", "block");
            readIMG(this);
        });

        function editReadIMG(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#edit_display_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#editFileUploader', function() {
            editReadIMG(this);
        });
    </script>

@endpush
