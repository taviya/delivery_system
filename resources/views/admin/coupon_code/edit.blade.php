<div class="register-form-errors"></div>
<div class="form-group row">
    <input name="update_id" id="update_id" type="hidden" class="form-control" value="{{ $couponCode->id }}">
    <label class="col-sm-2 col-form-label">Title</label>
    <div class="col-sm-10">
        <input name="name" id="edit_coupon_code_name" type="text" class="form-control"
               value="{{ $couponCode->name }}">
        @if ($errors->has('name'))
            <span style="color:red;">{{$errors->first('name')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Percentage Discount</label>
    <div class="col-sm-10">
        <input name="percentage_discount" type="number" class="form-control" value="{{ $couponCode->percentage_discount }}">
        @if ($errors->has('percentage_discount'))
            <span style="color:red;">{{$errors->first('percentage_discount')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Maximum Discount Amount</label>
    <div class="col-sm-10">
        <input name="max_discount_amount" type="number" class="form-control" value="{{ $couponCode->max_discount_amount }}">
        @if ($errors->has('max_discount_amount'))
            <span style="color:red;">{{$errors->first('max_discount_amount')}}</span>
        @endif
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-10">
        <textarea rows="5" cols="5" name="description" class="form-control">{{ $couponCode->description }}</textarea>
        @if ($errors->has('description'))
            <span style="color:red;">{{$errors->first('description')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Expired Date</label>
    <div class="col-sm-10">
        <input class="form-control bg-white" type="text" placeholder="dd/mm/yyyy" id="edit_expired_date" readonly name="expired_date" value="{{ date('d-m-Y', strtotime($couponCode->expired_date)) }}">
        @if ($errors->has('expired_date'))
            <span style="color:red;">{{$errors->first('expired_date')}}</span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Status</label>
    <div class="col-sm-10">
        <div class="radio radio-inline">
            <input type="radio" name="status" value="1" @if($couponCode->status == 1) checked @endif>
            Active
        </div>
        <div class="radio radio-inline">
            <input type="radio" name="status" value="0" @if($couponCode->status == 0) checked @endif>
            Deactive
        </div>
        @if ($errors->has('status'))
            <span style="color:red;">{{$errors->first('status')}}</span>
        @endif
    </div>
</div>


