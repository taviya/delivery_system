<div id="addModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Coupon Code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" role="form" id="addForm" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="register-form-errors"></div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input name="name" id="coupon_code_name" type="text" class="form-control" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span style="color:red;">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Percentage Discount</label>
                        <div class="col-sm-10">
                            <input name="percentage_discount" type="number" class="form-control" value="{{ old('percentage_discount') }}">
                            @if ($errors->has('percentage_discount'))
                                <span style="color:red;">{{$errors->first('percentage_discount')}}</span>
                            @endif
                            <span>NOTE: Enter coupon discount in percentage.</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Maximum Discount Amount</label>
                        <div class="col-sm-10">
                            <input name="max_discount_amount" type="number" class="form-control" value="{{ old('max_discount_amount') }}">
                            @if ($errors->has('max_discount_amount'))
                                <span style="color:red;">{{$errors->first('max_discount_amount')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <textarea rows="5" cols="5" name="description" class="form-control">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <span style="color:red;">{{$errors->first('description')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Expired Date</label>
                        <div class="col-sm-10">
                            <input class="form-control bg-white" type="text" placeholder="dd/mm/yyyy" id="expired_date" readonly name="expired_date" value="{{ old('expired_date') }}">
                            @if ($errors->has('expired_date'))
                                <span style="color:red;">{{$errors->first('expired_date')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="editModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Coupon Code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form  method="post" role="form" id="editForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="register-form-errors"></div>
                {{csrf_field()}}
                <div id="edi_data_wrap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
        </div>
    </div>
</div>


<div class="modal fade in" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" style="width: 400px;">
        <div class="modal-content">
            <!--Modal header-->
            <div class="modal-header">
                <h4 class="modal-title">Confirm Delete</h4>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
            </div>
            <!--Modal body-->
            <div class="modal-body">
                <p>Are you sure you want to delete this coupon code?</p>
                <div class="text-right">
                    <form id="idForm" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" class="coupon_code_del_id" value="" name="del_id">
                        <button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close">
                            Close
                        </button>
                        <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
