@extends('admin.layouts.app')
@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            @include('admin.include.sidebar')
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="page-header">
                                <div class="page-header-title">
                                    <h4>Dashboard</h4>
                                </div>
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="index-2.html">
                                                <i class="icofont icofont-home"></i>
                                            </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Dashboard</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="page-body">
                                <div class="row">
                                    @if(userRoleCheck([1]) && isset($userCount))
                                        <div class="col-md-6 col-xl-3">
                                            <a href="{{ route('users.index') }}">
                                                <div class="card social-widget-card">
                                                    <div class="card-block-big bg-facebook">
                                                        <h3>{{ $userCount }}</h3>
                                                        <span class="m-t-10">Users</span>
                                                            <i class="icofont icofont-user-alt-3"></i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                    @if(userRoleCheck([1]) && isset($shopCount))
                                        <div class="col-md-6 col-xl-3">
                                            <a href="{{ route('shop.list') }}">
                                                <div class="card social-widget-card">
                                                    <div class="card-block-big bg-twitter">
                                                        <h3>{{ $shopCount }}</h3>
                                                        <span class="m-t-10">Shops</span>
                                                        <i class="icofont icofont-social-shopify"></i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endif
                                    @if(userRoleCheck([1, 2]) && isset($productCount))
                                        <div class="col-md-6 col-xl-3">
                                        <a href="{{ route('product.index') }}">
                                            <div class="card social-widget-card">
                                                <div class="card-block-big bg-linkein">
                                                    <h3>{{ $productCount }}</h3>
                                                    <span class="m-t-10">Product</span>
                                                    <i class="icofont icofont-grocery"></i>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    @endif
                                    @if(userRoleCheck([3]))
                                        <div class="col-md-6 col-xl-3">
                                        <a href="{{ route('product.index') }}">
                                            <div class="card social-widget-card">
                                                <div class="card-block-big bg-success">
                                                    <h3>{{ $productCount }}</h3>
                                                    <span class="m-t-10">Delivered</span>
                                                    <i class="icofont icofont-plus-square"></i>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    @endif
                                    {{--@if(userRoleCheck([3]))
                                        <div class="col-md-6 col-xl-3">
                                        <a href="{{ route('product.index') }}">
                                            <div class="card social-widget-card">
                                                <div class="card-block-big bg-warning">
                                                    <h3>{{ $productCount }}</h3>
                                                    <span class="m-t-10">Pending Delivered</span>
                                                    <i class="icofont icofont-exclamation"></i>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    @endif--}}
                                    <div class="col-md-6 col-xl-3">
                                        <a href="">
                                            <div class="card social-widget-card">
                                                <div class="card-block-big bg-google-plus">
                                                    <h3>650</h3>
                                                    <span class="m-t-10">Order</span>
                                                    <i class="icofont icofont-cart-alt"></i>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    {{--<div class="col-md-12 col-xl-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Latest 5 Orders</h5>
                                                <div class="card-header-right">
                                                    <i class="icofont icofont-rounded-down"></i>
                                                    <i class="icofont icofont-refresh"></i>
                                                    <i class="icofont icofont-close-circled"></i>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Mobile</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(!empty($user5))
                                                            @foreach($user5 as $singleUser)
                                                                <tr class="cursor-pointer" id="last_10_user" data-id="{{ $singleUser->id }}">
                                                                    <td>{{ $singleUser->first_name.' '.$singleUser->first_last }}</td>
                                                                    <td>{{ $singleUser->email }}</td>
                                                                    <td>{{ $singleUser->mobile_no }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}
                                </div>
                                <div class="row">
                                    @if(isset($user5))
                                        <div class="col-md-6 col-xl-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Latest 5 Users</h5>
                                                <div class="card-header-right">
                                                    <i class="icofont icofont-rounded-down"></i>
                                                    <i class="icofont icofont-refresh"></i>
                                                    <i class="icofont icofont-close-circled"></i>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Email</th>
                                                            <th>Mobile</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(!empty($user5))
                                                            @foreach($user5 as $singleUser)
                                                                <tr class="cursor-pointer" id="last_10_user" data-id="{{ $singleUser->id }}">
                                                                    <td>{{ $singleUser->first_name.' '.$singleUser->first_last }}</td>
                                                                    <td>{{ $singleUser->email }}</td>
                                                                    <td>{{ $singleUser->mobile_no }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(isset($shop5))
                                        <div class="col-md-6 col-xl-6">
                                        <div class="card">
                                            <div class="card-header">
                                                <h5>Latest 5 Shops</h5>
                                                <div class="card-header-right">
                                                    <i class="icofont icofont-rounded-down"></i>
                                                    <i class="icofont icofont-refresh"></i>
                                                    <i class="icofont icofont-close-circled"></i>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th>Shop Name</th>
                                                            <th>Email</th>
                                                            <th>Mobile</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @if(!empty($shop5))
                                                            @foreach($shop5 as $singleShop)
                                                                <tr class="cursor-pointer" id="last_10_shop">
                                                                    <td>{{ $singleShop->shop_name }}</td>
                                                                    <td>{{ $singleShop->email }}</td>
                                                                    <td>{{ $singleShop->mobile_no }}</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if(userRoleCheck([2, 3]))
                                        <div class="col-md-12 col-xl-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Latest 5 Order</h5>
                                                    <div class="card-header-right">
                                                        <i class="icofont icofont-rounded-down"></i>
                                                        <i class="icofont icofont-refresh"></i>
                                                        <i class="icofont icofont-close-circled"></i>
                                                    </div>
                                                </div>
                                                <div class="card-block table-border-style">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Delivered To</th>
                                                                <th>Product Name</th>
                                                                @if(!userRoleCheck([2]))
                                                                    <th>Shop Address</th>
                                                                @endif
                                                                <th>Shipping Address</th>
                                                                <th>Mobile</th>
                                                                @if(userRoleCheck([3]))
                                                                    <th>Action</th>
                                                                @endif
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if($order_latest5->isNotEmpty())
                                                                @foreach($order_latest5 as $order)
                                                                    <tr class="cursor-pointer">
                                                                        <td>{{ $order->getShippingOrder->getShippingOrderAddress->ship_first_name.' '.$order->getShippingOrder->getShippingOrderAddress->ship_last_name }}</td>
                                                                        <td>{{ $order->getProductForOrder->product_name }}</td>
                                                                        @if(!userRoleCheck([2]))
                                                                            <td>{{ $order->getProductShopForOrder->address }}</td>
                                                                        @endif
                                                                        <td>{{ $order->getShippingOrder->getShippingOrderAddress->street_address_1.', '.$order->getShippingOrder->getShippingOrderAddress->street_address_2 }}</td>
                                                                        <td>{{ $order->getShippingOrder->getShippingOrderAddress->phone_number }}</td>
                                                                        @if(userRoleCheck([3]))
                                                                            <td><label class="label label-info send_otp cursor-pointer" data-order-id="{{ $order->id }}">Send Otp</label></td>
                                                                        @endif
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr><td>Order not available</td></tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(userRoleCheck([2]))
                                        <div class="col-md-12 col-xl-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Latest 5 Product</h5>
                                                    <div class="card-header-right">
                                                        <i class="icofont icofont-rounded-down"></i>
                                                        <i class="icofont icofont-refresh"></i>
                                                        <i class="icofont icofont-close-circled"></i>
                                                    </div>
                                                </div>
                                                <div class="card-block table-border-style">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Product Name</th>
                                                                <th>Product Image</th>
                                                                <th>Original Price</th>
                                                                <th>Stock</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @if($shop_latest_product5->isNotEmpty())
                                                                @foreach($shop_latest_product5 as $product)
                                                                    <?php
                                                                        $product_image = explode(',', $product->product_image);
                                                                        $url = asset('public/upload/product/thumbnail/' . $product_image[0]);
                                                                    ?>
                                                                    <tr class="cursor-pointer">
                                                                        <td>{{ $product->product_name }}</td>
                                                                        <td><img src="{{ $url }}" border="0" width="75" class="img-rounded" align="center"></td>
                                                                        <td>{{ $product->price }}</td>
                                                                        <td>{{ $product->stock }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr><td>Product not available</td></tr>
                                                            @endif
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div id="styleSelector">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- For otp -->
    <div class="modal fade" id="otp_modal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" role="form" id="confirm_delivery_otp">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Delivery Otp</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Enter Otp</label>
                            <div class="col-sm-8">
                                <input name="otp" type="number" class="form-control" value="">
                                <p class="text-danger otp_server_error"></p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Confirm</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script>
        // Send otp for delivery
        $('.send_otp').click(function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "post",
                url: "{{ route('otp.send.for.delivery') }}",
                data: {'order_id': $(this).data('order-id')},
                dataType: 'json',
                beforeSend: function() {
                    $("#loading").show();
                },
                success: function(res) {
                    if (res.status) {
                        $("#loading").hide();
                        $("#confirm_delivery_otp")[0].reset();
                        $(".otp_server_error").html('');
                        $("#otp_modal").modal('show');
                    }
                }
            });
        })

        $('#confirm_delivery_otp').validate({
            rules: {
                otp: {
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                    digits: true,
                }
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio")) {
                    var name = element.attr('name');
                    error.insertAfter("#" + name + "_radio-error");
                } else {
                    error.appendTo(element.parent());
                }
            },
            messages: {
                otp: {
                    required: 'Please Enter Otp',
                    minlength: 'Please Enter 6 Digit OTP',
                }
            },
            submitHandler: function (form) {
                $('#loading').show();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "{{route('order.confirm')}}",
                    type: "POST",
                    data: new FormData(form),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {
                        $('#loading').hide();
                        if (data.status) {
                            $('#addModal').modal('hide');
                            message(data.message, 'success');
                        }else{
                            $(".otp_server_error").html(data.message);
                        }
                    },
                    error: function (data) {
                        $('#loading').hide();
                        var errorString = '';
                        $.each(data.responseJSON.errors, function (key, value) {
                            errorString += value + '<br>';
                        });
                        message(errorString, 'danger');
                    },
                });
                return false;
            }
        });

        $('#last_10_user').click(function () {
            window.location = '{{ route('users.view', '') }}/' + $(this).data('id')
        })

        $('#last_10_shop').click(function () {
            window.location = '{{ route('shop.list') }}'
        })
    </script>

@endpush
