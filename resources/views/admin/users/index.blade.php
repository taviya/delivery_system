@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Users Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Users Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Users Management</h5>
									<div class="float-right">
										<a href="javascript:;" id="add-new-user" class="btn btn-primary"><i class="fa fa-plus"></i> Add New User</a>
									</div>
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="users-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Image</th>
													<th>Name</th>
													<th>Email</th>
													<th>Mobile No</th>
													<th>Gender</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
											<tfoot>
												<tr>
													<th>ID</th>
													<th>Image</th>
													<th>Name</th>
													<th>Email</th>
													<th>Mobile No</th>
													<th>Gender</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
							<!-- Users Management end -->
						</div>
						<!-- Page-body end -->
					</div>
				</div>
				<!-- Main-body start -->

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.users.user-modals')
@endsection

@push('scripts')
<script>
	
	$(function () {
        oTable = $('#users-table').DataTable({
            "processing": true,
            "serverSide": true,
            ajax: "{{ route("users.index") }}",
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'avatar_location', name: 'avatar_location', sortable: false},
				{data: 'name', name: 'name', sortable: false},
				{data: 'email', name: 'email', sortable: false},
				{data: 'mobile_no', name: 'mobile_no', sortable: false},
				{data: 'gender', name: 'gender',sortable: true},
				{data: 'status', name: 'status',sortable: true},
				{data: 'actions', name: 'actions', searchable: false, sortable: false,"width": "15%"}
            ],
        });

        $(document).on('click', '.status-update', function() {
            $.ajax({
                type: "get",
                url: "{{ route('users.status.update', '') }}/"+$(this).data('id'),
                data: {},
                success: function(res) {
                    if (res.status) {
                    	message(res.messages, 'success');
                        oTable.rows().invalidate('data').draw(false);
                    }
                }
            });
        });
    });

	$('#add-new-user').click(function(event) {
		$('.modal-body').find("input[type=text],input[type=email],input[type=number],input[type=password],textarea,select").val('').end();
		$('#main-user-modal').modal('show');
	})

	$('#add-main-user-form').validate({
		rules: {
			first_name: {
				required: true,
			},
			last_name: {
				required: true,
			},
			mobile_no: {
				required: true,
				minlength:10,
				maxlength:10,
				digits: true,
				remote: {
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: base_url + "/user/checkmobileno",
					type: "post",
					data: {
						mobileno: function() {
							return $('#mobilenumber').val();
						}
					},
				    dataFilter: function (data) {
				        var json = JSON.parse(data);
				        if (json.msg == "true") {
				            return "\"" + "Mobile No already exist, please add different mobile no" + "\"";
				        } else {
				            return 'true';
				        }
				    }
				}
			},
			email: {
				required: true,
				email:true,
				remote: {
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: base_url + "/user/checkemail",
					type: "post",
					data: {
						email: function() {
							return $('#emailaddress').val();
						}
					},
				    dataFilter: function (data) {
				        var json = JSON.parse(data);
				        if (json.msg == "true") {
				            return "\"" + "Email already exist, please add different email address" + "\"";
				        } else {
				            return 'true';
				        }
				    }
				}
			},
			gender: {
				required: true,
			},
			password: {
				required: true,
				minlength: 8,
			},
			password_confirmation: {
				required: true,
				equalTo: "#password",
			},
		},
		errorPlacement: function(error, element) {
			if (element.is(":radio")) {
				var name = element.attr('name');
				error.insertAfter("#"+name+"_radio-error");
			}else {
				error.appendTo(element.parent());
			}
		},
		messages:{
			first_name: {
				required: 'Please Enter First Name',
			},
			last_name: {
				required: 'Please Enter Last Name',
			},
			mobile_no: {
				required: 'Please Enter Your Mobile No',
				minlength: 'Please Enter 10 Digit Mobile No',
				maxlength: 'Please Enter 10 Digit Mobile No',
			},
			email: {
				required: 'Please Enter Your Email',
			},
			gender: {
				required: 'Please select gender',
			},
			password:{
				required: 'Please Enter Your Password',
				minlength: 'Password Length must be 8 Character',
			},
			password_confirmation:{
				required: 'Please Confirm Your Password',
			},
		},
		submitHandler: function(form) {
			$('#loading').show();
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: "{{route('user.store')}}",
				type: "POST",
				data: new FormData(form),
				dataType: 'json',
				contentType: false,
				cache: false,
				processData:false,
				success: function (data) {
					$('#loading').hide();
					if(data.status==1){
						$('#main-user-modal').modal('hide');
						message(data.messages, 'success');
                        oTable.rows().invalidate('data').draw(false);
					}
				},
				error: function (data) {
					$('#loading').hide();
					var errorString = '<ul>';
					$.each(data.responseJSON.errors, function( key, value) {
						errorString += '<li>' + value + '</li>';
					});
					errorString += '</ul>';
					message(errorString, 'danger');
				},
			});
			return false;
		}
	});

	function deleteMainCategoryModel($id)
	{
		$('#deleteid').val($id);
		$('#delete_modal').modal('show');
	}

	function deleteUserModel($id)
	{
		$('#deleteid').val($id);
		$('#delete_modal').modal('show');
	}

	function UserBlockModal($id)
	{
		$('#blockid').val($id);
		$('#block_modal').modal('show');
	}

	function UserUnblockModal($id)
	{
		$('#unblockid').val($id);
		$('#unblock_modal').modal('show');
	}

    function editMainCategoryModel(id)
    {
        var getUrl = window.location;
        var routeUrl = getUrl + '/edit' + "/" + id;

        $('#edi_data_wrap').html('');
        $('#loading').show();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: routeUrl,
            type: "get",
            dataType: 'json',
            success: function (data) {
                $('#loading').hide();
                $('#edi_data_wrap').html(data.data);
                $('#editModal').modal('show');
            },
            error: function (data) {
            },
        });
    }

    $('#editForm').validate({
        rules: {
			first_name: {
				required: true,
			},
			last_name: {
				required: true,
			},
			mobile_no: {
				required: true,
				minlength:10,
				maxlength:10,
				digits: true,
				remote: {
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: base_url + "/user/checkmobileno",
					type: "post",
					data: {
						mobileno: function() {
							return $('#editmobilenumber').val();
						},
						update_id: function() {
							return $('#updateId').val();
						}
					},
				    dataFilter: function (data) {
				        var json = JSON.parse(data);
				        if (json.msg == "true") {
				            return "\"" + "Mobile No already exist, please add different mobile no" + "\"";
				        } else {
				            return 'true';
				        }
				    }
				}
			},
			email: {
				required: true,
				email:true,
				remote: {
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: base_url + "/user/checkemail",
					type: "post",
					data: {
						email: function() {
							return $('#editemailaddress').val();
						},
						update_id: function() {
							return $('#updateId').val();
						}
					},
				    dataFilter: function (data) {
				        var json = JSON.parse(data);
				        if (json.msg == "true") {
				            return "\"" + "Email already exist, please add different email address" + "\"";
				        } else {
				            return 'true';
				        }
				    }
				}
			},
			gender: {
				required: true,
			},
		},
		errorPlacement: function(error, element) {
			if (element.is(":radio")) {
				var name = element.attr('name');
				error.insertAfter("#"+name+"_edit_radio-error");
			}else {
				error.appendTo(element.parent());
			}
		},
		messages:{
			first_name: {
				required: 'Please Enter First Name',
			},
			last_name: {
				required: 'Please Enter Last Name',
			},
			mobile_no: {
				required: 'Please Enter Your Mobile No',
				minlength: 'Please Enter 10 Digit Mobile No',
				maxlength: 'Please Enter 10 Digit Mobile No',
			},
			email: {
				required: 'Please Enter Your Email',
			},
			gender: {
				required: 'Please select gender',
			},
		},
    	submitHandler: function(form) {
	        $('#loading').show();
	        $.ajax({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            },
	            url: "{{ route('user.detail.update') }}",
	            type: "POST",
	            data: $("#editForm").serialize(),
	            dataType: 'json',
	            success: function (data) {
	                $('#loading').hide();
	                if(data.status==1){
	                    $("#editModal").modal('hide');
	                    message(data.messages, 'success');
	                    oTable.rows().invalidate('data').draw(false);
	                }
	            },
	            error: function (data) {
	                $('#loading').hide();
	                var errorString = '<ul>';
	                $.each(data.responseJSON.errors, function( key, value) {
	                    errorString += '<li>' + value + '</li>';
	                });
	                errorString += '</ul>';
	                message(errorString, 'danger');
	            },
	        });
        	return false;
    	}
	});

	$(document).on('click','.delete_btn', function() {
        $.ajax({
            type: "get",
            url: "{{ route('users.destroy','') }}/"+$('#deleteid').val(),
            data: {},
            success: function(data) {
            	if(data.status==1){
					$('#delete_modal').modal('hide');
					message(data.messages, 'success');
                    oTable.rows().invalidate('data').draw(false);
				}
            }
        });
    });

   
</script>

@endpush