<div id="main-user-modal" class="modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add New User</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" role="form" id="add-main-user-form" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<div class="register-form-errors"></div>
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">First Name</label>
						<div class="col-sm-10">
							<input name="first_name" type="text" class="form-control" value="{{ old('first_name') }}">
							@if ($errors->has('first_name'))
							<span style="color:red;">{{$errors->first('first_name')}}</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Last Name</label>
						<div class="col-sm-10">
							<input name="last_name" type="text" class="form-control" value="{{ old('last_name') }}">
							@if ($errors->has('last_name'))
							<span style="color:red;">{{$errors->first('last_name')}}</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">E-Mail Address</label>
						<div class="col-sm-10">
							<input name="email" id="emailaddress" type="email" class="form-control" value="{{ old('email') }}">
							@if ($errors->has('email'))
							<span style="color:red;">{{$errors->first('email')}}</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Mobile No</label>
						<div class="col-sm-10">
							<input name="mobile_no" id="mobilenumber" type="number" class="form-control" value="{{ old('mobile_no') }}">
							@if ($errors->has('mobile_no'))
							<span style="color:red;">{{$errors->first('mobile_no')}}</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="Gender" class="col-sm-2 col-form-label">{{ __('Gender') }}:</label>
						<div class="col-sm-10">
							<label class="radio-label">
								<input type="radio" name="gender" value="1"  {{(old('gender') == '1') ? 'checked' : ''}}>
								{{ __('Male') }}
							</label>
							<label class="radio-label">
								<input type="radio" name="gender" value="2" {{(old('gender') == '2') ? 'checked' : ''}}>
								{{ __('Female') }}
							</label>
							</br>
							<span id="gender_radio-error"></span>
							@error('gender')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="col-sm-2 col-form-label">{{ __('Password') }}</label>

						<div class="col-sm-10">
							<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">

							@error('password')
							<span class="invalid-feedback" role="alert">
								<strong>{{ $message }}</strong>
							</span>
							@enderror
						</div>
					</div>

					<div class="form-group row">
						<label for="password-confirm" class="col-sm-2 col-form-label">{{ __('Confirm Password') }}</label>

						<div class="col-sm-10">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade bd-example-modal-lg" id="editModal" tabindex="-1" role="dialog"
aria-labelledby="addServiceTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLongTitle">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form  method="post" role="form" id="editForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="register-form-errors"></div>
                {{csrf_field()}}
                <div id="edi_data_wrap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
</div>
</div>

<div class="modal fade in" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" style="width: 400px;">
		<div class="modal-content">
			<!--Modal header-->
			<div class="modal-header">
				<h4 class="modal-title">Confirm Delete</h4>
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
			</div>
			<!--Modal body-->
			<div class="modal-body">
				<p>Are You Sure You Want To Delete This User?</p>
				<div class="text-right">
					<form method="post" role="form" id="deleteForm" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="id" id="deleteid">
						<button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close">Close</button>
						<button type="button" class="btn btn-danger btn-sm delete_btn">Delete</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>