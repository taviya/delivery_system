<input name="update_id" id="updateId" type="hidden" class="form-control" value="{{ $users->id }}">
<div class="form-group row">
	<label class="col-sm-2 col-form-label">First Name</label>
	<div class="col-sm-10">
		<input name="first_name" type="text" class="form-control" value="{{ $users->first_name }}">
		@if ($errors->has('first_name'))
		<span style="color:red;">{{$errors->first('first_name')}}</span>
		@endif
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Last Name</label>
	<div class="col-sm-10">
		<input name="last_name" type="text" class="form-control" value="{{ $users->last_name }}">
		@if ($errors->has('last_name'))
		<span style="color:red;">{{$errors->first('last_name')}}</span>
		@endif
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">E-Mail Address</label>
	<div class="col-sm-10">
		<input name="email" id="editemailaddress" type="email" class="form-control" value="{{ $users->email }}">
		@if ($errors->has('email'))
		<span style="color:red;">{{$errors->first('email')}}</span>
		@endif
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-2 col-form-label">Mobile No</label>
	<div class="col-sm-10">
		<input name="mobile_no" id="editmobilenumber" type="number" class="form-control" value="{{ $users->mobile_no }}">
		@if ($errors->has('mobile_no'))
		<span style="color:red;">{{$errors->first('mobile_no')}}</span>
		@endif
	</div>
</div>

<div class="form-group row">
	<label for="Gender" class="col-sm-2 col-form-label">{{ __('Gender') }}:</label>
	<div class="col-sm-10">
		<label class="radio-label">
			<input type="radio" name="gender" value="1" {{ ($users->gender == '1') ? 'checked' : ''}}>
			{{ __('Male') }}
		</label>
		<label class="radio-label">
			<input type="radio" name="gender" value="2" {{( $users->gender == '2') ? 'checked' : ''}}>
			{{ __('Female') }}
		</label>
	</br>
	<span id="gender_edit_radio-error"></span>
	@error('gender')
	<span class="invalid-feedback" role="alert">
		<strong>{{ $message }}</strong>
	</span>
	@enderror
</div>
</div>