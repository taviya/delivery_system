<div class="modal fade in" id="delete_modal" role="dialog" tabindex="-1" aria-labelledby="delete_modal" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered" style="width: 400px;">
    <div class="modal-content">
      <!--Modal header-->
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete</h4>
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
      </div>
      <!--Modal body-->
      <div class="modal-body">
        <p>Are you sure you want to delete this product?</p>
        <div class="text-right">
          <form method="post" role="form" id="deleteForm" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="deleteid">
            <button data-dismiss="modal" class="btn btn-default btn-sm" type="button" id="modal_close">Close</button>
            <button type="button" class="btn btn-danger btn-sm delete_btn">Delete</button>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>