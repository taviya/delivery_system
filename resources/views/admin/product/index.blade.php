@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page-header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Product Management</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Product Management</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page-header end -->
						<!-- Page-body start -->
						<div class="page-body">
							<!-- Table header styling table start -->
							@include('admin.messages')
							<!-- Users Management table start -->
							<div class="card">
								<div class="card-header table-card-header">
									<h5>Product Management</h5>
									<div class="float-right">
										<a href="{{ route('product.create') }}" class="btn btn-primary" id="add-main-category"><i class="fa fa-plus"></i> Add New Product </a>
									</div>
								</div>
								<div class="card-block">
									<div class="dt-responsive table-responsive">
										<table id="users-table" class="table table-striped table-bordered nowrap">
											<thead>
												<tr>
													<th>ID</th>
													<th>Product Image</th>
													<th>Product Name</th>
													<th>Main Category Name</th>
													<th>Sub Category Name</th>
													<th>Shop Name</th>
													<th>Original Price</th>
													<th>Original Price + GST</th>
													<th>Original Price + GST + Commission</th>
													<th>Stock</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody>

											</tbody>
											<tfoot>
												<tr>
													<th>ID</th>
													<th>Product Image</th>
													<th>Product Name</th>
													<th>Main Category Name</th>
													<th>Sub Category Name</th>
													<th>Shop Name</th>
													<th>Original Price</th>
													<th>Original Price + GST</th>
													<th>Original Price + GST + Commission</th>
													<th>Stock</th>
													<th>Status</th>
													<th>Actions</th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
							<!-- Users Management end -->
						</div>
						<!-- Page-body end -->
					</div>
				</div>
				<!-- Main-body start -->

				<div id="styleSelector">

				</div>

			</div>
		</div>
	</div>
</div>
@include('admin.product.productmodal')
@endsection

@push('scripts')

<script type="text/javascript">

	$(function () {
	        showAdminColumns = "{{ userRoleCheck([1]) }}" == "1" ? true : false;
            oTable = $('#users-table').DataTable({
                "processing": true,
                "serverSide": true,
                ajax: "{{ route("product.index") }}",
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
                    {data: 'images', name: 'images', orderable: false, searchable: false},
                    {data: 'product_name', name: 'product_name', orderable: true},
                    {data: 'main_category_name', name: 'mc.main_category_name', orderable: true},
                    {data: 'sub_category_name', name: 'sc.sub_category_name', orderable: true},
                    {data: 'shop_name', name: 's.shop_name', orderable: true},
                    {data: 'original_price', name: 'original_price', orderable: false},
                    {data: 'price_gst', name: 'price_gst', orderable: false},
                    {data: 'price', name: 'price', visible: showAdminColumns, orderable: false},
                    {data: 'stock', name: 'stock', orderable: false},
                    {data: 'status', name: 'status', orderable: true, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
            });

        $(document).on('click', '.status-update', function() {
            $.ajax({
                type: "get",
                url: "{{ route('product.status.update', '') }}/"+$(this).data('id'),
                data: {},
                success: function(res) {
                    if (res.status) {
                       	message(res.message, 'success');
                        oTable.rows().invalidate('data').draw(false);
                    }
                }
            });
        });
    });


	function deleteMainCategoryModel($id)
	{
		$('#deleteid').val($id);
		$('#delete_modal').modal('show');
	}

	$(document).on('click','.delete_btn', function() {
        $.ajax({
            type: "get",
            url: "{{ route('product.delete','') }}/"+$('#deleteid').val(),
            data: {},
            success: function(data) {
            	if(data.status==1){
					$('#delete_modal').modal('hide');
					message(data.messages, 'success');
                    oTable.rows().invalidate('data').draw(false);
				}
            }
        });
    });

</script>

@endpush
