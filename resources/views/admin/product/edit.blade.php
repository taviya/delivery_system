@extends('admin.layouts.app')
@section('content')
@include('admin.include.sidebar')

<div class="pcoded-main-container">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">

				<!-- Main-body start -->
				<div class="main-body">
					<div class="page-wrapper">
						<!-- Page header start -->
						<div class="page-header">
							<div class="page-header-title">
								<h4>Edit Product</h4>
							</div>
							<div class="page-header-breadcrumb">
								<ul class="breadcrumb-title">
									<li class="breadcrumb-item">
										<a href="{{ route('admin.dashboard') }}">
											<i class="icofont icofont-home"></i>
										</a>
									</li>
									<li class="breadcrumb-item"><a href="javascript:;">Products</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- Page header end -->
						<!-- Page body start -->
						<div class="page-body">
							<div class="row">
								<div class="col-sm-12">
									<!-- Basic Form Inputs card start -->
									@include('admin.messages')
									<div class="card">
										<div class="card-header">
											<h5>Edit Product</h5>
											<div class="card-header-right">
												<i class="icofont icofont-rounded-down"></i>
												<i class="icofont icofont-refresh"></i>
												<i class="icofont icofont-close-circled"></i>
											</div>
										</div>
										<div id="map"></div>
										<div class="card-block">
											<form role="form" method="POST" action="{{route('product.update')}}" id="registration-form" enctype="multipart/form-data">
												{{csrf_field()}}
												<input type="hidden" name="product_id" id="product_id" value="{{ $products->id }}">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Product Name</label>
													<div class="col-sm-10">
														<input name="product_name" id="product_name" type="text" class="form-control" value="{{ $products->product_name }}">
														@if ($errors->has('product_name'))
														<span style="color:red;">{{$errors->first('product_name')}}</span>
														@endif
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Product Sub Heading </label>
													<div class="col-sm-10">
														<input name="product_sub_heading" id="product_sub_heading" type="text" class="form-control" value="{{ $products->product_sub_heading }}">
														@if ($errors->has('product_sub_heading'))
														<span style="color:red;">{{$errors->first('product_sub_heading')}}</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Description</label>
													<div class="col-sm-10">
														<textarea name="description" id="description" class="form-control">{{ $products->description }}</textarea>
														@if ($errors->has('description'))
														<span style="color:red;">{{$errors->first('description')}}</span>
														@endif
													</div>
												</div>

												{{-- @if(!empty($specifications)) --}}
												<div class="input_fields_wrap">
													@if(!empty($specifications))
													@php $i = 0; @endphp
													@foreach($specifications as $keyspec => $product_values)
													<div class="form-group row speci{{$i}}">
														<label class="col-sm-2 col-form-label">@if($i==0) {{ 'Specification' }}@endif</label>
														<div class="col-sm-4">
															<input name="product_specification_heading[{{$i}}]" id="product_specification_heading" type="text" placeholder="Specification (Heading)" class="form-control" value="{{ $keyspec }}">
														</div>
														<div class="col-sm-4">
															<input name="product_specification_value[{{$i}}]" id="product_specification_value" placeholder="Specification (Value)" type="text" class="form-control" value="{{ $product_values }}">
														</div>
														@if($i==0)
														<div class="col-sm-2">
															<button type="button" class="add_field_button btn btn-primary m-b-0">Add More Fields</button>
														</div>
														@else
														<div class="col-sm-2">
															<button type="button" data-id="{{$i}}" class="remove_field btn btn-danger m-b-0">Remove</button>
														</div>
														@endif
													</div>
													@php $i++; @endphp
													@endforeach
													<input type="hidden" id="foreachcount" value="{{ count($specifications) }}">
													@else
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">Specification</label>
														<div class="col-sm-4">
															<input name="product_specification_heading[1]" id="product_specification_heading" type="text" placeholder="Specification (Heading)" class="form-control" value="{{ old('product_specification_heading[1]') }}">
														</div>
														<div class="col-sm-4">
															<input name="product_specification_value[1]" id="product_specification_value" placeholder="Specification (Value)" type="text" class="form-control" value="{{ old('product_specification_value[1]') }}">
														</div>
														<div class="col-sm-2">
															<button type="button" class="add_field_button btn btn-primary m-b-0">Add More Fields</button>
														</div>
													</div>
													@endif
												</div>
												<span style="color: red;margin-left: 170px;" id="fields_not_proper"></span>
												{{-- @endif --}}

                                                @if(!userRoleCheck([2]))
												    <div class="form-group row">
													<label class="col-sm-2 col-form-label">Select Shop</label>
													<div class="col-sm-10">
														<select name="shop" id="shopId" class="form-control">
															<option value="">Select Shop</option>
															@if(!empty($shops))
															@foreach($shops as $shop)
															<option @if($shop->id == $products->shop_id ) @php echo "selected"; @endphp @endif value="{{ $shop->id }}">{{ $shop->shop_name }}</option>
															@endforeach
															@endif
														</select>
														@if ($errors->has('shop'))
														<span style="color:red;">{{ $errors->first('shop') }}</span>
														@endif
													</div>
												</div>
                                                @endif

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Select Main Category</label>
													<div class="col-sm-10">
														<select name="main_category_id" id="mainCategoryId" class="form-control">
															<option value="">Select Main Category</option>
															@if(!empty($maincategorys))
															@foreach($maincategorys as $maincategory)
															<option @if($maincategory->id == $products->main_category_id) @php echo "selected"; @endphp @endif value="{{ $maincategory->id }}">{{ $maincategory->main_category_name }}</option>
															@endforeach
															@endif
														</select>
														@if ($errors->has('main_category_id'))
														<span style="color:red;">{{ $errors->first('main_category_id') }}</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Select Sub Category</label>
													<div class="col-sm-10">
														<select name="sub_category_id" id="sub_category_id" class="form-control">

														</select>
														@if ($errors->has('sub_category_name'))
														<span style="color:red;">{{ $errors->first('sub_category_name') }}</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Select Brand</label>
													<div class="col-sm-10">
														<select name="brand_id" id="brandId" class="form-control">
															<option value="">Select Brand</option>
															@if(!empty($brands))
															@foreach($brands as $brand)
															<option  @if($brand->id == $products->brand_id ) @php echo "selected"; @endphp @endif value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
															@endforeach
															@endif
														</select>
														@if ($errors->has('brand_id'))
														<span style="color:red;">{{ $errors->first('brand_id') }}</span>
														@endif
													</div>
												</div>



												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Price</label>
													<div class="col-sm-10">
														<input name="price" type="number" min="0" id="Price" class="form-control" value="{{ $products->original_price }}">
														@if ($errors->has('price'))
														<span style="color:red;">{{$errors->first('price')}}</span>
														@endif
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Color</label>
													<div class="col-sm-10">
														<input name="color" type="text" class="form-control" value="{{ $products->color }}">
														@if ($errors->has('color'))
														<span style="color:red;">{{$errors->first('color')}}</span>
														@endif
													</div>
													<label class="col-sm-2 col-form-label"></label>
													<span style="color:black;margin-left: 15px;">Note: Enter the color name like red,black,blue.</span>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label"></label>
													<div class="col-sm-1">
														<input name="new_arrivals" type="checkbox" value="1" @if($products->new_arrivals == 1) @php echo "checked"; @endphp @endif >
													</div>
													<div class="col-sm-2">New Arrivals</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label"></label>
													<div class="col-sm-1">
														<input name="seasonal" type="checkbox" @if($products->seasonal == 1) @php echo "checked"; @endphp @endif value="1">
													</div>
													<div class="col-sm-2">Seasonal</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Product GST</label>
													<div class="col-sm-10">
														<input type="number" class="form-control product_gst_check" name="product_gst" value="{{ $products->product_gst }}">
													</div>
													<label class="col-sm-2 col-form-label" ></label><span style="color: red;" id="pricegst"></span>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Stock</label>
													<div class="col-sm-10">
														<input name="stock" type="number" id="stock" min="0" class="form-control" value="{{ $products->stock }}">
														@if ($errors->has('stock'))
														<span style="color:red;">{{$errors->first('stock')}}</span>
														@endif
														<span class="notaddedstock"></span>
													</div>
												</div>


												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Sizes</label>
													<div class="col-sm-4">
														For Shirts/Pants
														@if($products->normalsize == 1)
														@php
														$status = 'checked';
														@endphp
														@else
														@php
														$status = '';
														@endphp
														@endif
														<label class="switch"> <input type="checkbox" class="sizesforall" name="sizesforall" id="sizesforall" value="1" {{ $status }}> <span class="slider round"></span></label>
													</div>

													<div class="col-sm-4">
														For Shooes
														@if($products->shooessize == 1)
														@php
														$status1 = 'checked';
														@endphp
														@else
														@php
														$status1 = '';
														@endphp
														@endif
														<label class="switch"> <input type="checkbox" class="sizesforshooes" name="sizesforshooes" id="sizesforshooes" value="1" {{ $status1 }}> <span class="slider round"></span></label>
													</div>
												</div>

												<div class="sizesOptions" @if($products->normalsize != 1) style="display: none;" @endif>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														{{-- <div class="col-sm-3"></div> --}}
														<div class="col-sm-3"><label>Sizes</label></div>
														<div class="col-sm-3"><label>Quantity</label></div>
													</div>

													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="ssize" type="hidden" class="form-control" value="S">
														<div class="col-sm-3"><label>Small (s)</label></div>
														<div class="col-sm-3"><input name="sstock" id="sstock" type="number" class="form-control" value="@if(!empty($productsMetas[0])){{ $productsMetas[0]->sizes_stock }}@else{{ 0 }}@endif" min="0"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="msize" type="hidden" class="form-control" value="M">
														<div class="col-sm-3"><label>Medium (M)</label></div>
														<div class="col-sm-3"><input name="mstock" id="mstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[1])){{ $productsMetas[1]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="lsize" type="hidden" class="form-control" value="L">
														<div class="col-sm-3"><label>Large (L)</label></div>
														<div class="col-sm-3"><input name="lstock" id="lstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[2])){{ $productsMetas[2]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="xlsize" type="hidden" class="form-control" value="XL">
														<div class="col-sm-3"><label>Extra Large (XL)</label></div>
														<div class="col-sm-3"><input name="xlstock" id="xlstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[3])){{ $productsMetas[3]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="xxlsize" type="hidden" class="form-control" value="XXL">
														<div class="col-sm-3"><label>Double Extra Large (XXL)</label></div>
														<div class="col-sm-3"><input name="xxlstock" id="xxlstock"  min="0" type="number" class="form-control" value="@if(!empty($productsMetas[4])){{ $productsMetas[4]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<label class="col-sm-2 col-form-label"></label><span id="sizesoptionserror"></span>
												</div>

												<div class="sizesOptionsForShooes" @if($products->shooessize != 1) style="display: none;" @endif>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														{{-- <div class="col-sm-3"></div> --}}
														<div class="col-sm-3"><label>Sizes</label></div>
														<div class="col-sm-3"><label>Quantity</label></div>
													</div>

													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="six" type="hidden" class="form-control" value="6">
														<div class="col-sm-3"><label>6 (Six)</label></div>
														<div class="col-sm-3"><input name="sixstock" id="sixstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[0])){{ $productsMetas[0]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="seven" type="hidden" class="form-control" value="7">
														<div class="col-sm-3"><label>7 (Seven)</label></div>
														<div class="col-sm-3"><input name="sevenstock" id="sevenstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[1])){{ $productsMetas[1]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="eight" type="hidden" class="form-control" value="8">
														<div class="col-sm-3"><label>8 (Eight)</label></div>
														<div class="col-sm-3"><input name="eightstock" id="eightstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[2])){{ $productsMetas[2]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="nine" type="hidden" class="form-control" value="9">
														<div class="col-sm-3"><label>9 (Nine)</label></div>
														<div class="col-sm-3"><input name="ninestock" id="ninestock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[3])){{ $productsMetas[3]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="ten" type="hidden" class="form-control" value="10">
														<div class="col-sm-3"><label>10 (Ten)</label></div>
														<div class="col-sm-3"><input name="tenstock" id="tenstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[4])){{ $productsMetas[4]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="eleven" type="hidden" class="form-control" value="11">
														<div class="col-sm-3"><label>11 (Eleven)</label></div>
														<div class="col-sm-3"><input name="elevenstock" id="elevenstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[5])){{ $productsMetas[5]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="twelve" type="hidden" class="form-control" value="12">
														<div class="col-sm-3"><label>12 (Twelve)</label></div>
														<div class="col-sm-3"><input name="twelvestock" id="twelvestock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[6])){{ $productsMetas[6]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<div class="form-group row">
														<label class="col-sm-2 col-form-label"></label>
														<input name="thirteen" type="hidden" class="form-control" value="13">
														<div class="col-sm-3"><label>13 (Thirteen)</label></div>
														<div class="col-sm-3"><input name="thirteenstock" id="thirteenstock" min="0" type="number" class="form-control" value="@if(!empty($productsMetas[7])){{ $productsMetas[7]->sizes_stock }}@else{{ 0 }}@endif"></div>
													</div>
													<label class="col-sm-2 col-form-label"></label><span id="shooesoptionserror"></span>
												</div>

												

												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Product Images</label>
													<div class="col-sm-10">
														<input name="product_image[]" type="file" multiple="">
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2 col-form-label"></label>
													@if(!empty($productImages))
													@foreach($productImages as $productimage)
													<div class="col-sm-2 img-wrap lastimage">
														<span class="close">&times;</span>
														<img height="150" width="150" src="{{ asset('public/upload/product/thumbnail/'.$productimage) }}" data-id="{{ $productimage }}">
													</div>
													@endforeach
													@endif
												</div>

												<div class="row">
													<label class="col-sm-2"></label>
													<div class="col-sm-10">
														<button type="submit" class="btn btn-primary m-b-0" id="register-btn">Save</button>
														<a class="btn btn-primary m-b-0" href="{{ route('product.index') }}">Back</a>
													</div>
												</div>
											</form>
										</div>
									</div>

								</div>
							</div>
						</div>
						<!-- Page body end -->
					</div>
				</div>
				<!-- Main-body end -->
				<div id="styleSelector">

				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>
	$(document).ready(function() {
		CKEDITOR.replace( 'description' );
	});



	$('select[name="main_category_id"]').on('change',function(){
		var mainId = $(this).val();
		main_category_changes(mainId);
	});

	var mainCatId = '{{ $products->main_category_id }}';
	var subCatId = '{{ $products->sub_category_id }}';
	main_category_changes(mainCatId, subCatId);

	function main_category_changes(mainId, subCatId = '') {
		if(mainId)
		{
			jQuery.ajax({
				url :   "{{ route('maincategory.sub', '') }}/"+mainId,
				type : "GET",
				dataType : "json",
				success:function(data)
				{
					$('select[name="sub_category_id"]').empty();
					$('select[name="sub_category_id"]').append('<option value="">Select sub category</option>');
					$.each(data, function(key,value){
						var select = subCatId != '' && key == subCatId ? 'selected' : '';
						$('select[name="sub_category_id"]').append('<option value="'+ key +'" '+select+'>'+ value +'</option>');
					});
				}
			});
		}
		else
		{
			$('select[name="state"]').empty();
		}
	}

	$('.sizesforall').change(function () {
		var stock = $('#stock').val();
		if(stock != '') {
			if ($('#sizesforall').prop('checked')) {
				$('.notaddedstock').html('');
				$('.sizesforshooes').prop('checked',false);
				$('.sizesOptionsForShooes').css('display','none');
				$('.sizesOptions').css('display','block');
			} else {
				$('.sizesOptions').css('display','none');
			}
		} else {
			$('.notaddedstock').html('<span style="color:red;">Please add stock First</span>');
			$('#stock-error').html('');
			$('.sizesforall').prop('checked',false);
		}
	});

	$('.sizesforshooes').change(function () {
		var stock = $('#stock').val();
		if(stock != '') {
			if ($('#sizesforshooes').prop('checked')) {
				$('.notaddedstock').html('');
				$('.sizesforall').prop('checked',false);
				$('.sizesOptions').css('display','none');
				$('.sizesOptionsForShooes').css('display','block');
			}	else {
				$('.sizesOptionsForShooes').css('display','none');
			}
		} else {
			$('.notaddedstock').html('<span style="color:red;">Please add stock First</span>');
			$('#stock-error').html('');
			$('.sizesforshooes').prop('checked',false);
		}
	});


	$('.img-wrap .close').on('click', function() {
		var product_id = $('#product_id').val();
		var imagename = $(this).closest('.img-wrap').find('img').data('id');
		$(this).closest('.img-wrap').addClass('remove');
		$(this).closest('.img-wrap').removeClass('lastimage');
		jQuery.ajax({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			url :   "{{ route('product.deleteimage') }}",
			type : "POST",
			dataType : "json",
			data : { 'imagename' : imagename, 'product_id' : product_id},
			success:function(data)
			{
				if (data.status == 1) {
					$('.remove').css('display','none');
				}
			}
		});
	});


	$('#register-btn').click(function() {

		if($('#sizesforall').prop('checked'))
		{
			var sstock = $('#sstock').val();
			var mstock = $('#mstock').val();
			var lstock = $('#lstock').val();
			var xlstock = $('#xlstock').val();
			var xxlstock = $('#xxlstock').val();

			var total = parseInt(sstock) + parseInt(mstock) + parseInt(lstock) + parseInt(xlstock) + parseInt(xxlstock);
			var mainstock = $('#stock').val();

			if(mainstock == total) {
				$('#sizesoptionserror').html('');
			} else{
				$('#sizesoptionserror').html('<span style="color:red;">Sizes and Stock Total Must be Same</span>');
				return false;
			}
		}
		if($('#sizesforshooes').prop('checked')) {

			var sixstock = $('#sixstock').val();
			var sevenstock = $('#sevenstock').val();
			var eightstock = $('#eightstock').val();
			var ninestock = $('#ninestock').val();
			var tenstock = $('#tenstock').val();
			var elevenstock = $('#elevenstock').val();
			var twelvestock = $('#twelvestock').val();
			var thirteenstock = $('#thirteenstock').val();

			var total = parseInt(sixstock) + parseInt(sevenstock) + parseInt(eightstock) + parseInt(ninestock) + parseInt(tenstock) + parseInt(elevenstock)+ parseInt(twelvestock)+ parseInt(thirteenstock);
			var mainstock = $('#stock').val();

			if(mainstock == total) {
				$('#shooesoptionserror').html('');
			} else {
				$('#shooesoptionserror').html('<span style="color:red;">Sizes and Stock Total Must be Same</span>');
				return false;
			}

		}

		$('#registration-form').validate({
			rules: {
				product_name: {
					required: true,
				},
				product_sub_heading: {
					required: true,
				},
				brand_id: {
					required: true,
				},
				shop: {
					required: true,
				},
				main_category_id: {
					required: true,
				},
				sub_category_id: {
					required: true,
				},
				price: {
					required: true,
				},
				stock: {
					required: true,
				},
				product_gst: {
					required:true,
                    range: [1, 99],
                },
				"product_image[]": {
					required: function(){
						return $(".lastimage").length == "0";
					},
					// required: true,
					extension: "jpg|jpeg|png",
				},
			},
			errorPlacement: function(error, element) {
				if (element.is(":radio")) {
					error.insertAfter("#radio-error");
				}
				else if(element.is(":checkbox"))
				{
					error.insertAfter("#checkbox-error");
				}
				else {
					error.appendTo(element.parent());
				}
			},
			messages:{
				product_name: {
					required: 'Please Enter Product Name',
				},
				product_sub_heading: {
					required: 'Please Enter Product Sub Heading',
				},
				brand_id: {
					required: 'Please Select Brand',
				},
				shop: {
					required: 'Please Select Shop',
				},
				main_category_id: {
					required: 'Please Select Main Category',
				},
				sub_category_id: {
					required: 'Please Select Sub Category',
				},
				price: {
					required: 'Please Select Price',
				},
				stock:{
					required: 'Please Enter Stock',
				},
				"product_image[]": {
						// required: 'Product Image is required',
						extension: "only jpg,jpeg,png file extension required",
					},
				},
			});

		// $('.product_gst').change(function(){

	});

		$('body').on('change', '.product_gst_check', function() {
			
			var shop_choose = $('#shopId').val();
			var prize = $('#Price').val();
			if(shop_choose == ''){
				$('#pricegst').html('Please choose first Shop');
				$('.product_gst_check').val('');
				return false;
			} else if(prize == ''){
				$('#pricegst').html('Please enter price first');
				$('.product_gst_check').val('');
				return false;
			} 
			else {
				$('#pricegst').html(' ');
				return true;
			}
		});


</script>

@endpush
