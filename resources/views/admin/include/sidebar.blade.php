<nav class="pcoded-navbar" pcoded-header-position="relative">
	<div class="sidebar_toggle"><a href="{{ route('admin.dashboard') }}"><i class="icon-close icons"></i></a></div>
	<div class="pcoded-inner-navbar main-menu">
		<div class="">
			<div class="main-menu-header">
				@if(!empty(auth()->user()->admin_image))
				@php
				$image = auth()->user()->admin_image;
				@endphp
				@else
				@php
				$image = 'user.png';
				@endphp
				@endif

				<img class="img-40" src="{{asset('assets/frontend/profiles/'.$image.'')}}" alt="User-Profile-Image">
				<div class="user-details">
					<span>{{  auth()->user()->name }}</span>
					<span id="more-details">{{ getLoginUserRoleName() }}<i class="ti-angle-down"></i></span>
				</div>
			</div>
			<div class="main-menu-content">
				<ul>
					<li class="more-details">
						<a href="{{ route('admin.adminProfile',auth()->user()->id) }}"><i class="ti-user"></i>View Profile</a>
						<a href="{{ route('admin.adminProfile',auth()->user()->id) }}"><i class="ti-settings"></i>Change Password</a>
						<a href="{{ route('logout') }}"
						onclick="event.preventDefault();
						document.getElementById('logout-form').submit();"><i class="ti-layout-sidebar-left"></i>Logout</a>
						<form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
					</li>
				</ul>
			</div>
		</div>

		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'admin.dashboard') active pcoded-trigger @endif">
				<a href="{{ route('admin.dashboard') }}">
					<span class="pcoded-micon"><i class="ti-home"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
					<span class="pcoded-badge label label-danger">Default</span>
					<span class="pcoded-mcaret"></span>
				</a>
			</li>
		</ul>

		<!-- User Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'users.index' || \Request::route()->getName() == 'admin.users') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-user"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">User Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'users.index') active @endif">
						<a href="{{ route('users.index') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">All Users</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="@if (\Request::route()->getName() == 'admin.users') active @endif">
						<a href="{{ route('admin.users') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">All Admin Users</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- User Management Menu End -->

		<!-- Shop Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'shop.list') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-package"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Shop Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'shop.list') active @endif">
						<a href="{{ route('shop.list') }}" data-i18n="nav.editable-table.main">
							<span class="pcoded-micon"><i class="ti-package"></i></span>
							<span class="pcoded-mtext">Shop List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif

        <ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'orders.list') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Orders</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'orders.list') active @endif">
						<a href="{{ route('orders.list') }}" data-i18n="nav.editable-table.main">
							<span class="pcoded-micon"><i class="ti-bar-chart-alt"></i></span>
							<span class="pcoded-mtext">Orders List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>

		<!-- Shop Management Menu End -->

		<!-- Category Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'maincategory.index' || \Request::route()->getName() == 'subcategory.index') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-view-grid"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Category Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'maincategory.index') active @endif">
						<a href="{{ route('maincategory.index') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Main Category</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="@if (\Request::route()->getName() == 'subcategory.index') active @endif">
						<a href="{{ route('subcategory.index') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Sub Category</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Category Management Menu End -->

		<!-- Brand Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'brand.list') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-gift"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Brand</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'brand.list') active @endif">
						<a href="{{ route('brand.list') }}" data-i18n="nav.editable-table.main">
							<span class="pcoded-micon"><i class="ti-view-list"></i></span>
							<span class="pcoded-mtext">Brand List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Brand Management Menu End -->

		<!-- Product Management Menu Start -->
        @if(userRoleCheck([1, 2]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'product.index' ) active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-shopping-cart-full"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Product Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'product.index') active @endif">
						<a href="{{ route('product.index') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Product List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Product Management Menu End -->

		<!-- Slider Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'slider.list') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-view-list"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Slider Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'slider.list') active @endif">
						<a href="{{ route('slider.list') }}" data-i18n="nav.editable-table.main">
							<span class="pcoded-micon"><i class="ti-view-list"></i></span>
							<span class="pcoded-mtext">Slider List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Slider Management Menu End -->

		<!-- Coupon Code Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'coupon.code.list') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-layout-cta-right"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Coupon Code</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'coupon.code.list') active @endif">
						<a href="{{ route('coupon.code.list') }}" data-i18n="nav.editable-table.main">
							<span class="pcoded-micon"><i class="ti-view-list"></i></span>
							<span class="pcoded-mtext">Coupon Code List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Coupon Code Management Menu End -->

		<!-- Menu Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'admin.menuManager.index') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Menu Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'admin.menuManager.index') active @endif">
						<a href="{{ route('admin.menuManager.index') }}">
							<span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
							<span class="pcoded-mtext" data-i18n="nav.dash.main">Menu List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Menu Management Menu End -->

		<!-- Email Management Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'email.index' ) active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-email"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Email Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'email.index') active @endif">
						<a href="{{ route('email.index') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Subscriber Email</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Email Management Menu End -->

		<!-- Pin Code Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'pincode.list' ) active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-pin"></i></span>
					<span class="pcoded-mtext" data-i18n="nav.dash.main">Pincode Management</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'pincode.list') active @endif">
						<a href="{{ route('pincode.list') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Pincode List</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Pin Code Menu End -->

		<!-- Configurations Menu Start -->
        @if(userRoleCheck([1]))
		<ul class="pcoded-item pcoded-left-item">

			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'admin.logoIcon.index') active pcoded-trigger @endif
				@if (\Request::route()->getName() == 'admin.GenSetting') active pcoded-trigger @endif
				@if (\Request::route()->getName() == 'admin.social.index') active pcoded-trigger @endif
				@if (\Request::route()->getName() == 'admin.TimerSetting') active pcoded-trigger @endif
				@if (\Request::route()->getName() == 'admin.banner.setting') active pcoded-trigger @endif
				@if (\Request::route()->getName() == 'admin.google.index') active pcoded-trigger @endif">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="ti-settings"></i></span>
					<span class="pcoded-mtext">Configurations</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'admin.logoIcon.index') active @endif">
						<a href="{{route('admin.logoIcon.index')}}" data-i18n="nav.basic-components.alert">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Logo Icons Settings</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="@if (\Request::route()->getName() == 'admin.GenSetting') active @endif">
						<a href="{{route('admin.GenSetting')}}" data-i18n="nav.basic-components.breadcrumbs">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Site Settings</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="@if (\Request::route()->getName() == 'admin.social.index') active @endif">
						<a href="{{route('admin.social.index')}}" data-i18n="nav.basic-components.breadcrumbs">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Social Settings</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
					<li class="@if (\Request::route()->getName() == 'admin.google.index') active @endif">
						<a href="{{route('admin.google.index')}}" data-i18n="nav.basic-components.breadcrumbs">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Google Analytics</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
                    <li class="@if (\Request::route()->getName() == 'admin.TimerSetting') active @endif">
						<a href="{{route('admin.TimerSetting')}}" data-i18n="nav.basic-components.breadcrumbs">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Timer</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
                    <li class="@if (\Request::route()->getName() == 'admin.banner.setting') active @endif">
						<a href="{{route('admin.banner.setting')}}" data-i18n="nav.basic-components.breadcrumbs">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Banner</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>

			<li class="pcoded-hasmenu @if (\Request::route()->getName() == 'admin.general.edit') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-crown"></i></span>
					<span class="pcoded-mtext">SEO</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == 'admin.general.edit') active @endif">
						<a href="{{ route('admin.general.edit','1') }}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">General SEO</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>

			<li class="pcoded-hasmenu @if (\Request::route()->getName() == ' ') active pcoded-trigger @endif">
				<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
					<span class="pcoded-micon"><i class="ti-notepad"></i></span>
					<span class="pcoded-mtext">Log Viewer</span>
					<span class="pcoded-mcaret"></span>
				</a>
				<ul class="pcoded-submenu">
					<li class="@if (\Request::route()->getName() == ' ') active @endif">
						<a target="_blank" href="{{url('log-viewer')}}" data-i18n="nav.advance-components.draggable">
							<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
							<span class="pcoded-mtext">Log Viewer</span>
							<span class="pcoded-mcaret"></span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
        @endif
		<!-- Configurations Menu End -->

		{{--		@if(auth()->user()->can('Add Role') || auth()->user()->can('Delete Role') || auth()->user()->can('Edit Role')|| auth()->user()->can('Edit Role') || auth()->user()->can('Permission List')|| auth()->user()->can('Edit Permission')|| auth()->user()->can('Delete Permission')|| auth()->user()->can('Add Admin User')|| auth()->user()->can('Edit Admin User')|| auth()->user()->can('Delete Admin User'))--}}
		{{-- <div class="pcoded-navigatio-lavel" data-i18n="nav.category.ui-element" menu-title-theme="theme5">Role Settings</div> --}}
		{{--<ul class="pcoded-item pcoded-left-item">
			<li class="pcoded-hasmenu
			@if (\Request::route()->getName() == 'role.index') active pcoded-trigger @endif
			@if (\Request::route()->getName() == 'permission.index') active pcoded-trigger @endif
			@if (\Request::route()->getName() == 'admin.users') active pcoded-trigger @endif
			@if (\Request::route()->getName() == 'add.adminuser') active pcoded-trigger @endif
			">
			<a href="javascript:void(0)" data-i18n="nav.advance-components.main">
				<span class="pcoded-micon"><i class="ti-pencil-alt"></i></span>
				<span class="pcoded-mtext">Role Management</span>
				<span class="pcoded-mcaret"></span>
			</a>
			<ul class="pcoded-submenu">
				<li class="@if (\Request::route()->getName() == 'role.index') active @endif">
					<a href="{{ route('role.index') }}" data-i18n="nav.advance-components.draggable">
						<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
						<span class="pcoded-mtext">Roles</span>
						<span class="pcoded-mcaret"></span>
					</a>
				</li>
				<li class="@if (\Request::route()->getName() == 'permission.index') active @endif">
					<a href="{{route('permission.index')}}" data-i18n="nav.advance-components.draggable">
						<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
						<span class="pcoded-mtext">Permission</span>
						<span class="pcoded-mcaret"></span>
					</a>
				</li>
				<li class="@if (\Request::route()->getName() == 'admin.users') active @endif
					@if (\Request::route()->getName() == 'add.adminuser') active @endif
					">
					<a href="{{route('admin.users')}}" data-i18n="nav.advance-components.draggable">
						<span class="pcoded-micon"><i class="ti-angle-right"></i></span>
						<span class="pcoded-mtext">Admin Users</span>
						<span class="pcoded-mcaret"></span>
					</a>
				</li>
			</ul>
		</li>
	</ul>--}}
	{{--	@endif--}}

</div>
</nav>
