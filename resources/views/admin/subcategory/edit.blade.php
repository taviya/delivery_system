<div class="form-group row">
    <label class="col-sm-2 col-form-label">Select Main Category</label>
        <div class="col-sm-10">
            <select name="main_category_id" class="form-control">
                <option value="">Select Main Category</option>
                    @if(!empty($maincategorys))
                   	@foreach($maincategorys as $maincategory)
                    <option value="{{ $maincategory->id }}" @if($maincategory->id == $subcategory->main_category_id) @php echo "selected"; @endphp @endif>{{ $maincategory->main_category_name }}</option>
                    @endforeach
                    @endif
             </select>
        @if ($errors->has('main_category_id'))
        <span style="color:red;">{{ $errors->first('main_category_id') }}</span>
        @endif
    </div>
</div>
<div class="form-group row">
    	<label class="col-sm-2 col-form-label">Sub Category Name</label>
    	<input name="update_id" id="updateId" type="hidden" class="form-control" value="{{ $subcategory->id }}">
        <div class="col-sm-10">
            <input name="sub_category_name" id="editMainCategoryName" type="text" class="form-control" value="{{ $subcategory->sub_category_name }}">
            @if ($errors->has('sub_category_name'))
            <span style="color:red;">{{ $errors->first('sub_category_name') }}</span>
             @endif
        </div>
</div>
<div class="form-group row">
    <label class="col-sm-2 col-form-label">Category image</label>
    <div class="col-sm-10">
        <input type="file" name="subcategory_icon" id="editFileUploader" class="form-control">
    </div>
    <div class="col-sm-10">
        <img style="height: 100px; width: 100px;" id="edit_display_image" class="img-thumbnail" src="{{ $mode == 'Edit' ? asset("public/upload/category/thumbnail/".$subcategory->subcategory_icon) : old('subcategory_icon') }}">
    </div>
</div>
<div class="form-group row">
		<label class="col-sm-2 col-form-label">Status</label>
		<div class="col-sm-10">
			<div class="radio radio-inline">
				<input type="radio" name="status" value="1" @if($subcategory->status == 1) checked @endif>
				Active
			</div>
			<div class="radio radio-inline">
				<input type="radio" name="status" value="0" @if($subcategory->status == 0) checked @endif>
				Deactive
			</div>
			@if ($errors->has('status'))
				<span style="color:red;">{{$errors->first('status')}}</span>
			@endif
		</div>
</div>