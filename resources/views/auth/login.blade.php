@extends('user.layouts.app')
@section('title', 'Login')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('user.home') }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Login</li>
            </ol>
        </div>
    </nav>

    <!--<div class="page-header">
        <div class="container">
            <h1>Login and Create Account</h1>
        </div>
    </div>-->

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="heading">
                    <h2 class="title">Login</h2>
                    <p>If you have an account with us, please log in.</p>
                </div>

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="newsletter-signup" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label" for="newsletter-signup">Remember Me</label>
                    </div>

                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary">LOGIN</button>
                        <a href="{{ route('password.request') }}" class="forget-pass"> Forgot your password?</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop
