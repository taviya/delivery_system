<?php

namespace App\Models\SliderModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    use SoftDeletes;
    protected $table = 'slider';
    protected $fillable = [
        'title', 'subtitle', 'description', 'slider_image', 'btn_name', 'btn_link', 'optional', 'status'
    ];
}
