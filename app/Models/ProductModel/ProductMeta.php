<?php

namespace App\Models\ProductModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductMeta extends Model
{
	// use SoftDeletes;

    protected $guarded = [];
	protected $dates = [
		'created_at',
		'updated_at',
	]; 
}
