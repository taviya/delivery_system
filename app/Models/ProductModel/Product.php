<?php

namespace App\Models\ProductModel;

use App\Models\MainCategoryModel\MainCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

    protected $guarded = [];
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

    public function getMainCategory() {
        return $this->hasOne('App\Models\MainCategoryModel\MainCategory', 'id', 'main_category_id');
    }

    public function getSizeWiseProduct() {
        return $this->hasMany('App\Models\ProductModel\ProductMeta', 'product_id', 'id');
    }
}
