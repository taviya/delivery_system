<?php

namespace App\Models\BrandModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;
    protected $table = 'brand';
    protected $fillable = [
        'brand_name', 'status'
    ];
}
