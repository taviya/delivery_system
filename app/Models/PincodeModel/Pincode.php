<?php

namespace App\Models\PincodeModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pincode extends Model
{
 
    use SoftDeletes;

    protected $table = 'pincodes';
    protected $guarded = [];
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	]; 
}
