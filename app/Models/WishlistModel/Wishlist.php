<?php

namespace App\Models\WishlistModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wishlist extends Model
{
	//use SoftDeletes;
	protected $table = 'wishlist';
    protected $guarded = [];
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	];
}
