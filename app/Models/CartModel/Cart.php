<?php

namespace App\Models\CartModel;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
	// use SoftDeletes;

    protected $guarded = [];
    protected $table = 'cart';
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	]; 
}
