<?php

namespace App\Models\CouponCodeModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponCode extends Model
{
    use SoftDeletes;
    protected $table = 'coupon_code';
    protected $fillable = [
        'name', 'percentage_discount', 'max_discount_amount', 'description', 'expired_date', 'status'
    ];
}
