<?php

namespace App\Models\EmailSubscriberModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailSubscriber extends Model
{
	use SoftDeletes;

    protected $guarded = [];
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	]; 
}
