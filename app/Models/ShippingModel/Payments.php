<?php

namespace App\Models\ShippingModel;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Payments extends Model
{
	// use SoftDeletes;

    protected $guarded = [];
    protected $table = 'payments';
	protected $dates = [
		'created_at',
		'updated_at',
	]; 
}
