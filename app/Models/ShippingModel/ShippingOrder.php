<?php

namespace App\Models\ShippingModel;

use Illuminate\Database\Eloquent\Model;

class ShippingOrder extends Model
{
    protected $guarded = [];
    protected $table = 'shipping_order';
	protected $dates = [
		'created_at',
		'updated_at',
		// 'deleted_at',
	];

	public function getShippingOrderAddress() {
        return $this->hasOne('App\Models\ShippingModel\ShippingAddress', 'id', 'address_id');
    }
}
