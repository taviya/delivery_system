<?php

namespace App\Models\ShippingModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingAddress extends Model
{
	use SoftDeletes;

    protected $guarded = [];
    protected $table = 'shipping_address';
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	]; 
}
