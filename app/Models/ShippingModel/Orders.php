<?php

namespace App\Models\ShippingModel;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
	// use SoftDeletes;

    protected $guarded = [];
    protected $table = 'orders';
	protected $dates = [
		'created_at',
		'updated_at',
	];

	public function getUserForOrder() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getProductForOrder() {
        return $this->hasOne('App\Models\ProductModel\Product', 'id', 'product_id');
    }

    public function getShippingOrder() {
        return $this->hasOne('App\Models\ShippingModel\ShippingOrder', 'user_id', 'user_id');
    }

    public function getProductShopForOrder() {
        return $this->hasOne('App\Models\Shop\Shop', 'id', 'product_shop_id');
    }
}
