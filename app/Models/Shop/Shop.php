<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;
    protected $table = 'shop';
    protected $fillable = [
        'shop_name', 'shop_user_status', 'address', 'shop_commission', 'turnover', 'shop_image', 'status'
    ];

    public function getShopUser() {
        return $this->hasOne('App\Admin', 'shop_id', 'id');
    }
}
