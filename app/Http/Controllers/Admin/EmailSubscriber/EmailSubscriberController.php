<?php

namespace App\Http\Controllers\Admin\EmailSubscriber;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use Validator;
use DB;
use DataTables;
use App\Models\EmailSubscriberModel\EmailSubscriber;
use Illuminate\Support\Facades\Hash;

class EmailSubscriberController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $email = EmailSubscriber::orderBy('id', 'DESC')->get();
            return Datatables::of($email)
                ->addIndexColumn()
                ->addColumn('email', function ($user) {
                    $data = '<a href="mailto:'.$user->email.'">'.$user->email.'</a>';
                    return $data;
                })
                ->addColumn('action', function ($user) {
                    $data ='<button type="button" class="btn btn-outline-danger" onclick="deleteMainCategoryModel('.$user->id.')" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Delete Category"><i class="icofont icofont-trash"></i>
                    </button> ';
                    return $data;
                })
                ->rawColumns(['action','email'])
                ->make(true);
        } else {
            return view('admin.emailsubscriber.index');
        }
    }

    public function destroy($id)
    {
        $subcategory = EmailSubscriber::find($id);
        if ($subcategory->delete()) {
            return response()->json(['status' => 1, 'messages' => 'Email delete successfully']);
        }
    }
}
