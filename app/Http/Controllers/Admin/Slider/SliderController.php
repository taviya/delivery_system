<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Models\SliderModel\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Image;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $slider = Slider::orderBy('status', 'DESC')->orderBy('id', 'DESC')->get();
            return Datatables::of($slider)
                    ->addIndexColumn()
                    ->addColumn('status', function($row) {
                        $id = $row->id;
                        $status = $row->status;
                        return view('admin.datatable.status', compact('id', 'status'));
                    })
                    ->editColumn('slider_image', function ($row) {
                        $url = asset('public/upload/slider/thumbnail/' . $row->slider_image);
                        return '<img src="' . $url . '" border="0" width="40" class="img-rounded" align="center" />';
                    })
                    ->addColumn('action', function($row) {
                        $id = $row->id;
                        $edit  = 'Edit Slider';
                        $delete  = 'Delete Slider';
                        return view('admin.datatable.action', compact('id', 'edit', 'delete'));
                    })
                    ->rawColumns(['status', 'action', 'slider_image'])
                    ->make(true);
        }else{
            return view('admin.slider.slider_list');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'description' => 'required',
            'btn_name' => 'required',
            'btn_link' => 'required',
            'slider_image' => 'bail|required|file|max:5000kb|mimes:jpeg,png,jpg',
        ]);

        $originalImage = $request->file('slider_image');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/slider/thumbnail/';
            $originalPath = public_path().'/upload/slider/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);
        }

        $addArray = [
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'description' => $request->description,
            'btn_name' => $request->btn_name,
            'btn_link' => $request->btn_link,
            'optional' => $request->optional ? $request->optional : NULL,
            'slider_image' => $imageName,
            'status' => 1,
        ];

        $addSlider = Slider::create($addArray);
        $slider = Slider::find($addSlider->id);
        if ($slider) {
            return response()->json(['status' => TRUE, 'message' => 'Slider create successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        $editData = view('admin.slider.edit', compact('slider'))->render();
        return response()->json(['status' => TRUE, 'data' => $editData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'description' => 'required',
            'btn_name' => 'required',
            'btn_link' => 'required',
            'slider_image' => 'bail|file|max:5000kb|mimes:jpeg,png,jpg',
        ]);

        $slider = Slider::find($request->update_id);
        $slider->title = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->description = $request->description;
        $slider->btn_name = $request->btn_name;
        $slider->btn_link = $request->btn_link;
        $slider->optional = $request->optional ? $request->optional : NULL;
        $slider->status = $request->status ? $request->status : 0;

        $originalImage = $request->file('slider_image');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/slider/thumbnail/';
            $originalPath = public_path().'/upload/slider/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);

            $slider->slider_image = $imageName;
        }

        if ($slider->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Slider update successfully.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $slider = Slider::find($request->del_id);
        if ($slider->delete()) {
            return response()->json(['status' => TRUE, 'message' => 'Slider delete successfully.']);
        }
    }

    public function statusChange($id)
    {
        $slider = Slider::find($id);
        $slider->status = $slider->status ? 0 : 1;
        if ($slider->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Slider status change successfully.']);
        }
    }
}
