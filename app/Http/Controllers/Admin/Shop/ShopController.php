<?php

namespace App\Http\Controllers\Admin\Shop;

use App\Admin;
use App\Models\Shop\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Image;
use Session;

class ShopController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $shop = Shop::with('getShopUser')->orderBy('status', 'DESC')->orderBy('id', 'DESC')->get();
            return Datatables::of($shop)
                    ->addIndexColumn()
                    ->addColumn('status', function($row) {
                        $id = $row->id;
                        $status = $row->status;
                        return view('admin.datatable.status', compact('id', 'status'));
                    })
                    ->editColumn('shop_image', function ($row) {
                        $url = asset('public/upload/shop/thumbnail/' . $row->shop_image);
                        return '<img src="' . $url . '" border="0" width="40" class="img-rounded" align="center" />';
                    })
                    ->addColumn('action', function($row) {
                        $id = $row->id;
                        $edit  = 'Edit Shop';
                        $delete  = 'Delete Shop';
                        $goToUser  = 'Go To User';
//                        return view('admin.datatable.action', compact('id', 'edit', 'delete'));
                        $shopUser = $row->getShopUser ? '<a href="'.route('admin.adminProfile', $row->getShopUser->id).'" class="btn btn-outline-danger" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="'.$goToUser.'"><i class="icofont icofont-share"></i></a>' : '';
                        return '<a href="javascript:void(0);" class="btn btn-outline-warning data_edit" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="'.$edit.'" data-id="'.$id.'"><i class="icofont icofont-edit"></i></a>
                                <button type="button" class="btn btn-outline-danger data_delete" data-id="'.$id.'" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="'.$delete.'"><i class="icofont icofont-trash"></i>
                                </button>'.$shopUser;
                    })
                    ->rawColumns(['status', 'action', 'shop_image'])
                    ->make(true);
        }else{
            $mode = 'Add';
            $shopUser = Admin::select('id', 'name')->where('role_id', 3)->get();
            return view('admin.shop.shop_list', compact( 'mode', 'shopUser'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'shop_name' => 'required',
            'turnover' => 'required',
            'address' => 'required',
            'shop_commission' => 'required|numeric|between:0,100',
            'shop_image' => 'bail|required|file|max:5000kb|mimes:jpeg,png,jpg|dimensions:min_width=225,min_height=225,max_width=225,max_height=225',
        ], [
            'shop_image.dimensions' => 'For shop image select height: 225px and width: 225px image.',
        ]);

        $originalImage = $request->file('shop_image');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/shop/thumbnail/';
            $originalPath = public_path().'/upload/shop/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);
        }

        $addArray = [
            'shop_name' => $request->shop_name,
            'address' => $request->address,
            'shop_commission' => $request->shop_commission,
            'turnover' => $request->turnover,
            'shop_image' => $imageName,
            'status' => 1,
        ];

        $addShop = Shop::create($addArray);
        $shop = Shop::find($addShop->id);
        if ($shop) {
            Session::flash('success', 'Shop create successfully. you can create shop user.');
            return response()->json(['status' => TRUE, 'message' => 'Shop create successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shop = Shop::find($id);
        $mode = 'Edit';
        $shopUser = Admin::select('id', 'name')->where('role_id', 3)->get();
        $editData = view('admin.shop.edit', compact('shop', 'mode', 'shopUser'))->render();
        return response()->json(['status' => TRUE, 'data' => $editData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'shop_name' => 'required',
//            'mobile_no' => 'required|unique:shop,mobile_no,'.$request->update_id,
            'turnover' => 'required',
            'address' => 'required',
            'shop_commission' => 'required|numeric|between:0,100',
//            'shop_image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'shop_image' => 'bail|file|max:5000kb|mimes:jpeg,png,jpg|dimensions:min_width=225,min_height=225,max_width=225,max_height=225',
        ], [
            'shop_image.dimensions' => 'For shop image select height: 225px and width: 225px image.',
        ]);
        $shop = Shop::find($request->update_id);

        $shop->shop_name = $request->shop_name;
        $shop->address = $request->address;
        $shop->shop_commission = $request->shop_commission;
        $shop->turnover = $request->turnover;
        $shop->status = $request->status ? $request->status : 0;

        $originalImage = $request->file('shop_image');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/shop/thumbnail/';
            $originalPath = public_path().'/upload/shop/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);

            $shop->shop_image = $imageName;
        }

        if ($shop->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Shop update successfully.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $shop = Shop::find($request->del_id);
        if ($shop->delete()) {
            return response()->json(['status' => TRUE, 'message' => 'Shop delete successfully.']);
        }
    }

    /**
     * Shop status change.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function statusChange($id)
    {
        $shop = Shop::find($id);
        $shop->status = $shop->status ? 0 : 1;
        if ($shop->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Shop status change successfully.']);
        }
    }
}
