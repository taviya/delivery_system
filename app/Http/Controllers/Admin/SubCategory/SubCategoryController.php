<?php

namespace App\Http\Controllers\Admin\SubCategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use Validator;
use DB;
use DataTables;
use App\Models\MainCategoryModel\MainCategory;
use App\Models\SubCategoryModel\SubCategory;
use Illuminate\Support\Facades\Hash;
use Image;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $subcategory = SubCategory::leftjoin('main_categories as mc', 'sub_categories.main_category_id', '=', 'mc.id')
            ->select([
                'sub_categories.*',
                'mc.id as mid',
                'mc.main_category_name',
                'mc.status as mstatus',
                'mc.deleted_at as mdeleted_at',
            ])
            ->where('mc.status',1)
            ->where('mc.deleted_at','=',NULL)
            ->orderBy('sub_categories.status', 'DESC')
            ->orderBy('sub_categories.id', 'DESC')->get();
            return Datatables::of($subcategory)
            ->addIndexColumn()
            ->addColumn('main_category_name', function ($user) {
                return $user->main_category_name;
            })
            ->addColumn('status', function ($user) {
                if ($user->status == '1') {
                    return "<label data-id='".$user->id."' class='label label-info status-update status_list'>Active</label>";
                }else{
                    return "<label data-id='".$user->id."' class='label label-danger status-update status_list'>Deactive</label>";
                }
            })
            ->editColumn('subcategory_icon', function ($row) {
                $url = asset('public/upload/category/thumbnail/' . $row->subcategory_icon);
                return '<img src="' . $url . '" border="0" width="40" class="img-rounded" align="center" />';
            })
            ->addColumn('action', function ($user) {
                $data ='<a href="javascript:;" onclick="editMainCategoryModel('.$user->id.')" class="btn btn-outline-warning" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Edit User"><i class="icofont icofont-edit"></i></a>
                <button type="button" class="btn btn-outline-danger" onclick="deleteMainCategoryModel('.$user->id.')" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Delete Category"><i class="icofont icofont-trash"></i>
                </button> ';
                return $data;
            })
            ->rawColumns(['action','status','subcategory_icon'])
            ->make(true);
        } else {
            $maincategorys = MainCategory::where('status', 1)->get();
            return view('admin.subcategory.index',compact('maincategorys'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'sub_category_name.required' => 'Please enter category name',
            'main_category_id.required' => 'Please select main category',
        ];

        $validatedData = $request->validate([
            'sub_category_name' => 'required',
            'main_category_id' => 'required',
        ], $messages);

        $originalImage = $request->file('subcategory_icon');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/category/thumbnail/';
            $originalPath = public_path().'/upload/category/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);
        }

        SubCategory::create([
            'main_category_id' => $request->main_category_id,
            'sub_category_name' => $request->sub_category_name,
            'subcategory_icon' => $imageName,
            'status' => 1,
        ]);

        return json_encode([
            'status' => 1,
            'messages' => 'Subcategory create successfully.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['subcategory'] = SubCategory::find($id);
        $data['maincategorys'] = MainCategory::where('status', 1)->get();
        $data['mode'] = 'Edit';
        $editData = view('admin.subcategory.edit', $data)->render();

        return json_encode([
            'status' => 1,
            'data'   => $editData
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $categoryId = $request->update_id;

        $messages = [
            'sub_category_name.required' => 'Please enter category name',
            'main_category_id.required' => 'Please select main category',
        ];

        $validatedData = $request->validate([
            'sub_category_name' => 'required',
            'main_category_id' => 'required',
        ], $messages);

        $originalImage = $request->file('subcategory_icon');
        
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/category/thumbnail/';
            $originalPath = public_path().'/upload/category/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);
        }

        if(!empty($originalImage)) {
            SubCategory::where('id', $categoryId)->update([
                'main_category_id' => $request->main_category_id,
                'sub_category_name' => $request->sub_category_name,
                'subcategory_icon' => $imageName,
                'status' => $request->status ? $request->status : 0,
            ]);
        } else {
           SubCategory::where('id', $categoryId)->update([
            'main_category_id' => $request->main_category_id,
            'sub_category_name' => $request->sub_category_name,
            'status' => $request->status ? $request->status : 0,
        ]);
       }



       return json_encode([
        'status' => 1,
        'messages' => 'Subcategory update successfully.'
    ]);
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = SubCategory::find($id);
        if ($subcategory->delete()) {
            return response()->json(['status' => 1, 'messages' => 'Subcategory delete successfully']);
        }
    }

    public function checkUniqueCategoryName(Request $request) {

        $name = $request->categoryName;
        $id = $request->categoryID;
        if(!empty($id)) {
            $maincategory = MainCategory::where('id','!=',$id)->where('main_category_name', $name)->get();
            if($maincategory->count()) {
                return json_encode([
                    'msg' => 'true'
                ]);
            } else {
                return json_encode([
                    'msg' => 'false'
                ]);
            }
        } else {
            $maincategory = MainCategory::where('main_category_name', $name)->get();
            if($maincategory->count()) {
                return json_encode([
                    'msg' => 'true'
                ]);
            } else {
                return json_encode([
                    'msg' => 'false'
                ]);
            }
        }
    }

    public function statusUpdate($id)
    {
        $subCategory = SubCategory::find($id);
        if($subCategory->status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        SubCategory::where('id', $id)->update([
            'status' => $status,
        ]);

        return response()->json(['status' => TRUE, 'message' => 'Category status change successfully.']);
    }
}
