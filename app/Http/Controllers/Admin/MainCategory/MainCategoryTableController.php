<?php

namespace App\Http\Controllers\Admin\MainCategory;

use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use App\Models\MainCategoryModel\MainCategory;

class MainCategoryTableController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

	public function __invoke()
	{
		return Datatables::make($this->getForDataTable())
		->addIndexColumn()
		->addColumn('status', function ($user) {
			if ($user->status == '1') {
				return "<label class='label label-info'>Active</label>";
			}else{
				return "<label class='label label-danger'>Deactive</label>";
			}
		})
		->addColumn('actions', function ($user) {

			$data ='<a href="javascript:;" onclick="editMainCategoryModel('.$user->id.')" class="btn btn-outline-warning" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Edit Category"><i class="icofont icofont-edit"></i></a>
				<button type="button" class="btn btn-outline-danger" onclick="deleteMainCategoryModel('.$user->id.')" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Delete Category"><i class="icofont icofont-trash"></i>
				</button> ';

			return $data;
		})
		->rawColumns(['actions','status'])
		->make(true);
	}

	public function getForDataTable()
	{
        /**
         * Note: You must return deleted_at or the Career getActionButtonsAttribute won't
         * be able to differentiate what buttons to show for each row.
         */
        return MainCategory::query();
    }
}
