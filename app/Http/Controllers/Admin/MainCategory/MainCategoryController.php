<?php

namespace App\Http\Controllers\Admin\MainCategory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use Validator;
use DB;
use DataTables;
use App\Models\MainCategoryModel\MainCategory;
use Illuminate\Support\Facades\Hash;
use Image;

class MainCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $maincategory = MainCategory::orderBy('status', 'DESC')->orderBy('id', 'DESC')->get();
            return Datatables::of($maincategory)
            ->addIndexColumn()
            ->addColumn('status', function ($user) {
                if ($user->status == '1') {
                    return "<label data-id='".$user->id."' class='label label-info status-update status_list'>Active</label>";
                }else{
                    return "<label data-id='".$user->id."' class='label label-danger status-update status_list'>Deactive</label>";
                }
            })
            ->editColumn('category_icon', function ($row) {
                $url = asset('public/upload/category/thumbnail/' . $row->category_icon);
                return '<img src="' . $url . '" border="0" width="40" class="img-rounded" align="center" />';
            })
            ->addColumn('action', function ($user) {
                $data ='<a href="javascript:;" onclick="editMainCategoryModel('.$user->id.')" class="btn btn-outline-warning" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Edit Category"><i class="icofont icofont-edit"></i></a>
                <button type="button" class="btn btn-outline-danger" onclick="deleteMainCategoryModel('.$user->id.')" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Delete Category"><i class="icofont icofont-trash"></i>
                </button> ';
                return $data;
            })
            ->rawColumns(['action','status','category_icon'])
            ->make(true);
        } else {
            return view('admin.maincategory.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'main_category_name.required' => 'Please enter category name',
        ];

        $validatedData = $request->validate([
            'main_category_name' => 'required',
        ], $messages);

        $originalImage = $request->file('category_icon');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/category/thumbnail/';
            $originalPath = public_path().'/upload/category/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);
        }

        MainCategory::create([
            'main_category_name' => $request->main_category_name,
            'category_icon' => $imageName,
            'status' => 1,
        ]);

        return json_encode([
            'status' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $maincategory = MainCategory::find($id);
        $mode = 'Edit';

        $editData = view('admin.maincategory.edit', compact('maincategory', 'mode'))->render();
        return json_encode([
            'status'=>1,
            'data'=>$editData
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $categoryId = $request->update_id;

        $messages = [
            'main_category_name.required' => 'Please enter category name',
        ];

        $validatedData = $request->validate([
            'main_category_name' => 'required',
        ], $messages);

        $originalImage = $request->file('category_icon');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/upload/category/thumbnail/';
            $originalPath = public_path().'/upload/category/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
            $thumbnailImage->save($thumbnailPath.$imageName);
        }

        if(!empty($originalImage)) {
            MainCategory::where('id', $categoryId)->update([
                'main_category_name' => $request->main_category_name,
                'category_icon' => $imageName,
                'status' => $request->status ? $request->status : 0,
            ]);
        } else {
           MainCategory::where('id', $categoryId)->update([
            'main_category_name' => $request->main_category_name,
            'status' => $request->status ? $request->status : 0,
        ]);
       }


       return json_encode(array('status'=>1));
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $maincategory = MainCategory::find($id);
        $maincategory->delete();

        return response()->json(['status' => 1, 'messages' => 'Category delete successfully']);
    }

    public function MainCategoryDelete(Request $request) {

        $maincategory = MainCategory::find($request->id);
        $maincategory->delete();

        Session::flash('error', 'Category deleted successfully!');
        return redirect()->route('maincategory.index');
    }

    public function checkUniqueCategoryName(Request $request) {

        $name = $request->categoryName;
        $id = $request->categoryID;
        if(!empty($id)) {
            $maincategory = MainCategory::where('id','!=',$id)->where('main_category_name', $name)->get();
            if($maincategory->count()) {
                return json_encode([
                    'msg' => 'true'
                ]);
            } else {
                return json_encode([
                    'msg' => 'false'
                ]);
            }
        } else {
            $maincategory = MainCategory::where('main_category_name', $name)->get();
            if($maincategory->count()) {
                return json_encode([
                    'msg' => 'true'
                ]);
            } else {
                return json_encode([
                    'msg' => 'false'
                ]);
            }
        }
    }

    public function statusUpdate($id)
    {
        $maincategory = MainCategory::find($id);
        if($maincategory->status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        MainCategory::where('id', $id)->update([
            'status' => $status,
        ]);

        return response()->json(['status' => TRUE, 'message' => 'Category status change successfully.']);
    }
}
