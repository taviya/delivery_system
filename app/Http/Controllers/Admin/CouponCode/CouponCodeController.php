<?php

namespace App\Http\Controllers\Admin\CouponCode;

use App\Models\CouponCodeModel\CouponCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Image;

class CouponCodeController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $couponCode = CouponCode::orderBy('status', 'DESC')->orderBy('id', 'DESC')->get();
            return Datatables::of($couponCode)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $id = $row->id;
                    $status = $row->status;
                    return view('admin.datatable.status', compact('id', 'status'));
                })
                ->addColumn('action', function ($row) {
                    $id = $row->id;
                    $edit = 'Edit Coupon Code';
                    $delete = 'Delete Coupon Code';
                    return view('admin.datatable.action', compact('id', 'edit', 'delete'));
                })
                ->editColumn('percentage_discount', function ($row) {
                    return $row->percentage_discount. '%';
                })
                ->editColumn('expired_date', function ($row) {
                    return date('d-m-Y', strtotime($row->expired_date));
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        } else {
            return view('admin.coupon_code.coupon_code_list');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:coupon_code',
            'percentage_discount' => 'required',
            'description' => 'required',
            'expired_date' => 'required',
            'max_discount_amount' => 'required',
        ]);

        $addArray = [
            'name' => $request->name,
            'percentage_discount' => $request->percentage_discount,
            'description' => $request->description,
            'max_discount_amount' => $request->max_discount_amount,
            'expired_date' => date('Y-m-d', strtotime($request->expired_date)),
            'status' => 1,
        ];

        $addCouponCode = CouponCode::create($addArray);
        $couponCode = CouponCode::find($addCouponCode->id);
        if ($couponCode) {
            return response()->json(['status' => TRUE, 'message' => 'Coupon create successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CouponCode  $couponCode
     * @return \Illuminate\Http\Response
     */
    public function show(CouponCode $couponCode)
    {
        //
    }

    /**
     * @param $id
     * Show the form for editing the specified resource.
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function edit($id)
    {
        $couponCode = CouponCode::find($id);
        $mode = 'Edit';
        $editData = view('admin.coupon_code.edit', compact('couponCode', 'mode'))->render();
        return response()->json(['status' => TRUE, 'data' => $editData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CouponCode  $couponCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:coupon_code,name,'.$request->update_id,
            'percentage_discount' => 'required',
            'description' => 'required',
            'expired_date' => 'required',
            'max_discount_amount' => 'required',
        ]);

        $couponCode = CouponCode::find($request->update_id);
        $couponCode->name = $request->name;
        $couponCode->percentage_discount = $request->percentage_discount;
        $couponCode->max_discount_amount = $request->max_discount_amount;
        $couponCode->description = $request->description;
        $couponCode->expired_date = date('Y-m-d', strtotime($request->expired_date));
        $couponCode->status = $request->status ? $request->status : 0;

        if ($couponCode->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Coupon code update successfully.']);
        }
    }

    public function checkUniqueCouponCode(Request $request)
    {
        $name = $request->name;
        $id = $request->update_id;

        if (!empty($id)) {
            $nameCount = CouponCode::where('id', '!=', $id)->where('name', $name)->get();
            if ($nameCount->count()) {
                return response()->json(['msg' => 'true']);
            } else {
                return response()->json(['msg' => 'false']);
            }
        } else {
            $nameCount = CouponCode::where('name', $name)->get();
            if ($nameCount->count()) {
                return response()->json(['msg' => 'true']);
            } else {
                return response()->json(['msg' => 'false']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CouponCode  $couponCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $couponCode = CouponCode::find($request->del_id);
        if ($couponCode->delete()) {
            return response()->json(['status' => TRUE, 'message' => 'Coupon code delete successfully.']);
        }
    }

    /**
     * Coupon Code status change.
     *
     * @param  \App\CouponCode  $couponCode
     * @return \Illuminate\Http\Response
     */
    public function statusChange($id)
    {
        $CouponCode = CouponCode::find($id);
        $CouponCode->status = $CouponCode->status ? 0 : 1;
        if ($CouponCode->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Coupon code status change successfully.']);
        }
    }
}
