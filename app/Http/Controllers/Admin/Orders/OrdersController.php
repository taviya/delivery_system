<?php

namespace App\Http\Controllers\Admin\Orders;

use App\Models\ShippingModel\Orders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Session;
use Image;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {

            $orders = Orders::with('getUserForOrder')->orderBy('id', 'DESC')->groupBy('order_number')->get();
            return Datatables::of($orders)
                    ->addIndexColumn()
                    /*->addColumn('status', function($row) {
                        $id = $row->id;
                        $status = $row->status;
                        return view('admin.datatable.status', compact('id', 'status'));
                    })*/
                    ->addColumn('customer_name', function ($row) {
                        return $row->getUserForOrder->first_name.' '.$row->getUserForOrder->last_name;
                    })
                    /*->addColumn('action', function($row) {
                        $id = $row->id;
                        $edit  = 'Edit Shop';
                        $delete  = 'Delete Shop';
                        return view('admin.datatable.action', compact('id', 'edit', 'delete'));
                    })*/
                    ->rawColumns(['customer_name'])
                    ->make(true);
        }else{
            return view('admin.orders.orders_list');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show(Orders $orders)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(Orders $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Orders $orders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(Orders $orders)
    {
        //
    }
}
