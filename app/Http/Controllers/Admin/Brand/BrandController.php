<?php

namespace App\Http\Controllers\Admin\Brand;

use App\Models\BrandModel\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $brand = Brand::orderBy('status', 'DESC')->orderBy('id', 'DESC')->get();
            return Datatables::of($brand)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $id = $row->id;
                    $status = $row->status;
                    return view('admin.datatable.status', compact('id', 'status'));
                })
                ->addColumn('action', function ($row) {
                    $id = $row->id;
                    $edit = 'Edit Brand';
                    $delete = 'Delete Brand';
                    return view('admin.datatable.action', compact('id', 'edit', 'delete'));
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        } else {
            return view('admin.brand.brand_list');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'brand_name' => 'required|unique:brand',
        ]);

        $addArray = [
            'brand_name' => $request->brand_name,
            'status' => 1,
        ];

        $addBrand = Brand::create($addArray);
        $brand = Brand::find($addBrand->id);
        if ($brand) {
            return response()->json(['status' => TRUE, 'message' => 'Brand create successfully.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        $mode = 'Edit';
        $editData = view('admin.brand.edit', compact('brand', 'mode'))->render();
        return response()->json(['status' => TRUE, 'data' => $editData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'brand_name' => 'required|unique:brand,brand_name,'.$request->update_id,
        ]);

        $brand = Brand::find($request->update_id);
        $brand->brand_name = $request->brand_name;
        $brand->status = $request->status ? $request->status : 0;

        if ($brand->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Brand update successfully.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $brand = Brand::find($request->del_id);
        if ($brand->delete()) {
            return response()->json(['status' => TRUE, 'message' => 'Brand delete successfully.']);
        }
    }

    /**
     * Brand status change.
     *
     * @param  \App\Http\Controllers\Admin\Brand\  $couponCode
     * @return \Illuminate\Http\Response
     */
    public function statusChange($id)
    {
        $brand = Brand::find($id);
        $brand->status = $brand->status ? 0 : 1;
        if ($brand->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Brand status change successfully.']);
        }
    }
}
