<?php

namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;
use Validator;
use DB;
use DataTables;
use App\Models\MainCategoryModel\MainCategory;
use App\Models\SubCategoryModel\SubCategory;
use App\Models\ProductModel\Product;
use App\Models\ProductModel\ProductMeta;
use App\Models\BrandModel\Brand;
use App\Models\Shop\Shop;
use App\Models\GeneralSetting;
use Illuminate\Support\Facades\Hash;
use Image;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if (!userRoleCheck([1, 2])) {
                return redirect()->route('admin.dashboard');
            }
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $product = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
            ->leftjoin('sub_categories as sc', 'products.sub_category_id', '=', 'sc.id')
            ->leftjoin('shop as s', 'products.shop_id', '=', 's.id')
            ->select([
                'products.*',
                'mc.id as mid',
                'mc.main_category_name',
                'mc.status as mstatus',
                'mc.deleted_at as mdeleted_at',
                'sc.sub_category_name',
                's.shop_name',
            ])
            ->where('mc.status',1)
            ->where('mc.deleted_at','=',NULL);
            if (!userRoleCheck([1])) {
                $product->where('created_by', auth()->user()->id);
            }
            $product->orderBy('products.status', 'DESC')
            ->orderBy('products.id', 'DESC')->get();

            return Datatables::of($product)
            ->addIndexColumn()
            ->editColumn('images', function ($singleProduct) {
                $product_image = explode(',', $singleProduct->product_image);
                $url = asset('public/upload/product/thumbnail/' . $product_image[0]);
                return '<img src="' . $url . '" border="0" width="100" class="img-rounded" align="center" />';
            })
            ->addColumn('price_gst', function ($singleProduct) {
                $commissionAmount = $singleProduct->original_price * shopCommission($singleProduct->shop_id) / 100;
                return round($singleProduct->price - $commissionAmount);
            })
            /*->addColumn('price_gst_commission', function ($singleProduct) {
                $commissionAmount = $singleProduct->original_price * shopCommission($singleProduct->shop_id) / 100;
                return round($singleProduct->price - $commissionAmount);
            })*/
            ->addColumn('status', function ($singleProduct) {
                if ($singleProduct->status == '1') {
                    return "<label data-id='".$singleProduct->id."' class='label label-info status-update status_list'>Active</label>";
                }else{
                    return "<label data-id='".$singleProduct->id."' class='label label-danger status-update status_list'>Deactive</label>";
                }
            })
            ->addColumn('action', function ($singleProduct) {
                $data ='<a href='.route("product.edit",$singleProduct->id).' class="btn btn-outline-warning" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Edit Product"><i class="icofont icofont-edit"></i></a>
                <button type="button" class="btn btn-outline-danger" onclick="deleteMainCategoryModel('.$singleProduct->id.')" data-toggle="tooltip" data-trigger="hover" data-placement="top" title="Delete Product"><i class="icofont icofont-trash"></i>
                </button> ';
                return $data;
            })
            ->rawColumns(['action','status','images'])
            ->make(true);
        } else {
            return view('admin.product.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $maincategorys = MainCategory::where('status', 1)->get();
        $shops = Shop::where('status', 1)->get();
        $shops = Shop::select('id', 'shop_name')->where(array('shop_user_status' => 1, 'status' => 1))->get();
        $brands = Brand::where('status', 1)->get();
        return view('admin.product.create',compact('maincategorys','shops','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validationArray = [
            'product_name' => 'required',
            'brand_id' => 'required',
            'stock' => 'required',
            'main_category_id' => 'required',
            'sub_category_id' => 'required',
            'price' => 'required',
        ];
        if (!userRoleCheck([2])) {
            $validationArray['shop'] = 'required';
        }

        $request->validate($validationArray);

        $images = $request->file('product_image');
        if ($request->hasFile('product_image')) :
            foreach ($images as $item):
                $imageName = time().$item->getClientOriginalName();
                $thumbnailImage = Image::make($item);
                $thumbnailPath = public_path().'/upload/product/thumbnail/';
                $originalPath = public_path().'/upload/product/';
                $thumbnailImage->save($originalPath.$imageName);
                $thumbnailImage->resize(350,350);
                $thumbnailImage->save($thumbnailPath.$imageName);
                $arr[] = $imageName;
            endforeach;
            $image = implode(",", $arr);
        else:
            $image = NULL;
        endif;

        // if(!empty($request->price)) {
        //     $price = commission($request->price);
        // } else {
        //     $price = $request->price;
        // }

        if(!empty($request->product_specification_heading)) {
            $specified_headings = [];
            foreach ($request->product_specification_heading as $key_head => $heading_values) {
                $specified_headings[] = $heading_values;
            }
            $specified_heading = implode(",", $specified_headings);
        } else {
            $specified_heading = NULL;
        }

        if(!empty($request->product_specification_value)) {
            $specified_values = [];
            foreach ($request->product_specification_value as $key_value => $heading_sp_values) {
                $specified_values[] = $heading_sp_values;
            }
            $specified_value = implode(",", $specified_values);
        } else {
            $specified_value = NULL;
        }

        if (!userRoleCheck([2])) {
            $shopId = $request->shop;
        } else {
            $shopId = Auth::user()->shop_id;
        }

        if(!empty($request->price)) {
            $price = commissionandgst($shopId,$request->product_gst,$request->price);
        } else {
            $price = $request->price;
        }

        $products = Product::create([
            'product_name' => ucwords(strtolower($request->product_name)),
            'product_sub_heading' => ucwords(strtolower($request->product_sub_heading)),
            'description' => $request->description,
            'shop_id' => $shopId,
            'created_by' => auth()->user()->id,
            'main_category_id' => $request->main_category_id,
            'sub_category_id' => $request->sub_category_id,
            'brand_id' => $request->brand_id,
            'price' => $price,
            'original_price' => $request->price,
            'stock' => $request->stock,
            'normalsize' => $request->sizesforall ? $request->sizesforall : 0,
            'shooessize' => $request->sizesforshooes ? $request->sizesforshooes : 0,
            'color' => $request->color,
            'product_gst' => $request->product_gst,
            'product_image' => $image,
            'product_specification_heading' => $specified_heading,
            'product_specification_value' => $specified_value,
            'new_arrivals' => $request->new_arrivals ? $request->new_arrivals : 0,
            'seasonal' => $request->seasonal ? $request->seasonal : 0,
        ]);

        if(!empty($request->sizesforall)) {
            if(!empty($request->ssize) || !empty($request->msize) || !empty($request->lsize) || !empty($request->xlsize) || !empty($request->xxlsize)) {
                $sizesStock = [
                    'sstock' => $request->sstock,
                    'mstock' => $request->mstock,
                    'lstock' => $request->lstock,
                    'xlstock' => $request->xlstock,
                    'xxlstock' => $request->xxlstock,
                ];

                $sizes = [
                    'ssize' => $request->ssize,
                    'msize' => $request->msize,
                    'lsize' => $request->lsize,
                    'xlsize' => $request->xlsize,
                    'xxlsize' => $request->xxlsize,
                ];

                $main_sizes_arr = array_combine($sizes, $sizesStock);

                if(!empty($main_sizes_arr)) {
                    foreach ($main_sizes_arr as $key => $value) {
                        ProductMeta::create([
                            'product_id' => $products->id,
                            'sizes' => $key,
                            'sizes_stock' => $value ? $value : 0,
                        ]);
                    }
                }
            }
        }

        if(!empty($request->sizesforshooes)) {
            if(!empty($request->ssize) || !empty($request->msize) || !empty($request->lsize) || !empty($request->xlsize) || !empty($request->xxlsize)) {
                $sizesStock = [
                    'sixstock' => $request->sixstock,
                    'sevenstock' => $request->sevenstock,
                    'eightstock' => $request->eightstock,
                    'ninestock' => $request->ninestock,
                    'tenstock' => $request->tenstock,
                    'elevenstock' => $request->elevenstock,
                    'twelvestock' => $request->twelvestock,
                    'thirteenstock' => $request->thirteenstock,
                ];

                $sizes = [
                    'six' => $request->six,
                    'seven' => $request->seven,
                    'eight' => $request->eight,
                    'nine' => $request->nine,
                    'ten' => $request->ten,
                    'eleven' => $request->eleven,
                    'twelve' => $request->twelve,
                    'thirteen' => $request->thirteen,
                ];

                $main_sizes_arr = array_combine($sizes, $sizesStock);

                if(!empty($main_sizes_arr)) {
                    foreach ($main_sizes_arr as $key => $value) {
                        ProductMeta::create([
                            'product_id' => $products->id,
                            'sizes' => $key,
                            'sizes_stock' => $value ? $value : 0,
                        ]);
                    }
                }
            }
        }

        Session::flash('success', 'Product create successfully!');
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::where('id',$id)->first();
        $productsMetas = ProductMeta::where('product_id',$id)->get();
        if(!empty($products->product_image)) {
            $productImages = explode(',', $products->product_image);
        } else {
            $productImages = '';
        }
        if(!empty($products->product_specification_heading) && !empty($products->product_specification_value)) {
            $productHeading = explode(',', $products->product_specification_heading);
            $productValue = explode(',', $products->product_specification_value);
            $specifications = array_combine($productHeading, $productValue);

        } else {
            $specifications = '';
        }
        $maincategorys = MainCategory::where('status', 1)->get();
        $shops = Shop::where('status', 1)->get();
        $brands = Brand::where('status', 1)->get();
        return view('admin.product.edit',compact('maincategorys','shops','brands','products','productsMetas','productImages','specifications'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $product = Product::where('id',$request->product_id)->first();
        $productImages = $product->product_image;

        $validationArray = [
            'product_name' => 'required',
            'brand_id' => 'required',
            'stock' => 'required',
            'main_category_id' => 'required',
            'sub_category_id' => 'required',
            'price' => 'required',
        ];
        if (!userRoleCheck([2])) {
            $validationArray['shop'] = 'required';
        }
        $request->validate($validationArray);

        $images = $request->file('product_image');
        if ($request->hasFile('product_image')) :
            foreach ($images as $item):
                $imageName = time().$item->getClientOriginalName();
                $thumbnailImage = Image::make($item);
                $thumbnailPath = public_path().'/upload/product/thumbnail/';
                $originalPath = public_path().'/upload/product/';
                $thumbnailImage->save($originalPath.$imageName);
                $thumbnailImage->resize(350,350);
                $thumbnailImage->save($thumbnailPath.$imageName);
                $arr[] = $imageName;
            endforeach;
            if(!empty($productImages)) {

                $existingimagesArray = explode(',',$productImages);
                $mainimagearray = array_merge($existingimagesArray,$arr);
                $image = implode(",", $mainimagearray);
            } else {
                $image = implode(",", $arr);
            }
        else:
            $image = $productImages;
        endif;


        if(!empty($request->product_specification_heading)) {
            $specified_headings = [];
            foreach ($request->product_specification_heading as $key_head => $heading_values) {
                $specified_headings[] = $heading_values;
            }
            $specified_heading = implode(",", $specified_headings);
        } else {
            $specified_heading = NULL;
        }

        if(!empty($request->product_specification_value)) {
            $specified_values = [];
            foreach ($request->product_specification_value as $key_value => $heading_sp_values) {
                $specified_values[] = $heading_sp_values;
            }
            $specified_value = implode(",", $specified_values);
        } else {
            $specified_value = NULL;
        }

        if (!userRoleCheck([2])) {
            $shopId = $request->shop;
        } else {
            $shopId = Auth::user()->shop_id;
        }
        if(!empty($request->price)) {
            $price = commissionandgst($shopId,$request->product_gst,$request->price);
        } else {
            $price = $request->price;
        }

        $products = Product::where("id",$request->product_id)->update([
            'product_name' => ucwords(strtolower($request->product_name)),
            'product_sub_heading' => ucwords(strtolower($request->product_sub_heading)),
            'description' => $request->description,
            'shop_id' => $shopId,
            'main_category_id' => $request->main_category_id,
            'sub_category_id' => $request->sub_category_id,
            'brand_id' => $request->brand_id,
            'price' => $price,
            'original_price' => $request->price,
            'stock' => $request->stock,
            'normalsize' => $request->sizesforall ? $request->sizesforall : 0,
            'shooessize' => $request->sizesforshooes ? $request->sizesforshooes : 0,
            'color' => $request->color,
            'product_image' => $image,
            'product_gst' => $request->product_gst,
            'product_specification_heading' => $specified_heading,
            'product_specification_value' => $specified_value,
            'new_arrivals' => $request->new_arrivals ? $request->new_arrivals : 0,
            'seasonal' => $request->seasonal ? $request->seasonal : 0,
        ]);

        if(!empty($request->sizesforall)) {

            ProductMeta::where('product_id',$request->product_id)->delete();

            if(!empty($request->ssize) || !empty($request->msize) || !empty($request->lsize) || !empty($request->xlsize) || !empty($request->xxlsize)) {
                $sizesStock = [
                    'sstock' => $request->sstock,
                    'mstock' => $request->mstock,
                    'lstock' => $request->lstock,
                    'xlstock' => $request->xlstock,
                    'xxlstock' => $request->xxlstock,
                ];

                $sizes = [
                    'ssize' => $request->ssize,
                    'msize' => $request->msize,
                    'lsize' => $request->lsize,
                    'xlsize' => $request->xlsize,
                    'xxlsize' => $request->xxlsize,
                ];

                $main_sizes_arr = array_combine($sizes, $sizesStock);

                if(!empty($main_sizes_arr)) {
                    foreach ($main_sizes_arr as $key => $value) {
                        ProductMeta::create([
                            'product_id' => $request->product_id,
                            'sizes' => $key,
                            'sizes_stock' => $value ? $value : 0,
                        ]);
                    }
                }
            }
        }

        if(!empty($request->sizesforshooes)) {

            ProductMeta::where('product_id',$request->product_id)->delete();

            if(!empty($request->ssize) || !empty($request->msize) || !empty($request->lsize) || !empty($request->xlsize) || !empty($request->xxlsize)) {
                $sizesStock = [
                    'sixstock' => $request->sixstock,
                    'sevenstock' => $request->sevenstock,
                    'eightstock' => $request->eightstock,
                    'ninestock' => $request->ninestock,
                    'tenstock' => $request->tenstock,
                    'elevenstock' => $request->elevenstock,
                    'twelvestock' => $request->twelvestock,
                    'thirteenstock' => $request->thirteenstock,
                ];

                $sizes = [
                    'six' => $request->six,
                    'seven' => $request->seven,
                    'eight' => $request->eight,
                    'nine' => $request->nine,
                    'ten' => $request->ten,
                    'eleven' => $request->eleven,
                    'twelve' => $request->twelve,
                    'thirteen' => $request->thirteen,
                ];

                $main_sizes_arr = array_combine($sizes, $sizesStock);

                if(!empty($main_sizes_arr)) {
                    foreach ($main_sizes_arr as $key => $value) {
                        ProductMeta::create([
                            'product_id' => $request->product_id,
                            'sizes' => $key,
                            'sizes_stock' => $value ? $value : 0,
                        ]);
                    }
                }
            }
        }

        Session::flash('success', 'Product Update successfully!');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product->delete()) {
            return response()->json(['status' => 1, 'messages' => 'Product delete successfully']);
        }
    }

    public function checkUniqueCategoryName(Request $request) {

        $name = $request->categoryName;
        $id = $request->categoryID;
        if(!empty($id)) {
            $maincategory = MainCategory::where('id','!=',$id)->where('main_category_name', $name)->get();
            if($maincategory->count()) {
                return json_encode([
                    'msg' => 'true'
                ]);
            } else {
                return json_encode([
                    'msg' => 'false'
                ]);
            }
        } else {
            $maincategory = MainCategory::where('main_category_name', $name)->get();
            if($maincategory->count()) {
                return json_encode([
                    'msg' => 'true'
                ]);
            } else {
                return json_encode([
                    'msg' => 'false'
                ]);
            }
        }
    }

    public function statusUpdate($id)
    {
        $product = Product::find($id);
        $product->status = $product->status ? 0 : 1;
        if ($product->save()) {
            return response()->json(['status' => TRUE, 'message' => 'Product status change successfully.']);
        }
    }

    public function dependentDropdown($id)
    {
        $allsub = SubCategory::where('main_category_id',$id)->where('status',1)->pluck("sub_category_name","id");
        return response()->json($allsub);
    }

    public function deleteImage(Request $request) {

        $product = Product::where('id',$request->product_id)->first();
        $productImages = $product->product_image;

        if(!empty($product->product_image)) {
            $productImagesArray = explode(',', $product->product_image);
        } else {
            $productImagesArray = '';
        }

        $imagesArray = explode(',',$request->imagename);

        $resultMain = array();
        foreach ($productImagesArray as $arryexistedimageskey => $arryexistedimagesvalue){

            if(!in_array($arryexistedimagesvalue, $imagesArray))
            {
                $resultMain[$arryexistedimageskey]=$arryexistedimagesvalue;
            }
        }

        $stringArray = implode(",",$resultMain);

        Product::where('id',$request->product_id)->update([
            'product_image' => $stringArray
        ]);

        return response()->json(['status' => 1, 'message' => 'Image Delete successfully']);
    }

}
