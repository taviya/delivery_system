<?php

namespace App\Http\Controllers\User;

use App\Models\MainCategoryModel\MainCategory;
use App\Models\ProductModel\ProductMeta;
use App\Models\SubCategoryModel\SubCategory;
use App\Models\ProductModel\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $isCatUrl = 0;
        $isProdUrl = '';
        /*if (!empty($input)) {
            if (isset($input['category'])) {
                $checkedCat = MainCategory::where(array('status' => 1, 'id' => $input['category']))->first();
                if (!empty($checkedCat)) {
                    $isCatUrl = $checkedCat->id;
                }
            } elseif (isset($input['product'])) {
                $isProdUrl = $input['product'];
            }
        }*/

        /*$mainCategory = MainCategory::where('status', 1)->get();
        $subCategory = SubCategory::where('status', 1)->get();
        $clothingSize = ProductMeta::select('sizes')->distinct()->pluck('sizes')->toArray();
        $colors = Product::where('status', 1)->select('color')->distinct()->pluck('color')->toArray();
        $min = Product::where('status', 1)->min('price');
        $max = Product::where('status', 1)->max('price');*/
//        $colors = array_unique($color);
        return view('user.product.product_list', compact('isCatUrl', 'isProdUrl'));
    }

    public function productList(Request $request) {
        $input = $request->all();
        $products = Product::with('getMainCategory:id,main_category_name')->where('status', 1);
        if (!empty($input['min']) || !empty($input['max'])) {
            $products->where('price', '>', $input['min'])->where('price', '<', $input['max']);
        }
        if (!empty($input['search'])) {
            $products->whereHas('getMainCategory', function($q) use ($input){
                $q->where('main_category_name', 'like', '%' .$input['search']. '%')
                    ->orWhere('product_name', 'like', '%' . $input['search'] . '%');
            });
        }
        if (!empty($input['clothing_size'])) {
            $products->with('getSizeWiseProduct:id,product_id,sizes,sizes_stock')->whereHas('getSizeWiseProduct', function($q) use ($input){
                $q->whereIn('sizes', $input['clothing_size']);
            });
        }
        if (!empty($input['color'])) {
            $products->whereIn('color', $input['color']);
        }
        if (!empty($input['main_category'])) {
            $products->whereIn('main_category_id', $input['main_category']);
        }
        if (!empty($input['sub_category'])) {
            $products->whereIn('sub_category_id', $input['sub_category']);
        }
        if ($input['order_by'] == 'shorting_default'){
            $products->orderBy('created_at', 'desc');
        }else{
            $products->orderBy('price', $input['order_by']);
        }
        $productsCount = $products->count(); //8
        $limit = $input['globlPageCount'] + 20; //8
        $products = $products->skip(0)->take($limit)->get();
        $isLoadShow = $limit >= $productsCount ? FALSE : TRUE;

//        for filter side bar
        if($request->section == 'sub_category' && !$products->isEmpty())
        {
            $product_ids = $products->pluck('id')->toArray();
            $main_category_ids = $products->pluck('main_category_id')->toArray();
            $main_category_ids = array_unique($main_category_ids);
            $data['mainCategory'] = MainCategory::where('status', 1)->whereIn('id', $main_category_ids)->get();
            $product_colors = $products->pluck('color')->toArray();
            $data['colors'] = array_unique($product_colors);
            $data['clothingSize'] = ProductMeta::select('sizes')->whereIn('product_id', $product_ids)->distinct()->pluck('sizes')->toArray();

            $data['subCategory'] = SubCategory::where('status', 1)->get();

        }
        else
        {
            $data['mainCategory'] = MainCategory::where('status', 1)->get();
            $data['clothingSize'] = ProductMeta::select('sizes')->distinct()->pluck('sizes')->toArray();
            $data['colors'] = Product::where('status', 1)->select('color')->distinct()->pluck('color')->toArray();
            $data['subCategory'] = SubCategory::where('status', 1)->get();
        }


        $data['min'] = Product::where('status', 1)->min('price');
        $data['max'] = Product::where('status', 1)->max('price');
        $productFilterRender = view('user.product.product_filter_render', $data)->render();

        $productRender = view('user.product.product_render', compact('products'))->render();
        return response()->json(['status' => TRUE, 'append_product' => $productRender, 'product_filter_render' => $productFilterRender, 'isLoadShow' => $isLoadShow]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }

    public function getProductQuickView(Request $request)
    {
        $productId = $request->project_id;
        $data['products'] = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
        ->leftjoin('sub_categories as sc', 'products.sub_category_id', '=', 'sc.id')
        ->leftjoin('shop as s', 'products.shop_id', '=', 's.id')
        ->select([
            'products.*',
            'mc.id as mid',
            'mc.main_category_name',
            'mc.status as mstatus',
            'mc.deleted_at as mdeleted_at',
            'sc.sub_category_name',
            's.shop_name',
        ])
        ->where('products.id', $productId)
        ->where('products.status',1)
        ->where('mc.status',1)
        ->where('mc.deleted_at','=',NULL)
        ->orderBy('products.status', 'DESC')
        ->orderBy('products.id', 'DESC')->first();

        $data['sizes'] = ProductMeta::where('product_id',$data['products']->id)->get();
        $quickView = view('user.product.product_quickview', $data)->render();
        return response()->json(['status' => TRUE, 'quick_view' => $quickView]);
    }
}
