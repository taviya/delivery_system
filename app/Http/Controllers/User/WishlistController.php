<?php

namespace App\Http\Controllers\User;

use App\Models\ProductModel\ProductMeta;
use App\Models\ProductModel\Product;
use App\Models\WishlistModel\Wishlist;
use App\Models\MainCategoryModel\MainCategory;
use App\Models\SubCategoryModel\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class WishlistController extends Controller
{
    public function wishlist(Request $request)
    {
        $product_id = $request->product_id;
        $user_id = Auth::user()->id;
        $wishlist = Wishlist::where('product_id',$product_id)->where('user_id',$user_id)->first();
        if(!empty($wishlist)) {
            Wishlist::where('product_id',$product_id)->where('user_id',$user_id)->delete();
            $wishlistCount = Wishlist::where('user_id',$user_id)->where('seen_product',0)->count();
            return response()->json(['status' => 2, 'message' => 'remove', 'product_id' => $product_id,'wishlistcount'=>$wishlistCount]);
        } else {
            Wishlist::create([
                'product_id' => $product_id,
                'user_id' => $user_id,
            ]);
            $wishlistCount = Wishlist::where('user_id',$user_id)->where('seen_product',0)->count();
            return response()->json(['status' => 1, 'message' => 'added','product_id' => $product_id,'wishlistcount'=>$wishlistCount]);
        }
    }

    public function wishlistlisting(Request $request)
    {
        if(empty(Auth::user()->id))
        {
            return view('user.404');
        }
        $user_id = Auth::user()->id;
        // Wishlist::where('user_id',$user_id)->update(['seen_product' => 1]);
        $products = Product::leftjoin('wishlist', 'products.id', '=', 'wishlist.product_id')
        ->select([
            'products.*',
            'wishlist.id as wid',
            'wishlist.user_id as user_id',
        ])->where('user_id',$user_id)->where('status', 1)->paginate(15);
        
        return view('user.product.product_whishlist', compact('products'));
    }

    public function wishlistmainList(Request $request) {

        if(empty(Auth::user()->id))
        {
            return view('user.404');
        }

        $input = $request->all();
        $products = Product::leftjoin('wishlist', 'products.id', '=', 'wishlist.product_id')
        ->select([
            'products.*',
            'wishlist.id as wid',
            'wishlist.user_id as user_id',
        ])->where('user_id',Auth::user()->id)->with('getMainCategory')->where('price', '>', $input['min'])->where('price', '<', $input['max'])->where('status', 1);
        if (!empty($input['search'])) {
            $products->whereHas('getMainCategory', function($q) use ($input){
                $q->where('main_category_name', 'like', '%' .$input['search']. '%')
                    ->orWhere('product_name', 'like', '%' . $input['search'] . '%');
            });
        }
        if (!empty($input['clothing_size'])) {
            $products->with('getSizeWiseProduct')->whereHas('getSizeWiseProduct', function($q) use ($input){
                $q->whereIn('sizes', $input['clothing_size']);
            });
        }
        if (!empty($input['color'])) {
            $products->whereIn('color', $input['color']);
        }
        if (!empty($input['main_category'])) {
            $products->whereIn('main_category_id', $input['main_category']);
        }
        if (!empty($input['sub_category'])) {
            $products->whereIn('sub_category_id', $input['sub_category']);
        }
        if ($input['order_by'] == 'shorting_default'){
            $products->orderBy('products.created_at', 'desc');
        }else{
            $products->orderBy('price', $input['order_by']);
        }
        $productsCount = $products->count(); //8
        $limit = $input['globlPageCount'] + 16; //8
        $products = $products->skip(0)->take($limit)->get();
        $isLoadShow = $limit >= $productsCount ? FALSE : TRUE;
       
        $productRender = view('user.product.product_render', compact('products'))->render();
        return response()->json(['status' => TRUE, 'append_product' => $productRender, 'isLoadShow' => $isLoadShow]);
    }
}
