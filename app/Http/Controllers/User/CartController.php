<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\Models\Shop\Shop;
use App\Models\ProductModel\Product;
use App\Models\ProductModel\ProductMeta;
use App\Models\WishlistModel\Wishlist;
use App\Models\CartModel\Cart;
use Illuminate\Http\Request;
use Auth;

class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addtocart(Request $request)
    {
        $selectednormalsize = $request->selectednormalsize;
        $selectedshooessize = $request->selectedshooessize;
        $selectedproduct    = $request->selectedproduct;
        $user_id = Auth::user()->id;

        if(isset($selectednormalsize)) {
            $sel_size = $selectednormalsize;
        }

        if(isset($selectedshooessize)) {
            $sel_size = $selectedshooessize;
        }

        if(empty($selectednormalsize) && empty($selectednormalsize)) {
            $sel_size = NULL;
        }

        $products = Product::select([
            'products.stock',
            'products.id',
        ])
        ->where('products.status',1)
        ->where('products.id',$selectedproduct)
        ->first();

        if(!empty($products)) {
            if($products->stock == 0) {
                return response()->json(['status' => 1, 'message' => 'Product is out of stock']);
            } else {
                $carts = Cart::where('product_id',$selectedproduct)
                ->where('user_id',$user_id)
                ->where('sel_size',$sel_size)
                ->first();
                if(!empty($carts)) {
                    return response()->json(['status' => 2, 'message' => 'Already Add to cart successfully']);
                } else {
                    Cart::create([
                        'product_id' => $selectedproduct,
                        'user_id'    => $user_id,
                        'sel_size'   => $sel_size,
                    ]);
                    Wishlist::where('product_id',$selectedproduct)->where('user_id',$user_id)->delete();
                    return response()->json(['status' => 2, 'message' => 'Add to cart successfully']);
                }
            }
        }
    }

    public function cart() {

        if(empty(Auth::user()->id)) {
            return redirect()->route('home');
        }
        return view('user.cart.cart');
    }

    public function getCart(Request $request) {

        $user_id = Auth::user()->id;

        if(!empty($request->productId)) {
            Cart::where('id',$request->cart_id)->where('product_id',$request->productId)->where('user_id',$user_id)->update(['sel_quantity' => $request->quantity ]);
        }

        if(!empty($request->clearcart)) {
            Cart::where('user_id',$user_id)->delete();
        }

        if(!empty($request->clearproduct)) {
            Cart::where('id',$request->cartId)->where('product_id',$request->oneproductId)->where('user_id',$user_id)->delete();
        }

        if(!empty($request->movewishlist)) {

            Cart::where('id',$request->cartId)->where('product_id',$request->oneproductId)->where('user_id',$user_id)->delete();
            Wishlist::create([
                'product_id' => $request->oneproductId,
                'user_id' => $user_id,
            ]);

        }

        $data['products'] =  Cart::leftjoin('products', 'cart.product_id', '=', 'products.id')
        ->select([
            'products.product_name',
            'products.price',
            'products.id',
            'products.stock',
            'products.product_image',
            'cart.sel_size',
            'cart.sel_quantity',
            'cart.created_at as adddate',
            'cart.id as cart_id',
        ])
        ->where('products.status',1)
        ->where('cart.user_id',$user_id)
        ->orderBy('adddate', 'DESC')
        ->get();
        $editData = view('user.cart.cart_render', $data)->render();
        return response()->json(['status' => 1, 'data' => $editData]);
    }

    public function getAjaxCartView(Request $request) {

        $user_id = Auth::user()->id;

        if($request->cart_id && $request->product_id) {
            Cart::where('id',$request->cart_id)->where('product_id',$request->product_id)->where('user_id',$user_id)->delete();
        }

        $data['products'] =  Cart::leftjoin('products', 'cart.product_id', '=', 'products.id')
        ->select([
            'products.product_name',
            'products.price',
            'products.id',
            'products.stock',
            'products.product_image',
            'cart.sel_size',
            'cart.sel_quantity',
            'cart.created_at as adddate',
            'cart.id as cart_id',
        ])
        ->where('products.status',1)
        ->where('cart.user_id',$user_id)
        ->orderBy('adddate', 'DESC')
        ->get();

        $editData = view('user.cart.ajax_cart_view', $data)->render();
        return response()->json(['status' => 1, 'data' => $editData, 'cart_count' => count(cartProducts())]);
    }

    public function demolisting() {
        return view('user.product.product_demo');
    }
}
