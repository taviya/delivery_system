<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\Models\Shop\Shop;
use App\Models\ProductModel\Product;
use App\Models\ProductModel\ProductMeta;
use App\Models\ShippingModel\ShippingAddress;
use App\Models\ShippingModel\ShippingOrder;
use App\Models\ShippingModel\Orders;
use App\Models\ShippingModel\Payments;
use App\Models\PincodeModel\Pincode;
use App\Models\CartModel\Cart;
use Illuminate\Http\Request;
use Auth;

class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function checkoutShipping() {

        $data['cartproducts'] =  cartProducts();

        if(empty(Auth::user()->id) || count($data['cartproducts']) == 0) {
            return redirect()->route('home');
        }

        $user_id = Auth::user()->id;
        $data['shipping_address'] = ShippingAddress::where('user_id',$user_id)->orderby('id','DESC')->get();
        return view('user.checkout.checkout-shipping',$data);
    }

    public function checkoutReview() {

        $data['cartproducts'] =  cartProducts();

        if(empty(Auth::user()->id) || count($data['cartproducts']) == 0) {
            return redirect()->route('home');
        }

        $user_id = Auth::user()->id;
        $data['alreadyShipping'] = ShippingOrder::where('user_id','=', $user_id)->first();
        $data['shippingAddress'] = ShippingAddress::where('id','=', $data['alreadyShipping']->address_id)->first();
        if(empty($data['alreadyShipping'])) {
            return redirect()->route( 'checkout-shipping' );
        }
        return view('user.checkout.checkout-review',$data);
    }

    public function addShippingAddress(Request $request) {

        if(empty(Auth::user()->id)) {
            return redirect()->route('home');
        }

        $user_id = Auth::user()->id;

        $messages = [
            'ship_first_name.required' => 'Please enter first name',
            'ship_last_name.required' => 'Please enter last name',
            'street_address_1.required' => 'Please enter address',
            'street_address_2.required' => 'Please enter address',
            'city.required' => 'Please enter city name',
            'pin_code.required' => 'Please enter your pincode',
            'phone_number.required' => 'Please enter your mobile no',
        ];

        $validatedData = $request->validate([
            'ship_first_name' => 'required',
            'ship_last_name' => 'required',
            'street_address_1' => 'required',
            'street_address_2' => 'required',
            'city' => 'required',
            'pin_code' => 'required',
            'phone_number' => 'required',
        ], $messages);

        ShippingAddress::create([
            'user_id' => $user_id,
            'ship_first_name'  => $request->ship_first_name,
            'ship_last_name'      => $request->ship_last_name,
            'street_address_1'  => $request->street_address_1,
            'street_address_2'     => $request->street_address_2,
            'city'     => $request->city,
            'pin_code'     => $request->pin_code,
            'country'     => $request->country,
            'phone_number'     => $request->phone_number,
        ]);

        return redirect()->route( 'checkout-shipping' );
    }

    public function checkPincode(Request $request) {

        $pincode = $request->pin_code;
        $getPincode = Pincode::where('status',1)->where('pincode',$pincode)->first();
        if(!empty($getPincode)){
            return json_encode(['msg' => 'true']);
        } else {
            return json_encode(['msg' => 'false']);
        }
    }

    public function editShippingAddress(Request $request) {

        $address_id = $request->address_id;
        $addressmodals = ShippingAddress::find($address_id);
        
        $editData = view('user.checkout.addressmodals', compact('addressmodals'))->render();
        return json_encode([
            'status' => 1,
            'data'   => $editData
        ]);
    }

    public function updateShippingAddress(Request $request) {

        if(empty(Auth::user()->id)) {
            return redirect()->route('home');
        }

        $user_id = Auth::user()->id;

        $messages = [
            'ship_first_name.required' => 'Please enter first name',
            'ship_last_name.required' => 'Please enter last name',
            'street_address_1.required' => 'Please enter address',
            'street_address_2.required' => 'Please enter address',
            'city.required' => 'Please enter city name',
            'pin_code.required' => 'Please enter your pincode',
            'phone_number.required' => 'Please enter your mobile no',
        ];

        $validatedData = $request->validate([
            'ship_first_name' => 'required',
            'ship_last_name' => 'required',
            'street_address_1' => 'required',
            'street_address_2' => 'required',
            'city' => 'required',
            'pin_code' => 'required',
            'phone_number' => 'required',
        ], $messages);

        ShippingAddress::where('id','=', $request->id)->update([
            'user_id' => $user_id,
            'ship_first_name'  => $request->ship_first_name,
            'ship_last_name'      => $request->ship_last_name,
            'street_address_1'  => $request->street_address_1,
            'street_address_2'     => $request->street_address_2,
            'city'     => $request->city,
            'pin_code'     => $request->pin_code,
            'country'     => $request->country,
            'phone_number'     => $request->phone_number,
        ]);

        return redirect()->route( 'checkout-shipping' );
    }

    public function updateShippingOrder(Request $request) {

        if(empty(Auth::user()->id)) {
            return redirect()->route('home');
        }

        $user_id = Auth::user()->id;
        $alreadyShipping = ShippingOrder::where('user_id','=', $user_id)->first();
        if(!empty($alreadyShipping)) {
            ShippingOrder::where('user_id','=', $user_id)->update([
                'address_id'      => $request->address_id,
                'payment_method'  => $request->payment_method,
            ]);
        } else {
            ShippingOrder::create([
                'user_id' => $user_id,
                'address_id'      => $request->address_id,
                'payment_method'  => $request->payment_method,
            ]);
        }
        return json_encode([
            'status' => 1
        ]);
    }

    public function placeOrder(Request $request) {
        $cartproducts =  cartProducts();
        $user_id = Auth::user()->id;
        $randomOrderNumber = substr(str_shuffle("0123456789"), 0, 5);
        
        foreach ($cartproducts as $key => $cartproduct) {
            Orders::create([
                'product_id' => $cartproduct->id,
                'product_shop_id' => $cartproduct->shop_id,
                'user_id'    => $user_id,
                'order_number'   => $randomOrderNumber,
            ]);
        }
        $order_amount = grandtotal();
        $address = ShippingOrder::where('user_id',$user_id)->first();
        $address_id = $address['address_id'];
        
        Payments::create([
            'order_id'   => $randomOrderNumber,
            'user_id'    => $user_id,
            'shipping_address_id'   => $address_id,
            'order_amount'    => $order_amount,
            'payment_status'  => 0,
            'payment_type'    => 0,
        ]);

        Cart::where('user_id',$user_id)->delete();
        return redirect()->route( 'my-orders' )->with('success', 'Order Placed Successfully');;
    }
}
