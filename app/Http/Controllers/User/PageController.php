<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use Auth;

class PageController extends Controller
{
    public function index($slug)
    {
        $page = Menu::where('menu_status','1')->where('slug',$slug)->first();
        return view('user.pages.index',compact('page'));
    }
    public function demolisting() {
        return view('user.product.product_demo');
    }
}
