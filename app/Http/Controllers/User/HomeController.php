<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\Models\Shop\Shop;
use App\Models\ProductModel\Product;
use App\Models\ProductModel\ProductMeta;
use App\Models\SubCategoryModel\SubCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $generalSettings = GeneralSetting::first();
        $ourPartners = Shop::where('status',1)->get();
        $products = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
        ->leftjoin('sub_categories as sc', 'products.sub_category_id', '=', 'sc.id')
        ->leftjoin('shop as s', 'products.shop_id', '=', 's.id')
        ->select([
            'products.*',
            'mc.id as mid',
            'mc.main_category_name',
            'mc.status as mstatus',
            'mc.deleted_at as mdeleted_at',
            'sc.sub_category_name',
            's.shop_name',
        ])
        ->where('products.status',1)
        ->where('mc.status',1)
        ->where('mc.deleted_at','=',NULL)
        ->orderBy('products.status', 'DESC')
        ->orderBy('products.id', 'DESC')->get();
        return view('user.home', compact('generalSettings','ourPartners','products'));
    }

    public function categoryList($id) {
        $generalSettings = GeneralSetting::first();
        $ourPartners = Shop::where('status',1)->get();
        $subCategories = SubCategory::where('main_category_id',$id)->where('status',1)->orderBy('id', 'DESC')->take(12)->get();
        $products = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
        ->select([
            'products.product_name',
            'products.price',
            'products.id',
            'products.stock',
            'products.product_image',
            'mc.id as mid',
            'mc.main_category_name',
            'mc.status as mstatus',
            'mc.deleted_at as mdeleted_at',
        ])
        ->where('products.status',1)
        ->where('mc.status',1)
        ->where('mc.deleted_at','=',NULL)
        ->orderBy('products.status', 'DESC')
        ->orderBy('products.id', 'DESC')->take(10)->get();
        if(!empty($subCategories)) {
            return view('user.categoryhome', compact('generalSettings','ourPartners','subCategories','products'));
        } else {
            return view('user.404');
        }
    }

    public function allShorts(){
        return view('user.short_videos');
    }
}
