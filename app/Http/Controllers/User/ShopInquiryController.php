<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\ShopInquiry;
use Illuminate\Http\Request;
use App\Mail\DynamicMail;
use Illuminate\Support\Facades\Mail;
use Image;

class ShopInquiryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.shop_inquiry.shop_inquiry');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'shop_name' => 'required',
            'shop_email' => 'required',
            'shop_phone' => 'required',
            'address' => 'required',
            'turnover' => 'required',
            'shop_message' => 'required',
            'shop_image' => 'bail|required|file|max:5000kb|mimes:jpeg,png,jpg',
        ]);

        $name = $request->shop_name;
        $view = 'user.shop_inquiry.mail';
        $data = $request->all();

        $originalImage = $request->file('shop_image');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $originalPath = public_path().'/upload/shop_temp_image/';
            $thumbnailImage->save($originalPath.$imageName);
        }
        $generalSettings = GeneralSetting::first();
        $data = array('viewData' => $data);
        Mail::send($view, $data, function ($message) use ($imageName, $generalSettings) {
            $message->to($generalSettings->email)
                ->subject("Shop Inquiry")
                ->attach(public_path('upload/shop_temp_image/' . $imageName));
        });

        /*$data['image'] = $originalPath.'/'.$imageName;
        Mail::to(env('MAIL_TO'))->send(new DynamicMail($name, $view, $data));*/
        unlink($originalPath.'/'.$imageName);
        return redirect()->route('shop-form')->with('success', 'Inquiry sent successfully. we will contact you soon.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShopInquiry  $shopInquiry
     * @return \Illuminate\Http\Response
     */
    public function show(ShopInquiry $shopInquiry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShopInquiry  $shopInquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopInquiry $shopInquiry)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShopInquiry  $shopInquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShopInquiry $shopInquiry)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShopInquiry  $shopInquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopInquiry $shopInquiry)
    {
        //
    }
}
