<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\User;
use App\Models\Shop\Shop;
use App\Models\ProductModel\Product;
use App\Models\ProductModel\ProductMeta;
use App\Models\MainCategoryModel\MainCategory;
use App\Models\SubCategoryModel\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\DynamicMail;
use Image;
use Validator;
use Session;

class UserController extends Controller
{
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $generalSettings = GeneralSetting::first();
        $ourPartners = Shop::where('status',1)->get();
        $subCategories = SubCategory::where('status',1)->orderBy('id', 'DESC')->take(12)->get();
        
        $products = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
        ->select([
            'products.product_name',
            'products.price',
            'products.id',
            'products.stock',
            'products.product_image',
            'mc.id as mid',
            'mc.main_category_name',
            'mc.status as mstatus',
            'mc.deleted_at as mdeleted_at',
        ])
        ->where('products.status',1)
        ->where('mc.status',1)
        ->where('mc.deleted_at','=',NULL)
        ->orderBy('products.status', 'DESC')
        ->orderBy('products.id', 'DESC')->take(10)->get();
        return view('user.home', compact('generalSettings','ourPartners','products','subCategories'));
    }

    public function productDetail($id)
    {
        $data['products'] = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
        ->leftjoin('sub_categories as sc', 'products.sub_category_id', '=', 'sc.id')
        ->leftjoin('shop as s', 'products.shop_id', '=', 's.id')
        ->select([
            'products.*',
            'mc.id as mid',
            'mc.main_category_name',
            'mc.status as mstatus',
            'mc.deleted_at as mdeleted_at',
            'sc.sub_category_name',
            's.shop_name',
        ])
        ->where('products.id',$id)
        ->where('products.status',1)
        ->where('mc.status',1)
        ->where('mc.deleted_at','=',NULL)
        ->orderBy('products.status', 'DESC')
        ->orderBy('products.id', 'DESC')->first();

        if(!empty($data['products'])) {
            $data['relatedProducts'] = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
            ->leftjoin('sub_categories as sc', 'products.sub_category_id', '=', 'sc.id')
            ->leftjoin('shop as s', 'products.shop_id', '=', 's.id')
            ->select([
                'products.*',
                'mc.id as mid',
                'mc.main_category_name',
                'mc.status as mstatus',
                'mc.deleted_at as mdeleted_at',
                'sc.sub_category_name',
                's.shop_name',
            ])
            ->where('products.status',1)
            ->where('products.main_category_id',$data['products']->main_category_id)
            ->where('products.id','!=',$data['products']->id)
            ->where('mc.status',1)
            ->where('mc.deleted_at','=',NULL)
            ->orderBy('products.status', 'DESC')
            ->orderBy('products.id', 'DESC')->take(10)->get();

            $data['featuresProducts'] = Product::leftjoin('main_categories as mc', 'products.main_category_id', '=', 'mc.id')
            ->select([
                'products.*',
                'mc.id as mid',
                'mc.main_category_name',
                'mc.status as mstatus',
                'mc.deleted_at as mdeleted_at',
            ])
            ->where('products.status',1)
            ->where('products.main_category_id','!=',$data['products']->main_category_id)
            ->where('products.id','!=',$data['products']->id)
            ->where('mc.status',1)
            ->where('mc.deleted_at','=',NULL)
            ->orderBy('products.status', 'DESC')
            ->orderBy('products.id', 'DESC')->take(5)->get();

            $data['sizes'] = ProductMeta::where('product_id',$data['products']->id)->get();

            if(!empty($data['products']->product_specification_heading) && !empty($data['products']->product_specification_value))
            {
                $product_heading = explode(',', $data['products']->product_specification_heading);
                $product_value = explode(',', $data['products']->product_specification_value);
                $data['specifications'] = array_combine($product_heading, $product_value);
            }

            return view('user.product.product_detail',$data);
        } else {
            return view('user.404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'first_name.required' => 'Please enter first name',
            'last_name.required' => 'Please enter last name',
            'email.required' => 'Please enter email',
            'mobile_no.required' => 'Please enter mobile no',
            'gender.required' => 'Please select gender',
            'password.required' => 'Please enter password',
        ];

        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users',
            'mobile_no' => 'required|unique:users',
            'gender' => 'required',
            'password' => 'required',
        ], $messages);

        User::create([
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'mobile_no'  => $request->mobile_no,
            'gender'     => $request->gender,
            'created_by' => 0,
            'password'   => Hash::make($request->password),
        ]);

        return response()->json(array('status' => TRUE, 'message' => 'Registration successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return view('auth.account.account');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'mobile_no' => 'required',
            'avatar_location' => 'bail|file|max:5000kb|mimes:jpeg,png,jpg',
        ]);

        $originalImage = $request->file('avatar_location');
        $imageName = '';
        if ($originalImage) {
            $imageName = time().$originalImage->getClientOriginalName();
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = './assets/frontend/profiles/';
            $originalPath = './assets/frontend/profiles/';
            $thumbnailImage->save($originalPath.$imageName);
            $thumbnailImage->resize(150,150);
        }

        $userUpdate = array(
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'mobile_no' => $request->mobile_no,
            'avatar_location' => $imageName,
        );

        User::where('id', Auth::id())->update($userUpdate);
        return response()->json(['status' => TRUE, 'message' => 'Account update successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function contactUs() {
        $generalSettings = GeneralSetting::first();
        return view('user.contact.contact_us', compact('generalSettings'));
    }

    public function contactUsInquiry(Request $request) {

        $request->validate([
            'contact_name' => 'required',
            'contact_email' => 'required',
            'contact_phone' => 'required',
            'contact_message' => 'required',
        ]);
        $generalSettings = GeneralSetting::first();
        $name = $request->contact_name;
        $view = 'user.contact.mail';
        $data = $request->all();
        $data['subject'] = 'Contact us inquiry';
        Mail::to($generalSettings->email)->send(new DynamicMail($name, $view, $data));
        return redirect()->route('contact.us')->with('success', 'Thanks for contacting us we will be in touch with you shortly.');
    }

    public function userChangePassword(Request $request) {
        return view('auth.account.change_password');
    }

    public function myOrders(Request $request) {
        return view('auth.account.my-orders');
    }

    public function userPasswordSet(Request $request) {

        $messages = [
            'password.required' => 'The new password field is required',
            'password.confirmed' => "Password does'nt match"
        ];
        $validator = Validator::make($request->all(), [
            'confirmpassword' => 'required',
            'password' => 'required|confirmed'
        ], $messages);

        $user = User::find(Auth::id());
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json(['status' => 1, 'message' => 'Password changed successfully!']);

    }
}
