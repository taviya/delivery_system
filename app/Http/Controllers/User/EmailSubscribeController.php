<?php

namespace App\Http\Controllers\User;

use App\Models\MainCategoryModel\MainCategory;
use App\Models\ProductModel\ProductMeta;
use App\Models\SubCategoryModel\SubCategory;
use App\Models\ProductModel\Product;
use App\Models\GeneralSetting;
use App\Models\EmailSubscriberModel\EmailSubscriber;
use App\Mail\DynamicMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmailSubscribeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendmail(Request $request)
    {
        $request->validate([
            'email' => 'required',
        ]);
        $view = 'user.subscriber';
        $data = $request->all();
        
        $alreadySubscriber = EmailSubscriber::where('email',$data['email'])->first();
        
        if(!empty($alreadySubscriber)) {
            return response()->json(['status' => 1, 'messages' => 'You’re Already Subscribed!']);
        } else {
            EmailSubscriber::create([
                'email' => $data['email']
            ]);

            $generalSettings = GeneralSetting::first();
            $data = array('viewData' => $data);
            Mail::send($view, $data, function ($message) use ($generalSettings) {
                $message->to($generalSettings->email)
                ->subject("New Subscriber Mail");
            });
            return response()->json(['status' => 2, 'messages' => 'Thank you for Subscribing!']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
