<?php

namespace App\Http\Controllers;

use App\MetaField;
use Illuminate\Http\Request;

class MetaFieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MetaField  $metaField
     * @return \Illuminate\Http\Response
     */
    public function show(MetaField $metaField)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MetaField  $metaField
     * @return \Illuminate\Http\Response
     */
    public function edit(MetaField $metaField)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MetaField  $metaField
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MetaField $metaField)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MetaField  $metaField
     * @return \Illuminate\Http\Response
     */
    public function destroy(MetaField $metaField)
    {
        //
    }
}
