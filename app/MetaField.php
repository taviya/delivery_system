<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MetaField extends Model
{
    //
    use SoftDeletes;
    protected $table = 'site_meta_field';
    protected $fillable = ['name', 'value', 'status'];
    /*protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	];*/
}
