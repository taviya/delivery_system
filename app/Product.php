<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];
	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

    public function getMainCategory() {
        return $this->hasOne('App\Models\MainCategoryModel\MainCategory', 'id', 'main_category_id')
            ->whereIn('id', [2]);
    }
}

