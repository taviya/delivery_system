<?php

use App\MetaField;
use App\Models\GeneralSetting;
use App\Models\Menu;
use App\Models\Role\CustomRole;
use App\Models\Social;
use App\Models\WishlistModel\Wishlist;
use App\Models\MainCategoryModel\MainCategory;
use App\Models\SubCategoryModel\SubCategory;
use App\Models\CartModel\Cart;
use App\Models\Shop\Shop;
use Carbon\Carbon;

if (!function_exists('stringcutter')) {
	function stringcutter($text)
	{
		return Str::limit($text, 20, ' ...');
	}
}

if (!function_exists('wishlist')) {
	function wishlist($product_id) {
		if(Auth::user()) {
			$wishlist = Wishlist::where('product_id',$product_id)->where('user_id',Auth::user()->id)->first();
			if(!empty($wishlist)) {
				return true;
			} else {
				return false;
			}
		}
	}
}

if (!function_exists('getSiteSetting')) {
	function getSiteSetting() {
        return $generalSettings = GeneralSetting::first();
    }
}

if (!function_exists('allMainCategory')) {

	function allMainCategory() {
        $allmenu = MainCategory::where('main_categories.status',1)
        ->where('main_categories.deleted_at','=',NULL)
        ->orderBy('main_categories.id', 'DESC')
        ->take(3)
        ->get();

        return $allmenu;
    }
}

if (!function_exists('allSubCategory')) {

	function allSubCategory($id) {
        $allmenu = SubCategory::where('main_category_id',$id)
        ->where('sub_categories.status',1)
        ->where('sub_categories.deleted_at','=',NULL)
        ->orderBy('sub_categories.id', 'DESC')
//        ->take(8)
        ->get();

        return $allmenu;
    }
}

if (!function_exists('wishlistCount')) {

	function wishlistCount($id) {
        $wishlistallcount = Wishlist::where('user_id',$id)
        ->where('seen_product',0)
        ->count();

        return $wishlistallcount;
    }
}


if (!function_exists('footerpages')) {
    function footerpages()
    {
        return Menu::where('menu_status', '1')->where('menu_position', '2')->get();
    }
}

if (!function_exists('headerpages')) {
    function headerpages()
    {
        return Menu::where('menu_status', '1')->where('menu_position', '1')->get();
    }
}

if (!function_exists('socialdata')) {
    function socialdata()
    {
        return Social::orderBy('id', 'ASC')->get();
    }
}

if (!function_exists('footerTimer')) {
    function footerTimer()
    {
        $start_time = MetaField::where('name', 'start_time')->first()->value;
        $close_time = MetaField::where('name', 'close_time')->first()->value;

        $currentTime = strtotime(date('H:i:s'));
        if (strtotime($close_time.':00:00') >= $currentTime && strtotime('0'.$start_time.':00:00') <= $currentTime) {
            $crrentSysDate = new DateTime(date('H:i:s'));
            $userDefineDate = $crrentSysDate->format('H:i:s');
            $start = date_create($userDefineDate);
            $end = date_create(date('H:i:s', strtotime($close_time.':00:00')));

            $diff = date_diff($start, $end);
            return array('status' => TRUE, 'diff' => $diff);
        } else {
            return array('status' => FALSE);
        }
    }
}

if (!function_exists('userRoleCheck')) {
    function userRoleCheck($roles)
    {
        return in_array(auth()->user()->role_id, $roles) ? true : false;
    }
}

if (!function_exists('commissionandgst')) {
    function commissionandgst($shop_id,$gst,$price)
    {
        $shop = Shop::where('id',$shop_id)->pluck('shop_commission');
        if(!empty($shop)){
            $shop_commission = $shop[0];
        }
        $highTotal = floatval($shop_commission) * $price;
        $mainAmount = $highTotal/100;

        $gSThighTotal = floatval($gst) * $price;
        $mainGSTAmount = $gSThighTotal/100;

        return round($price + $mainAmount + $mainGSTAmount);
    }
}


if (!function_exists('withquantity')) {
    function withquantity($price,$product_id,$cart_id)
    {
        $cartDetails = Cart::where('id',$cart_id)->where('product_id',$product_id)->where('user_id',Auth::user()->id)->first();

        $subTotalProduct = $cartDetails->sel_quantity * $price;

        return $subTotalProduct;
    }
}

if (!function_exists('subtotal')) {
    function subtotal()
    {
        $amountDetails = Cart::leftjoin('products', 'cart.product_id', '=', 'products.id')
        ->select([
            'products.price',
            'products.id',
            'cart.sel_quantity',
            'cart.created_at as adddate',
            'cart.id as cart_id',
            'cart.product_id',
        ])
        ->where('cart.user_id',Auth::user()->id)
        ->get();

        if(!empty($amountDetails)) {
            $baseprice = [];
            foreach ($amountDetails as $key => $amount) {
                $baseprice[] = $amount->sel_quantity * $amount->price;
            }
            return array_sum($baseprice);
        } else {
            return '0.00';
        }
    }
}

if (!function_exists('gstPrice')) {
    function gstPrice()
    {
        $subtotal = subtotal();
        $generalSettings = GeneralSetting::first();
        $highTotal = floatval($generalSettings->gst) * $subtotal;
        $mainAmount = $highTotal/100;
        return round($mainAmount);
    }
}

if (!function_exists('grandtotal')) {
    function grandtotal()
    {
        $subtotal = subtotal();
        // $gstPrice = gstPrice();
        $gstPrice = 0;
        return round($subtotal + $gstPrice);
    }
}

if (!function_exists('cartProducts')) {
    function cartProducts()
    {
        if(Auth::user()) {
            $user_id = Auth::user()->id;
            $cartproducts =  Cart::leftjoin('products', 'cart.product_id', '=', 'products.id')
            ->select([
                'products.product_name',
                'products.price',
                'products.id',
                'products.stock',
                'products.product_image',
                'products.shop_id',
                'cart.sel_size',
                'cart.sel_quantity',
                'cart.created_at as adddate',
                'cart.id as cart_id',
            ])
            ->where('products.status',1)
            ->where('cart.user_id',$user_id)
            ->orderBy('adddate', 'DESC')
            ->get();
            return $cartproducts;
        }
    }
}

?>
