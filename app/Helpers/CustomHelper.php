<?php

use App\MetaField;
use App\Models\GeneralSetting;
use App\Models\Menu;
use App\Models\Role\CustomRole;
use App\Models\Shop\Shop;
use App\Models\Social;
use App\Models\WishlistModel\Wishlist;
use App\Models\MainCategoryModel\MainCategory;
use App\Models\SubCategoryModel\SubCategory;
use App\Models\CartModel\Cart;
use Carbon\Carbon;

if (!function_exists('getLoginUserRoleName')) {
    function getLoginUserRoleName()
    {
        $roleName = CustomRole::find(Auth::user()->role_id)->name;
        return str_replace('_', ' ', ucwords($roleName, '_'));
    }
}
if (!function_exists('getRoleNameBYId')) {
    function getRoleNameBYId($roleId)
    {
        $roleName = CustomRole::find($roleId)->name;
        return str_replace('_', ' ', $roleName);
    }
}
if (!function_exists('shopCommission')) {
    function shopCommission($shopId)
    {
        return Shop::find($shopId)->shop_commission;
    }
}
if (!function_exists('getShop')) {
	function getShop($shopId)
	{
	    return Shop::find($shopId);
	}
}
if (!function_exists('getsSiteMetaField')) {
	function getsSiteMetaField($name)
	{
        return MetaField::where('name', $name)->first();
	}
}
?>
