<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
	return view('user.layouts.app');
})->name('user.home');*/
Auth::routes();
/*Route::match(['get', 'post'], 'login', function(){
    return redirect('/');
});
Route::match(['get', 'post'], 'register', function(){
    return redirect('/');
});*/
Route::get('/', 'User\UserController@index')->name('user.home');
Route::get('/home', 'User\UserController@index')->name('user.home');

Route::post('user_register', 'User\UserController@store')->name('user.register');
Route::post('user_login', 'Auth\LoginController@userLogin')->name('user.login');

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider')->name('facebook.login');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');


Route::get('/home', 'User\UserController@index')->name('home');
Route::get('/account', 'User\UserController@edit')->name('account');
Route::get('/user-change-password', 'User\UserController@userChangePassword')->name('user-change-password');
Route::post('/user-password-set', 'User\UserController@userPasswordSet')->name('user-password-set');
Route::post('/account-update', 'User\UserController@update')->name('account-update');
Route::get('/user/logout', 'User\Auth\LoginController@userLogout')->name('user.logout');
Route::get('/contact-us', 'User\UserController@contactUs')->name('contact.us');
Route::post('/contact-us-inquiry', 'User\UserController@contactUsInquiry')->name('contact.us.inquiry');

Route::get('/shop', 'User\ShopInquiryController@index')->name('shop-form');
Route::post('/shop-inquiry', 'User\ShopInquiryController@store')->name('shop-inquiry');

//Product
Route::get('/product-list/{key?}', 'User\ProductController@index')->name('product-list');
Route::post('/get-product', 'User\ProductController@productList')->name('get-product');
Route::post('/get-product-quick-view', 'User\ProductController@getProductQuickView')->name('get-product-quick-view');
#=========== Admin Routes =============#
include 'admin-route.php';

#=========== Ravi Routes =============#
include 'ravi-route.php';
