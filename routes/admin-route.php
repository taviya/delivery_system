<?php

Route::prefix('admin')->group(function(){
	Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/forgot-password-email', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.forgot.password.email');
	Route::post('/send-forgot-password-email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmails')->name('admin.password.emails');
	Route::get('/{token}/reset-password', 'Admin\Auth\ForgotPasswordController@getPassword');
	Route::post('/reset-password', 'Admin\Auth\ForgotPasswordController@updatePassword')->name('admin.password.request');

	Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
	Route::post('/logout', 'Admin\Auth\AdminLoginController@logout')->name('admin.logout');

});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin']], function () {
//    For delivery person otp
    Route::post('/otp-send-for-delivery', 'Admin\AdminController@OtpSendForDelivery')->name('otp.send.for.delivery');
    Route::post('/order-confirm', 'Admin\AdminController@orderConfirmByOtp')->name('order.confirm');

	//Admin Users Route
	Route::get('/admin-users', 'Admin\AdminController@adminUser')->name('admin.users');
	Route::get('/admin-addusers', 'Admin\AdminController@addadminUser')->name('add.adminuser');
	Route::get('/admin-user-edit/{id}', 'Admin\AdminController@adminUserEdit')->name('admin.user.edit');
	Route::delete('/admin-user-edit/{id}', 'Admin\AdminController@adminUserDelete')->name('admin.user.delete');
	Route::post('/admin.user.update', 'Admin\AdminController@adminUserUpdate')->name('admin.user.update');
	Route::post('/admin.user.create', 'Admin\AdminController@adminUserCreate')->name('admin.user.create');

	/// Resource Routes

	Route::resources([
		'users'       => 'Admin\Users\UsersController',
		'ajaxmodule'  => 'Admin\AjaxModule\AjaxModuleController',
	]);

	/// User Management
	Route::post('users/get', 'Admin\Users\UsersTableController')->name('users.get');
	Route::post('users/delete', 'Admin\Users\UsersController@UserDelete')->name('users.delete');
	Route::post('users/block', 'Admin\Users\UsersController@UserBlock')->name('users.block');
	Route::post('users/unblock', 'Admin\Users\UsersController@UserUnBlock')->name('users.unblock');
	Route::get('users/view/{id}', 'Admin\Users\UsersController@UserView')->name('users.view');
	Route::post('users/profile/{id}', 'Admin\Users\UsersController@userProfileUpdate')->name('user.profile.update');
	Route::post('users/password/{id}', 'Admin\Users\UsersController@userPassword')->name('user.changepassword');
	Route::get('/addusers', 'Admin\Users\UsersController@addUsers')->name('add.newuser');
	Route::post('/storeuser', 'Admin\Users\UsersController@storeUsers')->name('admin.storeuser');
	Route::get('users/edit/{id}', 'Admin\Users\UsersController@Useredit')->name('users.edit');
	Route::post('users/update', 'Admin\Users\UsersController@userUpdate')->name('user.detail.update');


	// Role Management
	Route::resource('role', 'Admin\Role\RoleController', ['except' => ['show']]);
    // Permission Management
	Route::resource('permission', 'Admin\Permission\PermissionController', ['except' => ['show']]);

	//// Configurations Routes

	Route::get('/config/logo', 'Admin\Configurations\LogoIconController@index')->name('admin.logoIcon.index');
	Route::post('/configurations/logoIcon/update', 'Admin\Configurations\LogoIconController@update')->name('admin.logoIcon.update');

	Route::get('/config/general', 'Admin\Configurations\GeneralSettingController@GenSetting')->name('admin.GenSetting');
	Route::post('/generalSetting', 'Admin\Configurations\GeneralSettingController@UpdateGenSetting')->name('admin.UpdateGenSetting');

	Route::get('/config/timer', 'Admin\Configurations\GeneralSettingController@TimerSetting')->name('admin.TimerSetting');
	Route::post('/TimerSettingSave', 'Admin\Configurations\GeneralSettingController@TimerSettingSave')->name('admin.TimerSettingSave');

	Route::get('/config/banner', 'Admin\Configurations\GeneralSettingController@bannerSetting')->name('admin.banner.setting');
	Route::post('/BannerSettingSave', 'Admin\Configurations\GeneralSettingController@bannerSettingSave')->name('admin.banner.setting.save');

	Route::get('/config/social', 'Admin\Configurations\SocialController@index')->name('admin.social.index');
	Route::post('/configurations/social/store', 'Admin\Configurations\SocialController@store')->name('admin.social.store');
	Route::post('/configurations/social/delete', 'Admin\Configurations\SocialController@delete')->name('admin.social.delete');
	Route::get('/configurations/social/edit/{id}', 'Admin\Configurations\SocialController@edit')->name('admin.social.edit');
	Route::post('/configurations/social/update', 'Admin\Configurations\SocialController@update')->name('admin.social.update');

	Route::get('/google/analytics', 'Admin\Configurations\Googleanalytics@index')->name('admin.google.index');
	Route::post('/configurations/google/update', 'Admin\Configurations\Googleanalytics@update')->name('admin.google.update');

	// General SEO
	Route::get('general-seo/{id}', 'Admin\Seo\GeneralController@edit')->name('admin.general.edit');
	Route::post('general-seo/{id}', 'Admin\Seo\GeneralController@update')->name('admin.general.update');

	// Menu Manager Routes
	Route::get('/menu/index', 'Admin\Menu\MenuManagerController@index')->name('admin.menuManager.index');
	Route::post('/menuManager/store', 'Admin\Menu\MenuManagerController@store')->name('admin.menuManager.store');
	Route::get('/menu/{menuID}/edit', 'Admin\Menu\MenuManagerController@edit')->name('admin.menuManager.edit');
	Route::post('/menuManager/{menuID}/update', 'Admin\Menu\MenuManagerController@update')->name('admin.menuManager.update');
	Route::post('/menuManager/{menuID}/delete', 'Admin\Menu\MenuManagerController@delete')->name('admin.menuManager.delete');

	// Admin Profiles Routes
	Route::get('profile/{id}', 'Admin\AdminController@adminProfile')->name('admin.adminProfile');
	Route::post('editprofile', 'Admin\AdminController@updateAdminProfile')->name('admin.profile.update');
	Route::post('updatePassword', 'Admin\AdminController@updatePassword')->name('admin.changepassword');
	Route::post('/configurations/adminprofile/update', 'Admin\AdminController@updateprofile')->name('admin.logoIcon.update');

	Route::get('subscribers', 'Admin\SubscriberController@index')->name('admin.subscriber.get');
	Route::post('send/broadcastmail', 'Admin\SubscriberController@mailtosubsc')->name('admin.sendmail');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin']], function () {
    //Shop Route
	Route::get('/shop-list/{id?}', 'Admin\Shop\ShopController@index')->name('shop.list');
	Route::post('/shop-addshop', 'Admin\Shop\ShopController@store')->name('add.shop');
	Route::get('/shop-edit/{id}', 'Admin\Shop\ShopController@edit')->name('shop.edit');
	Route::post('/shop-delete', 'Admin\Shop\ShopController@destroy')->name('shop.delete');
	Route::post('/shop-update', 'Admin\Shop\ShopController@update')->name('shop.update');
	Route::get('/shop-status-update/{id}', 'Admin\Shop\ShopController@statusChange')->name('shop.status.update');

    //Slider Route
	Route::get('/slider-list', 'Admin\Slider\SliderController@index')->name('slider.list');
	Route::post('/add-slider', 'Admin\Slider\SliderController@store')->name('add.slider');
	Route::get('/slider-edit/{id}', 'Admin\Slider\SliderController@edit')->name('slider.edit');
	Route::post('/slider-update', 'Admin\Slider\SliderController@update')->name('slider.update');
	Route::post('/slider-delete', 'Admin\Slider\SliderController@destroy')->name('slider.delete');
	Route::get('/slider-status-update/{id}', 'Admin\Slider\SliderController@statusChange')->name('slider.status.update');

    //Coupon Code Route
	Route::get('/coupon-code-list', 'Admin\CouponCode\CouponCodeController@index')->name('coupon.code.list');
	Route::post('/add-coupon-code', 'Admin\CouponCode\CouponCodeController@store')->name('add.coupon.code');
	Route::get('/coupon-code-edit/{id}', 'Admin\CouponCode\CouponCodeController@edit')->name('coupon.code.edit');
	Route::post('/coupon-code-update', 'Admin\CouponCode\CouponCodeController@update')->name('coupon.code.update');
	Route::post('/coupon-code-delete', 'Admin\CouponCode\CouponCodeController@destroy')->name('coupon.code.delete');
	Route::get('/coupon-code-status-update/{id}', 'Admin\CouponCode\CouponCodeController@statusChange')->name('coupon.code.status.change');
	Route::post('coupon-code-name-check', 'Admin\CouponCode\CouponCodeController@checkUniqueCouponCode')->name('coupon.code.check');

    //Brand Route
	Route::get('/brand-list', 'Admin\Brand\BrandController@index')->name('brand.list');
	Route::post('/add-brand', 'Admin\Brand\BrandController@store')->name('add.brand');
	Route::get('/brand-edit/{id}', 'Admin\Brand\BrandController@edit')->name('brand.edit');
	Route::post('/brand-update', 'Admin\Brand\BrandController@update')->name('brand.update');
	Route::post('/brand-delete', 'Admin\Brand\BrandController@destroy')->name('brand.delete');
	Route::get('/brand-status-update/{id}', 'Admin\Brand\BrandController@statusChange')->name('brand.status.change');

	// Main Category Routes
	Route::get('main-category', 'Admin\MainCategory\MainCategoryController@index')->name('maincategory.index');
	Route::post('maincategory/store', 'Admin\MainCategory\MainCategoryController@store')->name('maincategory.store');
	Route::post('maincategory/delete', 'Admin\MainCategory\MainCategoryController@MainCategoryDelete')->name('maincategory.delete');
	Route::get('main-category/edit/{id}', 'Admin\MainCategory\MainCategoryController@edit')->name('maincategory.edit');
	Route::post('maincategory/update', 'Admin\MainCategory\MainCategoryController@update')->name('maincategory.update');
	Route::get('maincategory/{id}', 'Admin\MainCategory\MainCategoryController@destroy')->name('maincategory.delete');
	Route::get('maincategorystatusupdate/{id}', 'Admin\MainCategory\MainCategoryController@statusUpdate')->name('maincategory.status.update');
	Route::post('maincategory/checkcategory', 'Admin\MainCategory\MainCategoryController@checkUniqueCategoryName')->name('maincategory.checkcategory');

	// Sub Category Routes
	Route::get('sub-category', 'Admin\SubCategory\SubCategoryController@index')->name('subcategory.index');
    Route::post('subcategory/store', 'Admin\SubCategory\SubCategoryController@store')->name('subcategory.store');
    Route::get('sub-category/edit/{id}', 'Admin\SubCategory\SubCategoryController@edit')->name('subcategory.edit');
    Route::post('subcategory/update', 'Admin\SubCategory\SubCategoryController@update')->name('subcategory.update');
    Route::get('subcategory/{id}', 'Admin\SubCategory\SubCategoryController@destroy')->name('subcategory.delete');
    Route::get('subcategorystatusupdate/{id}', 'Admin\SubCategory\SubCategoryController@statusUpdate')->name('subcategory.status.update');
    Route::post('subcategory/checkcategory', 'Admin\SubCategory\SubCategoryController@checkUniqueCategoryName')->name('subcategory.checkcategory');

    // User Routes
    Route::post('user/store', 'Admin\Users\UsersController@store')->name('user.store');
    Route::get('user/delete/{id}', 'Admin\Users\UsersController@destroy')->name('users.destroy');
    Route::get('user/status/update/{id}', 'Admin\Users\UsersController@statusUpdate')->name('users.status.update');
    Route::post('user/checkemail', 'Admin\Users\UsersController@checkUniqueEmail')->name('user.email');
    Route::post('user/checkmobileno', 'Admin\Users\UsersController@checkUniqueMobileNo')->name('user.mobileno');

    // Admin Routes
    Route::post('/admin-users', 'Admin\AdminController@addadminUser')->name('add.adminusers');
    Route::get('/admin-users/edit/{id}', 'Admin\AdminController@adminUserEdit')->name('admin.users.edit');
    Route::post('updatePassword/{id}', 'Admin\AdminController@updatePassword')->name('admin.user.changepassword');
    Route::post('/adminprofileimage/{id}', 'Admin\AdminController@updateprofile')->name('admin.profilesimage.update');
    Route::get('admin/status/update/{id}', 'Admin\AdminController@statusUpdate')->name('admin.status.update');
    Route::get('admin/delete/{id}', 'Admin\AdminController@destroy')->name('admins.destroy');
    Route::post('admin/checkemail', 'Admin\AdminController@checkUniqueEmail')->name('admin.email');

    // Email Routes
    Route::get('emails', 'Admin\EmailSubscriber\EmailSubscriberController@index')->name('email.index');
    Route::get('emails/{id}', 'Admin\EmailSubscriber\EmailSubscriberController@destroy')->name('email.delete');
    Route::get('/read-all-notifications', 'Admin\AdminController@readNotification')->name('read.all.notification');

    // Notification Routes
    Route::get('/notifications', 'Admin\AdminController@showNotification')->name('show.all.notification');
    Route::get('notification/{id}', 'Admin\AdminController@deleteNotification')->name('notification.delete');

    //Pincode Route
	Route::get('/pincode-list', 'Admin\Pincode\PincodeController@index')->name('pincode.list');
	Route::post('/add-pincode', 'Admin\Pincode\PincodeController@store')->name('add.pincode');
	Route::get('/pincode-edit/{id}', 'Admin\Pincode\PincodeController@edit')->name('pincode.edit');
	Route::post('/pincode-update', 'Admin\Pincode\PincodeController@update')->name('pincode.update');
	Route::post('/pincode-delete', 'Admin\Pincode\PincodeController@destroy')->name('pincode.delete');
	Route::get('/pincode-status-update/{id}', 'Admin\Pincode\PincodeController@statusChange')->name('pincode.status.change');

});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin']], function () {

    // Products Routes
    Route::get('products', 'Admin\Product\ProductController@index')->name('product.index');
    Route::get('product/create', 'Admin\Product\ProductController@create')->name('product.create');
    Route::get('product/sub/{id}', 'Admin\Product\ProductController@dependentDropdown')->name('maincategory.sub');
    Route::post('product/store', 'Admin\Product\ProductController@store')->name('product.store');
    Route::get('product/edit/{id}', 'Admin\Product\ProductController@edit')->name('product.edit');
    Route::get('product-status-change/{id}', 'Admin\Product\ProductController@statusUpdate')->name('product.status.update');
    Route::post('product/update', 'Admin\Product\ProductController@update')->name('product.update');
    Route::post('product/deleteimage', 'Admin\Product\ProductController@deleteImage')->name('product.deleteimage');
    Route::post('images/check', 'Admin\Product\ProductController@checkImage');
    Route::get('product/delete/{id}', 'Admin\Product\ProductController@destroy')->name('product.delete');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin']], function () {

    // Orders Routes
    Route::get('orders', 'Admin\Orders\OrdersController@index')->name('orders.list');
});
Route::get('/ajax-view-cart', 'User\CartController@getAjaxCartView')->name('ajax-view-cart');

?>
