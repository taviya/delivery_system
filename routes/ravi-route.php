<?php
Route::group(['prefix' => 'admin', 'middleware' => ['auth:admin']], function () {
	// Route::resources([
	// 	'maincategory'  => 'Admin\MainCategory\MainCategoryController',
	// ]);

	
});
Route::get('page/{slug}', 'User\PageController@index')->name('page');
Route::get('/product-detail/{id}', 'User\UserController@productDetail')->name('product-details');
Route::post('/quickview', 'User\UserController@quickview')->name('quickview');

Route::post('/wishlist', 'User\WishlistController@wishlist')->name('wishlist');
Route::get('/wishlist-product', 'User\WishlistController@wishlistlisting')->name('wishlist.listing');
Route::post('/wishlistmainlist', 'User\WishlistController@wishlistmainList')->name('wishlist.mainlist');

Route::post('/emailsubscriber', 'User\EmailSubscribeController@sendmail')->name('email.subscribe');

Route::post('/addtocart', 'User\CartController@addtocart')->name('add.tocart');
Route::get('/cart', 'User\CartController@cart')->name('cart');
Route::post('/get-cart', 'User\CartController@getCart')->name('get.cart');

Route::get('/checkout-shipping', 'User\CheckoutController@checkoutShipping')->name('checkout-shipping');
Route::get('/checkout-review', 'User\CheckoutController@checkoutReview')->name('checkout-review');
Route::post('/shpping-address', 'User\CheckoutController@addShippingAddress')->name('shpping-address');
Route::post('/check-pincode', 'User\CheckoutController@checkPincode')->name('check-pincode');

Route::post('/edit-shipping-address', 'User\CheckoutController@editShippingAddress')->name('edit-shipping-address');
Route::post('/update-shipping-address', 'User\CheckoutController@updateShippingAddress')->name('update-shpping-address');
Route::post('/shipping-order', 'User\CheckoutController@updateShippingOrder')->name('shipping-order');
Route::get('/place-order', 'User\CheckoutController@placeOrder')->name('place-order');
Route::get('/my-orders', 'User\UserController@myOrders')->name('my-orders');

Route::get('/shop/{any}', 'User\HomeController@categoryList')->name('maincategories.catname');

Route::get('/demolisting', 'User\PageController@demolisting')->name('demolisting');
Route::get('/products-shorts', 'User\HomeController@allShorts')->name('all.shorts');

?>
