8-jul-2021

ALTER TABLE `admins` ADD `status` INT NOT NULL DEFAULT '1' COMMENT '1:active 0:deactive' AFTER `phone`;
ALTER TABLE `admins` ADD `deleted_at` TIMESTAMP NULL AFTER `created_at`;
ALTER TABLE `admins` CHANGE `admin_image` `admin_image` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;


10 jul 2021

ALTER TABLE `users` CHANGE `created_by` `created_by` INT UNSIGNED NOT NULL DEFAULT '0';


19 jul 2021

ALTER TABLE `products` ADD `normalsize` INT NOT NULL DEFAULT '0' AFTER `seasonal`, ADD `shooessize` INT NOT NULL DEFAULT '0' AFTER `normalsize`;

24 jul 2021

ALTER TABLE `main_categories` ADD `category_icon` TEXT NULL AFTER `main_category_name`;


07 Aug 2021 

ALTER TABLE `products` ADD `product_sub_heading` VARCHAR(200) NULL DEFAULT NULL AFTER `product_name`;
ALTER TABLE `products` ADD `product_specification_heading` TEXT NULL DEFAULT NULL AFTER `product_image`, ADD `product_specification_value` TEXT NULL DEFAULT NULL AFTER `product_specification_heading`;


21 Aug 2021

ALTER TABLE `general_settings` ADD `price_commission` INT NOT NULL DEFAULT '0' AFTER `gst`;

ALTER TABLE `products` ADD `original_price` INT NOT NULL DEFAULT '0' AFTER `price`;

ALTER TABLE `general_settings` CHANGE `price_commission` `price_commission` TEXT NOT NULL DEFAULT '0';

MAIL_DRIVER=sendmail
MAIL_HOST=localhost
MAIL_PORT=25
MAIL_USERNAME=support@easytowndeal.com
MAIL_PASSWORD=helloworld111
MAIL_FROM_ADDRESS=support@easytowndeal.com
MAIL_FROM_NAME=easytowndeal
MAIL_ENCRYPTION=NULL

14 sep 2021

ALTER TABLE `payments` ADD `shipping_address_id` INT NULL AFTER `user_id`;


26-sep
ALTER TABLE `sub_categories` ADD `subcategory_icon` TEXT NULL DEFAULT NULL AFTER `sub_category_name`;


08 ocT

ALTER TABLE `products` ADD `product_gst` INT NOT NULL DEFAULT '0' AFTER `stock`;